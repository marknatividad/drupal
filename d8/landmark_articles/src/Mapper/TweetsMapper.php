<?php

namespace Drupal\landmark_articles\Mapper;

use Drupal\Component\Datetime\Time;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\landmark_helpers\Dao\SiteSettingsDao;
use Drupal\landmark_helpers\Wrapper\FieldWrapper;
use Drupal\landmark_helpers\Wrapper\ImageStyleWrapper;
use Drupal\landmark_main\LSG\Tweets;
use Drupal\landmark_main\Mapper\BaseMapper;
use Drupal\media_entity_twitter\Exception\TwitterApiException;

/**
 * Class TweetsMapper.
 */
class TweetsMapper extends BaseMapper {

  private $lsg;
  private $siteSettingsDao;
  private $json;
  private $cache;
  private $time;

  /**
   * TweetsMapper constructor.
   */
  public function __construct(EntityTypeManager $entityTypeManager, FieldWrapper $fieldWrapper, ImageStyleWrapper $imageStyleWrapper, Tweets $lsg, SiteSettingsDao $siteSettingsDao, Json $json, CacheBackendInterface $cache, Time $time) {
    parent::__construct($entityTypeManager, $fieldWrapper, $imageStyleWrapper);
    $this->lsg = $lsg;
    $this->siteSettingsDao = $siteSettingsDao;
    $this->json = $json;
    $this->cache = $cache;
    $this->time = $time;
  }

  /**
   * Maps site settings data.
   *
   * @return \Drupal\landmark_main\LSG\Tweets|false
   *   The LSG object or FALSE.
   */
  public function mapFromSiteSettings() {
    // Get twitter username from the Social Links site settings.
    $twitter_url = $this->siteSettingsDao->getSiteSetting('global', 'social_links', 'field_social_twitter', 0, 'uri');
    if (!$twitter_url) {
      return FALSE;
    }
    $button = [
      'link' => $twitter_url,
      'label' => 'follow us',
      'target' => '_blank',
    ];
    $this->lsg->setBtn($button);

    $parts = explode('/twitter.com/', $twitter_url);
    if (empty($parts[1])) {
      return FALSE;
    }
    $this->lsg->setUsername($parts[1]);

    // Do not return anything if tweets can't be fetched.
    if (!$tweets = $this->getTweets($parts[1])) {
      return FALSE;
    }
    $this->lsg->setTweets($tweets);

    return $this->lsg;
  }

  /**
   * Gets 2 recent user's tweets.
   *
   * @param string $username
   *   Twitter username.
   *
   * @return array[]
   *   2 arrays with text and date.
   */
  public function getTweets($username) {
    $tweets = [];

    if (!$config = $this->getAppCredentials()) {
      return [];
    }

    // Get tweets from cache, even stale, in case the Twitter API call fails.
    $cid = 'landmark_articles:tweets_mapper:' . $username;
    $expire = $this->time->getRequestTime() + 3600;
    $cache = $this->cache->get($cid, TRUE);

    if ($cache && !empty($cache->valid)) {
      return $cache->data;
    }

    try {
      $twitter = new \TwitterAPIExchange($config);
      $response = $twitter->setGetfield('?screen_name=' . $username . '&count=2')
        ->buildOauth('https://api.twitter.com/1.1/statuses/user_timeline.json', 'GET')
        ->performRequest();
      $response = $this->json::decode($response);

      // Handle errors as per
      // https://dev.twitter.com/overview/api/response-codes.
      if (!empty($response['errors'])) {
        throw new TwitterApiException($response['errors']);
      }

      if (is_array($response)) {
        foreach ($response as $tweet) {
          $tweets[] = [
            'text' => ['#markup' => $this->formatTweet($tweet)],
            'date' => date('F j, Y', strtotime($tweet['created_at'])),
          ];
        }
      }
    }
    catch (\Exception $e) {
      watchdog_exception('landmark_articles', $e);

      // Re-cache stale tweets or an empty array for another 5 min in order to
      // not make Twitter API calls and log errors on every page request.
      $tweets = $cache ? $cache->data : [];
      $expire = $this->time->getRequestTime() + 300;
    }

    $this->cache->set($cid, $tweets, $expire);

    return $tweets;
  }

  /**
   * Returns credentials to connect to a Twitter application.
   *
   * @return array|null
   *   Array of credentials or NULL.
   */
  private function getAppCredentials() {
    $setting = $this->siteSettingsDao->getSiteSetting('global', 'socshare', NULL, 0, 'value');

    $credentials = [
      'consumer_key' => $setting['field_socshare_twkey'] ?? NULL,
      'consumer_secret' => $setting['field_socshare_twsec'] ?? NULL,
      'oauth_access_token' => $setting['field_socshare_twtoken'] ?? NULL,
      'oauth_access_token_secret' => $setting['field_socshare_twtokensec'] ?? NULL,
    ];

    return count(array_filter($credentials)) == 4 ? $credentials : NULL;
  }

  /**
   * Formats a Tweet.
   *
   * @param array $tweet
   *   Tweet data as returned from the Twitter API call.
   *
   * @return string
   *   Fully-formatted markup string.
   */
  public function formatTweet(array $tweet) {
    $replacements = [];
    $entities = $tweet['entities'] + [
      'urls' => [],
      'media' => [],
      'user_mentions' => [],
      'hashtags' => [],
    ];

    foreach (['urls', 'media'] as $type) {
      foreach ($entities[$type] as $url) {
        if (!empty($url['url'])) {
          $display_url = !empty($url['display_url']) ? $url['display_url'] : $url['url'];
          $url = $url['url'];

          $replacements[$url] = '<a href="' . $url . '" target="_blank">' . $display_url . '</a>';
        }
      }
    }

    foreach ($entities['user_mentions'] as $mention) {
      if (!empty($mention['screen_name'])) {
        $screen_name = $mention['screen_name'];

        $replacements['@' . $screen_name] = '<a href="https://twitter.com/' . $screen_name . '" target="_blank">@' . $screen_name . '</a>';
      }
    }

    foreach ($entities['hashtags'] as $hashtag) {
      if (!empty($hashtag['text'])) {
        $hashtag = $hashtag['text'];

        $replacements['#' . $hashtag] = '<a href="https://twitter.com/hashtag/' . $hashtag . '" target="_blank">#' . $hashtag . '</a>';
      }
    }

    return strtr($tweet['text'], $replacements);
  }

}
