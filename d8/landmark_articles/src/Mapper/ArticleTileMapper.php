<?php

namespace Drupal\landmark_articles\Mapper;

/**
 * @file
 * Class ArticleTileMapper.
 */

use Drupal\Core\Path\AliasManager;
use Drupal\landmark_helpers\Wrapper\FieldWrapper;
use Drupal\landmark_helpers\Wrapper\ImageStyleWrapper;
use Drupal\landmark_helpers\Wrapper\UnicodeWrapper;
use Drupal\landmark_main\LSG\ArticleTile;
use Drupal\node\NodeInterface;
use Drupal\paragraphs\ParagraphInterface;

/**
 * Class ArticleTileMapper.
 *
 * @package Drupal\landmark_articles\Mapper
 */
class ArticleTileMapper {

  private $fieldWrapper;
  private $imageStyleWrapper;
  private $aliasManager;
  private $unicodeWrapper;
  private $lsg;
  private $imageStyleName = 'image_1200x_';

  /**
   * ArticleTileMapper constructor.
   */
  public function __construct(FieldWrapper $fieldWrapper, ImageStyleWrapper $imageStyleWrapper, AliasManager $aliasManager, UnicodeWrapper $unicodeWrapper, ArticleTile $lsg) {
    $this->fieldWrapper = $fieldWrapper;
    $this->imageStyleWrapper = $imageStyleWrapper;
    $this->aliasManager = $aliasManager;
    $this->unicodeWrapper = $unicodeWrapper;
    $this->lsg = $lsg;
  }

  /**
   * Map article data from node.
   */
  public function mapFromNode(NodeInterface $node) {
    if ('article' !== $node->bundle()) {
      return FALSE;
    }

    $title = $node->getTitle();
    $this->lsg->setTitle($title);

    $banner = $this->fieldWrapper->getFieldReferencedEntities($node, 'field_article_banner');
    if (NULL !== $banner) {
      $banner = reset($banner);

      $description = $banner->get('field_pg_articlebanner_text')->value;
      $description = html_entity_decode(strip_tags($description));
      $description = $this->unicodeWrapper->truncate($description, 300, TRUE, TRUE, 20);
      $this->lsg->setDescription($description);

      $img = $this->getImageFrom($banner, 'field_pg_articlebanner_image');
      $this->lsg->setImg($img);
    }

    $pill = [];
    $tags = $this->fieldWrapper->getFieldReferencedEntities($node, 'field_shared_tags');
    if (NULL !== $tags) {
      /** @var \Drupal\taxonomy\Entity\Term $tag */
      foreach ($tags as $tag) {
        $pill[] = [
          'txt' => $tag->getName(),
        ];
      }
    }
    $this->lsg->setPill($pill);
    $this->lsg->setLabels([
      'show' => [
        '#markup' => '+<span class="hidden-pill-total">#</span> MORE TAGS',
      ],
      'hide' => '- HIDE TAGS',
    ]);

    $link = [
      'href' => $this->aliasManager->getAliasByPath('/node/' . $node->id()),
    ];
    $this->lsg->setLink($link);

    return $this->lsg;
  }

  /**
   * Returns the image source and alt text.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   *   The entity to read the value from.
   * @param string $field_name
   *   The field name of the entity.
   *
   * @return array
   *   The formatted array.
   */
  public function getImageFrom(ParagraphInterface $paragraph, $field_name): array {
    /* @var \Drupal\file\Entity\File $imageFile */
    $image_source = NULL;
    if ($imageFile = $this->fieldWrapper->getFieldMediaImageFile($paragraph, $field_name)) {
      $image_source = $this->imageStyleWrapper->getImageUriFromFile($imageFile, $this->imageStyleName);
    }

    /* @var \Drupal\media_entity\Entity\Media $imageMedia */
    $image_alt = NULL;
    $imageMedia = $this->fieldWrapper->getFieldValueFromEntityRef($paragraph, 'image', $field_name, FALSE);
    if (NULL !== $imageMedia) {
      $image_alt = $imageMedia[0]['alt'];
    }

    return [
      'src' => $image_source,
      'alt' => $image_alt,
    ];
  }

}
