<?php

namespace Drupal\landmark_articles\Mapper;

/**
 * @file
 * Class ArticleDetailsMapper.
 */

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Render\Renderer;
use Drupal\landmark_helpers\Wrapper\FieldWrapper;
use Drupal\landmark_main\LSG\Article;
use Drupal\node\NodeInterface;

/**
 * Class ArticleDetailsMapper.
 *
 * @package Drupal\landmark_articles\Mapper
 */
class ArticleDetailsMapper {

  private $entityTypeManager;
  private $fieldWrapper;
  private $renderer;
  private $lsg;

  /**
   * ArticleDetailsMapper constructor.
   */
  public function __construct(EntityTypeManager $entityTypeManager, FieldWrapper $fieldWrapper, Renderer $renderer, Article $lsg) {
    $this->entityTypeManager = $entityTypeManager;
    $this->fieldWrapper = $fieldWrapper;
    $this->renderer = $renderer;
    $this->lsg = $lsg;
  }

  /**
   * Map article data from node.
   */
  public function mapFromNode(NodeInterface $node) {
    if ('article' !== $node->bundle()) {
      return FALSE;
    }

    $banner = $this->getBannerFrom($node);
    $this->lsg->setBanner($banner);

    $body = $this->getBodyFrom($node);
    $this->lsg->setBody($body);

    $shareConfig = [
      'facebook',
      'twitter',
      'linkedin',
    ];
    $this->lsg->setShareConfig($shareConfig);

    return $this->lsg;
  }

  /**
   * Extract banner content from node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The article node.
   *
   * @return array
   *   The render array.
   */
  public function getBannerFrom(NodeInterface $node) {
    $banner = $this->fieldWrapper->getFieldReferencedEntities($node, 'field_article_banner');
    if (NULL === $banner) {
      return [];
    }

    $banner = reset($banner);
    $paragraphViewBuilder = $this->entityTypeManager->getViewBuilder('paragraph');
    $paragraphView = $paragraphViewBuilder->view($banner);
    $markup = $this->renderer->render($paragraphView);

    return [
      '#markup' => $markup,
    ];
  }

  /**
   * Extract body content from node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The article node.
   *
   * @return array
   *   The render array.
   */
  public function getBodyFrom(NodeInterface $node) {
    $contents = $this->fieldWrapper->getFieldReferencedEntities($node, 'field_article_body');
    if (NULL === $contents) {
      return [];
    }

    $children = [];
    $paragraphViewBuilder = $this->entityTypeManager->getViewBuilder('paragraph');
    /** @var \Drupal\paragraphs\Entity\Paragraph $paragraphItem */
    foreach ($contents as $paragraphItem) {
      $paragraphView = $paragraphViewBuilder->view($paragraphItem);
      $children[$paragraphItem->uuid()] = [
        '#markup' => $this->renderer->render($paragraphView),
      ];
    }

    return [
      '#type' => 'container',
      'children' => $children,
    ];
  }

}
