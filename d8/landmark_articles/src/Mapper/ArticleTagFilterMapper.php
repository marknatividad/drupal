<?php

namespace Drupal\landmark_articles\Mapper;

/**
 * @file
 * Class ArticleTagFilterMapper.
 */

use Drupal\landmark_articles\Request\ArticleTagQueryParam;
use Drupal\landmark_helpers\Wrapper\UrlHelperWrapper;
use Drupal\landmark_helpers\Wrapper\UrlWrapper;
use Drupal\landmark_main\LSG\ArticleFilter;
use Drupal\taxonomy\TermInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class ArticleTagFilterMapper.
 *
 * @package Drupal\landmark_articles\Mapper
 */
class ArticleTagFilterMapper {

  private $queryParam;
  private $lsg;
  private $requestStack;
  private $urlWrapper;
  private $urlHelperWrapper;

  /**
   * ArticleTagFilterMapper constructor.
   */
  public function __construct(ArticleTagQueryParam $queryParam, ArticleFilter $lsg, RequestStack $requestStack, UrlWrapper $urlWrapper, UrlHelperWrapper $urlHelperWrapper) {
    $this->queryParam = $queryParam;
    $this->lsg = $lsg;
    $this->requestStack = $requestStack;
    $this->urlWrapper = $urlWrapper;
    $this->urlHelperWrapper = $urlHelperWrapper;
  }

  /**
   * Maps term data.
   *
   * @param \Drupal\taxonomy\TermInterface $term
   *   The term to extract values from.
   *
   * @return \Drupal\landmark_main\LSG\ArticleFilter
   *   The filter object.
   */
  public function mapFromTerm(TermInterface $term) {
    $href = $this->getHrefFrom($term);
    $this->lsg->setHref($href);
    $modifier = (TRUE === $this->queryParam->isFilterCurrent($term)) ? 'current-filter' : '';
    $this->lsg->setModifier($modifier);
    $this->lsg->setTitle($term->getName());
    return $this->lsg;
  }

  /**
   * Returns the formed category URL.
   *
   * @param \Drupal\taxonomy\TermInterface $term
   *   The term.
   *
   * @return string
   *   The formed URL.
   */
  public function getHrefFrom(TermInterface $term) {
    $options = [];

    $tags_query = $this->queryParam->buildParameter($term);
    if (NULL !== $tags_query) {
      $options['query'] = $tags_query;
    }

    $request_uri = $this->requestStack->getCurrentRequest()->getUri();
    $url_parts = $this->urlHelperWrapper->parse($request_uri);
    return $this->urlWrapper->fromUri($url_parts['path'], $options);
  }

}
