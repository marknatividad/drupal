<?php

namespace Drupal\landmark_articles\Mapper;

/**
 * @file
 * Class LatestArticlesBlockMapper.
 */

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Render\Renderer;
use Drupal\landmark_articles\QueryBuilder\ArticleQueryBuilder;
use Drupal\landmark_helpers\Dao\SiteSettingsDao;
use Drupal\landmark_helpers\Wrapper\FieldWrapper;
use Drupal\landmark_helpers\Wrapper\UrlWrapper;
use Drupal\landmark_main\LSG\LatestArticlesBlock;
use Drupal\paragraphs\ParagraphInterface;

/**
 * Class LatestArticlesBlockMapper.
 *
 * @package Drupal\landmark_articles\Mapper
 */
class LatestArticlesBlockMapper {

  private $entityTypeManager;
  private $fieldWrapper;
  private $siteSettingsDao;
  private $urlWrapper;
  private $renderer;
  private $lsg;

  /**
   * LatestArticlesMapper constructor.
   */
  public function __construct(EntityTypeManager $entityTypeManager, FieldWrapper $fieldWrapper, SiteSettingsDao $siteSettingsDao, UrlWrapper $urlWrapper, Renderer $renderer, LatestArticlesBlock $lsg) {
    $this->entityTypeManager = $entityTypeManager;
    $this->fieldWrapper = $fieldWrapper;
    $this->siteSettingsDao = $siteSettingsDao;
    $this->urlWrapper = $urlWrapper;
    $this->renderer = $renderer;
    $this->lsg = $lsg;
  }

  /**
   * Maps paragraph data.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   *   The entity to extract values from.
   *
   * @return \Drupal\landmark_main\LSG\LatestArticlesBlock|false
   *   The LSG object or false.
   */
  public function mapFromParagraph(ParagraphInterface $paragraph) {
    if ('pg_artlatest' !== $paragraph->bundle()) {
      return FALSE;
    }

    $modifiers = $this->fieldWrapper->getFieldValue($paragraph, 'field_pg_artlatest_modifiers');
    $this->lsg->setModifiers($modifiers);

    $link = $this->getLinkButtonFrom($paragraph, 'field_pg_artlatest_link_label');
    $this->lsg->setLink($link);

    $tiles = $this->getArticleTiles($paragraph);
    $this->lsg->setTiles($tiles);

    return $this->lsg;
  }

  /**
   * Returns the link button URL and link text.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   *   The entity to read the value from.
   * @param string $field_name
   *   The field name of the entity.
   *
   * @return array
   *   The formatted array.
   */
  public function getLinkButtonFrom(ParagraphInterface $paragraph, $field_name) {
    $label = $this->fieldWrapper->getFieldValue($paragraph, $field_name);
    if (NULL === $label) {
      $label = 'View more articles';
    }
    $href = $this->urlWrapper->fromRoute('landmark_articles.articles_list');

    $settings = $this->siteSettingsDao->getArticleListingSettings();
    if (NULL !== $settings['path']) {
      $href = $settings['path'];
    }

    return [
      'txt' => $label,
      'href' => $href,
    ];
  }

  /**
   * Returns article tiles render array.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   *   The entity to read the value from.
   *
   * @return array
   *   The renderable array.
   */
  public function getArticleTiles(ParagraphInterface $paragraph) {
    $tiles = [];

    $tags = $this->fieldWrapper->getFieldValue($paragraph, 'field_pg_artlatest_tag_filter', FALSE);
    $query = $this->entityTypeManager->getStorage('node')->getQuery();
    $queryBuilder = new ArticleQueryBuilder($query);

    if (NULL !== $tags) {
      $tags_ids = array_column($tags, 'target_id');
      $queryBuilder->setTags($tags_ids);
    }

    $queryBuilder->setSort('created', 'DESC');
    $queryBuilder->setLimit(3);
    $query = $queryBuilder->generateQuery();
    $nids = $query->execute();

    if (!empty($nids)) {
      $nodes = $this->entityTypeManager->getStorage('node')->loadMultiple($nids);
      foreach ($nodes as $node) {
        $nodeViewBuilder = $this->entityTypeManager->getViewBuilder('node');
        $nodeView = $nodeViewBuilder->view($node, 'tile');
        $nodeView['#no_image'] = TRUE;
        $tiles[] = $this->renderer->render($nodeView);
      }
    }

    return $tiles;
  }

}
