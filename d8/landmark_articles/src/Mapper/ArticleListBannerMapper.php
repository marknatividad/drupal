<?php

namespace Drupal\landmark_articles\Mapper;

/**
 * @file
 * Class ArticleListBannerMapper.
 */

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\landmark_helpers\Dao\SiteSettingsDao;
use Drupal\landmark_helpers\Wrapper\FieldWrapper;
use Drupal\landmark_helpers\Wrapper\ImageStyleWrapper;
use Drupal\landmark_main\LSG\HeaderBanner;
use Drupal\landmark_main\Mapper\BaseMapper;

/**
 * Class ArticleListBannerMapper.
 *
 * @package Drupal\landmark_products\Mapper
 */
class ArticleListBannerMapper extends BaseMapper {

  private $headerBanner;
  private $bannerSettings;
  private $imageStyleName = 'image_3000x_';

  /**
   * ArticleListBannerMapper constructor.
   */
  public function __construct(EntityTypeManager $entityTypeManager, FieldWrapper $fieldWrapper, ImageStyleWrapper $imageStyleWrapper, HeaderBanner $headerBanner, SiteSettingsDao $siteSettingsDao) {
    parent::__construct($entityTypeManager, $fieldWrapper, $imageStyleWrapper);
    $this->headerBanner = $headerBanner;
    $this->bannerSettings = $siteSettingsDao->getArticleListingSettings();
  }

  /**
   * Maps site settings data.
   *
   * @return \Drupal\landmark_main\LSG\HeaderBanner|false
   *   The banner object or false.
   */
  public function mapFromSiteSettings() {
    if (empty($this->bannerSettings)) {
      return FALSE;
    }

    $bg = $this->getLogoImgUri($this->bannerSettings['banner'], $this->imageStyleName);
    $this->headerBanner->setBackgroundImg($bg);
    $this->headerBanner->setDividedVersion(FALSE);
    $this->headerBanner->setTitle($this->bannerSettings['title']);
    $this->headerBanner->setDescription($this->bannerSettings['description']);
    return $this->headerBanner;
  }

}
