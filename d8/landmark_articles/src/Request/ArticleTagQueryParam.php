<?php

namespace Drupal\landmark_articles\Request;

/**
 * @file
 * Class ArticleTagQueryParam.
 */

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Path\AliasManager;
use Drupal\taxonomy\TermInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class ArticleTagQueryParam.
 *
 * @package Drupal\landmark_articles\Request
 */
class ArticleTagQueryParam {

  private $aliasManager;
  private $entityTypeManager;
  private $requestStack;
  private $aliasPrefix = '/tags/';
  private $delimiter = ',';
  private $queryKey = 'tags';

  /**
   * ArticleTagQueryParam constructor.
   */
  public function __construct(AliasManager $aliasManager, EntityTypeManager $entityTypeManager, RequestStack $requestStack) {
    $this->aliasManager = $aliasManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->requestStack = $requestStack;
  }

  /**
   * Return article tag term IDs based on query parameters.
   *
   * @return array
   *   Array of article tag term IDs.
   */
  public function getTermsIds() {
    $terms_ids = [];

    $tags_existing = $this->getParameter();
    if (!empty($tags_existing)) {
      $tags_existing = explode($this->delimiter, $tags_existing);
      foreach ($tags_existing as $term_alias) {
        $alias = $this->aliasPrefix . $term_alias;
        $path = $this->aliasManager->getPathByAlias($alias);
        if ($alias != $path) {
          $terms_ids[] = str_replace('/taxonomy/term/', '', $path);
        }
      }
    }

    if (!empty($terms_ids)) {
      $terms_ids = array_unique($terms_ids);
    }

    return $terms_ids;
  }

  /**
   * Converts tag query parameters to an array.
   *
   * @return array
   *   Array of article tag term names.
   */
  public function getTermsNames() {
    $tags_existing = $this->getParameter();
    if (empty($tags_existing)) {
      return [];
    }

    $terms = [];

    $tags_existing = explode($this->delimiter, $tags_existing);
    foreach ($tags_existing as $term_alias) {
      $alias = $this->aliasPrefix . $term_alias;
      $path = $this->aliasManager->getPathByAlias($alias);
      if ($alias != $path) {
        $term_id = str_replace('/taxonomy/term/', '', $path);
        /** @var \Drupal\taxonomy\Entity\Term $term */
        $term = $this->entityTypeManager
          ->getStorage('taxonomy_term')
          ->load($term_id);
        if ($term) {
          $terms[] = $term->getName();
        }
      }
    }

    return $terms;
  }

  /**
   * Returns the GET parameter value.
   *
   * @return string
   *   The parameter.
   */
  public function getParameter() {
    return $this->requestStack->getCurrentRequest()->query->get($this->queryKey);
  }

  /**
   * Returns the query key/value associative array.
   *
   * @return array
   *   The query key/value pair.
   */
  public function getParameterAsOption(): array {
    $parameter = $this->getParameter();

    if (!empty($parameter)) {
      return [$this->queryKey => $parameter];
    }

    return [];
  }

  /**
   * Returns the term alias.
   *
   * @param \Drupal\taxonomy\TermInterface $term
   *   The term.
   *
   * @return string
   *   The machine name.
   */
  public function getTermAlias(TermInterface $term): string {
    $alias = $this->aliasManager->getAliasByPath('/taxonomy/term/' . $term->id());
    return str_replace($this->aliasPrefix, '', $alias);
  }

  /**
   * Checks whether or not the given term exists in the query string.
   *
   * @param \Drupal\taxonomy\TermInterface $term
   *   The term.
   *
   * @return bool
   *   Whether or not the term exists in the tags query string.
   */
  public function isFilterCurrent(TermInterface $term): bool {
    $tags_existing = $this->getParameter();

    if (!empty($tags_existing)) {
      $tags_existing = explode($this->delimiter, $tags_existing);
      $term_alias = $this->getTermAlias($term);
      if (FALSE !== array_search($term_alias, $tags_existing)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Builds the tags query parameter.
   *
   * @param \Drupal\taxonomy\TermInterface $term
   *   The term.
   *
   * @return array|null
   *   The formed query parameter.
   */
  public function buildParameter(TermInterface $term) {
    $term_alias = $this->getTermAlias($term);
    $tags_existing = $this->getParameter();

    if (empty($tags_existing)) {
      $tags_existing = [];
    }
    else {
      $tags_existing = explode($this->delimiter, $tags_existing);
    }

    if (!in_array($term_alias, $tags_existing)) {
      $tags_existing[] = $term_alias;
    }
    else {
      if (FALSE !== ($key = array_search($term_alias, $tags_existing))) {
        unset($tags_existing[$key]);
      }
    }

    if (!empty($tags_existing)) {
      return [
        $this->queryKey => implode($this->delimiter, $tags_existing),
      ];
    }

    return NULL;
  }

}
