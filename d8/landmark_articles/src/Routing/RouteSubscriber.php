<?php

namespace Drupal\landmark_articles\Routing;

/**
 * @file
 * Class RouteSubscriber.
 */

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber.
 *
 * @package Drupal\landmark_articles\Routing
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // @codingStandardsIgnoreStart
    /** @var \Drupal\landmark_helpers\Dao\SiteSettingsDao $siteSettingsDao */
    $siteSettingsDao = \Drupal::service('landmark_helpers.site_settings_dao');
    // @codingStandardsIgnoreEnd
    $settings = $siteSettingsDao->getArticleListingSettings();

    // Change default article listing path.
    if (NULL !== $settings['path'] && $route = $collection->get('landmark_articles.articles_list')) {
      $route->setPath($settings['path']);
    }
  }

}
