<?php

namespace Drupal\landmark_articles\Model;

/**
 * @file
 * Class ArticleListModel.
 */

/**
 * Class ArticleListModel.
 *
 * Representation of parameters required for article list queries.
 */
class ArticleListModel {

  private $itemsPerPage = 9;
  private $totalResults = 0;
  private $page = 0;
  private $pagination = TRUE;

  /**
   * Returns the number of tiles to display per page.
   *
   * @return int
   *   The page size.
   */
  public function getItemsPerPage() {
    return $this->isPagination() ? $this->itemsPerPage : NULL;
  }

  /**
   * Displays current page information and total number of results.
   *
   * @return string
   *   The label string.
   */
  public function getPaginationLabel() {
    $label = $this->totalResults;
    if (TRUE === $this->isPagination()) {
      $start = ($this->page * $this->itemsPerPage) + 1;
      $end = ($start + $this->itemsPerPage) - 1;
      if ($end > $this->totalResults) {
        $end = $this->totalResults;
      }
      $label = $start . ' - ' . $end . ' of ' . $this->totalResults;
    }
    return $label;
  }

  /**
   * Pagination getter.
   *
   * @return bool
   *   Whether or not pagination is enabled.
   */
  public function isPagination(): bool {
    return $this->pagination;
  }

  /**
   * Total results setter.
   *
   * @param int $total_results
   *   The total number of rows retrieved.
   */
  public function setNumResults(int $total_results) {
    $this->totalResults = $total_results;
  }

  /**
   * Page setter.
   *
   * @param int $page
   *   The current page.
   */
  public function setPage(int $page) {
    $this->page = $page;
  }

  /**
   * Pagination setter.
   *
   * @param bool $pagination
   *   Whether or not pagination is enabled.
   */
  public function setPagination(bool $pagination) {
    $this->pagination = $pagination;
  }

}
