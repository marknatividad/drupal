<?php

namespace Drupal\landmark_articles\Controller;

/**
 * @file
 * Class ArticleController.
 */

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Render\Renderer;
use Drupal\landmark_articles\Mapper\ArticleTagFilterMapper;
use Drupal\landmark_articles\Mapper\TweetsMapper;
use Drupal\landmark_articles\Model\ArticleListModel;
use Drupal\landmark_articles\Request\ArticleTagQueryParam;
use Drupal\landmark_helpers\Wrapper\FieldWrapper;
use Drupal\search_api\Entity\Index;
use Drupal\search_api\ParseMode\ParseModePluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class ArticleController.
 *
 * @package Drupal\landmark_articles\Controller
 */
class ArticleController extends ControllerBase {

  protected $entityTypeManager;
  protected $articleListModel;
  protected $articleTagFilterMapper;
  protected $articleTagQueryParam;
  protected $requestStack;
  protected $renderer;
  protected $parseModePluginManager;
  protected $tweetsMapper;
  protected $fieldWrapper;

  /**
   * ArticleController constructor.
   */
  public function __construct(EntityTypeManager $entityTypeManager, ArticleListModel $articleListModel, ArticleTagFilterMapper $articleTagFilterMapper, ArticleTagQueryParam $articleTagQueryParam, FieldWrapper $fieldWrapper, RequestStack $requestStack, Renderer $renderer, ParseModePluginManager $parseModePluginManager, TweetsMapper $tweetsMapper) {
    $this->entityTypeManager = $entityTypeManager;
    $this->articleListModel = $articleListModel;
    $this->articleTagFilterMapper = $articleTagFilterMapper;
    $this->articleTagQueryParam = $articleTagQueryParam;
    $this->fieldWrapper = $fieldWrapper;
    $this->requestStack = $requestStack;
    $this->renderer = $renderer;
    $this->parseModePluginManager = $parseModePluginManager;
    $this->tweetsMapper = $tweetsMapper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('landmark_articles.article_list_model'),
      $container->get('landmark_articles.article_tag_filter_mapper'),
      $container->get('landmark_articles.article_tag_query_param'),
      $container->get('landmark_helpers.field_wrapper'),
      $container->get('request_stack'),
      $container->get('renderer'),
      $container->get('plugin.manager.search_api.parse_mode'),
      $container->get('landmark_articles.tweets_mapper')
    );
  }

  /**
   * Returns the articles page render array.
   *
   * @return array
   *   The renderable array.
   */
  public function listArticles() {
    $resultCount = $this->getArticleNodes(TRUE);
    $articleNodesPaginated = $this->getArticleNodes();

    return [
      '#theme' => 'landmark_articles_list_page',
      '#search_config' => [
        'filters' => $this->getArticleFilters(),
      ],
      '#list' => $this->getRenderableArticlesList($articleNodesPaginated, $resultCount),
      '#tweets_config' => $this->getTweets(),
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * Returns the renderable articles list.
   *
   * @param array $articleNodesPaginated
   *   Array of article nodes.
   * @param int $resultCount
   *   The result count.
   *
   * @return array
   *   The renderable array.
   */
  public function getRenderableArticlesList(array $articleNodesPaginated, int $resultCount): array {
    $keys = $this->requestStack->getCurrentRequest()->query->get('keys');
    if (!empty($keys) && empty($articleNodesPaginated)) {
      return [
        'list' => [
          '#theme' => 'landmark_search_no_results',
        ],
      ];
    }

    $pager = [];
    $tiles = [];

    // Initialise pager.
    if ($this->articleListModel->isPagination() && $resultCount > 0) {
      pager_default_initialize($resultCount, $this->articleListModel->getItemsPerPage());
      $pager = ['#type' => 'pager'];
    }

    foreach ($articleNodesPaginated as $i => $articleNode) {
      $nodeViewBuilder = $this->entityTypeManager->getViewBuilder('node');
      $articleNodeView = $nodeViewBuilder->view($articleNode, 'tile_list');

      // Apply different tile layouts depending on a tile position.
      if ($i >= 0 && $i <= 2 || $i >= 4 && $i <= 6) {
        $articleNodeView['#no_image'] = TRUE;
      }
      elseif ($i == 3) {
        $articleNodeView['#horizontal'] = TRUE;
      }

      $tiles[] = $this->renderer->render($articleNodeView);
    }

    $renderable = [
      'list' => [
        '#theme' => 'landmark_articles_list_block',
        '#config' => [
          'tilesInRow' => [3, 1, 3, 2],
          'tiles' => $tiles,
          'paginationLabel' => $this->articleListModel->getPaginationLabel(),
        ],
        '#cache' => [
          'max-age' => 0,
        ],
      ],
      'pager' => $pager,
    ];

    return $renderable;
  }

  /**
   * Return article nodes.
   *
   * @param bool $return_count
   *   Whether or not to return the result count.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]|int
   *   An array of entity objects indexed by their IDs.
   *   Returns an empty array if no matching entities are found.
   *   If $return_count is set to TRUE, returns the number of results.
   */
  private function getArticleNodes($return_count = FALSE) {
    $articleNodes = [];

    $query = $this->getArticleQuery();

    // Set fulltext search keywords.
    $keys = $this->requestStack->getCurrentRequest()->query->get('keys');
    if (!empty($keys)) {
      $query->keys($keys);
    }

    // Search by tags if present.
    $article_tags = $this->articleTagQueryParam->getTermsNames();
    if (!empty($article_tags)) {
      $query->addCondition('name_3', $article_tags, 'IN');
    }

    /*
     * Article: Title
     * Article Banner: Text
     * 1 Column Text: Text
     * 2 Column Text: Text 1
     * 2 Column Text: Text 2
     * Article Tag: Name
     */
    $fields = [
      'title',
      'field_pg_articlebanner_text',
      'field_pg_text1col_text_1',
      'field_pg_text2col_text1',
      'field_pg_text2col_text2',
      'field_shared_tags_name',
    ];
    $query->setFulltextFields($fields);

    // Paginate the results.
    if (FALSE === $return_count) {
      $start = 0;
      $current_page = $this->requestStack->getCurrentRequest()->query->get('page');
      if (!empty($current_page) && intval($current_page) > 0) {
        $start = intval($current_page) * $this->articleListModel->getItemsPerPage();
      }
      $query->range($start, $this->articleListModel->getItemsPerPage());
    }

    // Execute the search.
    $results = $query->execute();
    if (TRUE === $return_count) {
      return $results->getResultCount();
    }

    $items = $results->getResultItems();

    /** @var \Drupal\search_api\Item\Item $item */
    foreach ($items as $item) {
      /** @var \Drupal\Core\Entity\Plugin\DataType\EntityAdapter $originalObject */
      $originalObject = $item->getOriginalObject();

      /** @var \Drupal\node\Entity\Node $node */
      $node = $originalObject->getValue();
      $articleNodes[] = $node;
    }

    return $articleNodes;
  }

  /**
   * Getter for the query.
   *
   * @return \Drupal\search_api\Query\Query
   *   The query object.
   */
  private function getArticleQuery() {
    $index = Index::load('acquia_search_index');
    /** @var \Drupal\search_api\Query\Query $query */
    $query = $index->query();

    // Change the parse mode for the search.
    $parse_mode = $this->parseModePluginManager->createInstance('direct');
    $parse_mode->setConjunction('OR');
    $query->setParseMode($parse_mode);

    // Add basic conditions.
    $query
      ->addCondition('status', 1)
      ->addCondition('type', 'article');
    $query->sort('created', 'DESC');

    return $query;
  }

  /**
   * Returns a unique set of taxonomy terms attached to the given articles.
   *
   * @return array
   *   Array of term entities keyed by their tid.
   */
  private function getArticleFilters(): array {
    $query = $this->getArticleQuery();
    $results = $query->execute();
    $items = $results->getResultItems();
    $tagTids = [];

    /** @var \Drupal\search_api\Item\Item $item */
    foreach ($items as $item) {
      /** @var \Drupal\Core\Entity\Plugin\DataType\EntityAdapter $originalObject */
      $originalObject = $item->getOriginalObject();

      /** @var \Drupal\node\Entity\Node $node */
      $node = $originalObject->getValue();
      $articleTags = $this->fieldWrapper->getFieldValue($node, 'field_shared_tags', FALSE);
      if (NULL !== $articleTags) {
        $tagTids = array_merge($tagTids, array_column($articleTags, 'target_id'));
      }
    }

    $tags = [];
    if (!empty($tagTids)) {
      $tagTids = array_unique($tagTids);

      $articleTagsTerms = $this->entityTypeManager
        ->getStorage('taxonomy_term')
        ->loadMultiple($tagTids);

      /** @var \Drupal\taxonomy\Entity\Term $articleTagsTerm */
      foreach ($articleTagsTerms as $articleTagsTerm) {
        $articleFilter = $this->articleTagFilterMapper->mapFromTerm($articleTagsTerm);
        $tags[$articleTagsTerm->getName()] = $articleFilter->toArray();
      }
    }

    // Sort filters by name.
    if (!empty($tags)) {
      ksort($tags);
    }

    return $tags;
  }

  /**
   * Gets config for the news.tweets macro.
   *
   * @return array
   *   Config array.
   */
  private function getTweets(): array {
    $lsg = $this->tweetsMapper->mapFromSiteSettings();
    return $lsg ? $lsg->toArray() : [];
  }

}
