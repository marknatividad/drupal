<?php

namespace Drupal\landmark_articles\QueryBuilder;

/**
 * @file
 * Class ArticleQueryBuilder.
 */

use Drupal\Core\Entity\Query\QueryInterface;

/**
 * Class ArticleQueryBuilder.
 *
 * @package Drupal\landmark_articles\QueryBuilder
 */
class ArticleQueryBuilder {

  private $query;
  private $sortField;
  private $sortDirection;
  private $tags = [];
  private $queryTag;
  private $limit;
  private $offset;
  private $excludedIds = [];

  /**
   * ArticleQueryBuilder constructor.
   */
  public function __construct(QueryInterface $query) {
    $this->query = $query;
  }

  /**
   * Setter function for sort.
   *
   * @param string $field
   *   Name of the field.
   * @param string $direction
   *   The sort direction.
   */
  public function setSort($field, $direction = 'ASC') {
    $this->sortField = $field;
    $this->sortDirection = $direction;
  }

  /**
   * Setter function for $tags.
   *
   * @param array $tags
   *   Array of article tag term IDs.
   */
  public function setTags(array $tags) {
    $this->tags = $tags;
  }

  /**
   * Setter function for query tag.
   *
   * @param string $tag
   *   The tag.
   */
  public function setQueryTag($tag) {
    $this->queryTag = $tag;
  }

  /**
   * Set the limit of the query.
   *
   * @param int $limit
   *   Up to how many items to return.
   */
  public function setLimit($limit) {
    $this->limit = $limit;
  }

  /**
   * Set the offset of the query.
   *
   * @param int $offset
   *   Offset x number of items from the query.
   */
  public function setOffset($offset) {
    $this->offset = $offset;
  }

  /**
   * Setter function for $excludedIds.
   *
   * @param array $excludedIds
   *   The array of NIDs to be excluded.
   */
  public function setExcludedIds(array $excludedIds) {
    $this->excludedIds = $excludedIds;
  }

  /**
   * Generates query for retrieving articles.
   */
  public function generateQuery() {
    $this->query
      ->condition('type', 'article')
      ->condition('status', 1);

    if (!empty($this->tags)) {
      $this->query
        ->condition('field_shared_tags', $this->tags, 'IN');
    }

    if (!empty($this->excludedIds)) {
      $this->query
        ->condition('nid', $this->excludedIds, 'NOT IN');
    }

    if (!empty($this->sortField) && !empty($this->sortDirection)) {
      $this->query
        ->sort($this->sortField, $this->sortDirection);
    }

    if ($this->limit) {
      if ($this->offset) {
        $this->query->range($this->offset, $this->limit);
      }
      else {
        $this->query->range(0, $this->limit);
      }
    }

    if (!empty($this->queryTag)) {
      $this->query
        ->addTag($this->queryTag);
    }

    return $this->query;
  }

}
