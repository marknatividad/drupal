<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
 
/*
 * Fields Available (please update once changed)
 *  [nid] ==  Content: Nid
 *  [field_image] == (Referenced Products) Field: Image
 *  [field_brand] == Content: Brand 
 *  [php] == (Referenced Products) Commerce Product: From Price
 *  [php_1] == (Referenced Products) Commerce Product: Linen Lover's Price
 *  [php_2] == Content: Path
 *  [title] == Content: Title 
 *  [field_rating] == Field: Rating
 */
?>

<div class="row-fluid">

  <!-- Main Column -->
  <div class="span12">
  
    <div class="span3">
      <?php print $fields['field_image']->content; ?>
    </div><!-- /span4 -->
    
    <div class="span9">
      <?php print $fields['field_brand']->content; ?>
      
      <?php print $fields['title']->content; ?>
      
      <div class="field-price">
        <span>From <?php print $fields['php']->content; ?></span>
        <span>|</span>
        <span>Linen Lovers <?php print $fields['php_1']->content; ?></span>
      </div>
      
      <?php print $fields['field_rating']->content; ?>
      
      <div>
        <a href="/<?php print $fields['php_2']->content; ?>" class="btn">Product Information</a>
        <a href="#" class="btn">Write a Review</a>
      </div>
    </div><!-- /span8 -->
  
  </div><!-- /Main Column -->

</div>
