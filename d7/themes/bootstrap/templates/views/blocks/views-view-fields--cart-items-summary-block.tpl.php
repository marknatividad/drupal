<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 *
 * Cart Fields Available (please update once changed)
 *  [commerce_display_path] == Commerce Line item: Display path
 *  [field_image] == Field: Image
 *  [line_item_title] == Commerce Line Item: Title
 *  [field_style] == Commerce Product: Style
 *  [field_colour] == Commerce Product: Colour
 *  [quantity] == Commerce Line Item: Quantity
 *  [commerce_total] == Commerce Line item: Total
 *  [commerce_total-amount] == Raw amount
 *  [commerce_total-currency_code] == Raw currency_code
 *  [commerce_total-data] == Raw data
 *
 *  @IMPORTANT:
 *    Bootstrap default scaffolding system is in use at the moment.
 *    Once scaffolding/grid system is confirmed, replace 'span<X>'
 *    classes.
 */

?>

<div class="row-fluid cart-items-summary-row">

  <!-- Main Column -->
  <div class="span12">

    <!-- Column 1 -->
        <!-- Inline Column 1.1 -->
        <div class="span3">
          <span class="cart-fields-column">
            <div>
              <?php print $fields['field_image']->content ?>
            </div>
          </span>
        </div>


        <!-- Inline Column 2.1-->
        <div class="span9">
          <span class="cart-fields-column">
            <div>
              <?php print $fields['line_item_title']->content ?>
            </div>
            <div>
              <?php print $fields['field_style']->label_html ?>
              <?php print $fields['field_style']->content ?>
            </div>
            <span>
              <?php print $fields['field_colour']->label_html ?>
              <?php print $fields['field_colour']->content ?>
              <?php print $fields['quantity']->separator ?>
            </span>
            <span>
              <?php print $fields['quantity']->label_html ?>
              <?php print $fields['quantity']->content ?>
              <?php print $fields['quantity']->separator ?>
            </span>
            <span>
              <?php print $fields['commerce_total']->label_html ?>
              <?php print $fields['commerce_total']->content ?>
            </span>
          </span>
        </div>

  </div>

</div>