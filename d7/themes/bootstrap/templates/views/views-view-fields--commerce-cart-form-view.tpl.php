<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */

/*
 * Cart Fields Available (please update once changed)
 *  [commerce_display_path] == Commerce Line item: Display path (hidden)
 *  [field_image] == Field: Image
 *  [edit_delete] == Commerce Line Item: Delete button
 *  [line_item_title] == Commerce Line Item: Title
 *  [field_colour] == Commerce Product: Colour
 *  [field_style] == Commerce Product: Colour
 *  [commerce_unit_price] == Commerce Line item: Unit price
 *  [edit_quantity] == Commerce Line Item: Quantity text field
 *  [commerce_total] == Commerce Line item: Total
 *  [field_sale_price] == Commerce Product: Sale Price
 *  [commerce_price] == Commerce Product: Price
 *  [php_1] == Product Coupon Line Item
 *
 *  @IMPORTANT:
 *    Bootstrap default scaffolding system is in use at the moment.
 *    Once scaffolding/grid system is confirmed, replace 'span<X>'
 *    classes.
 */

  $line_through = ($row->field_field_sale_price != null) ? 'line-through' : '';
?>

<div class="row-fluid cart-fields-row">

  <!-- Main Column -->
  <div class="span12">

    <!-- Column 1 -->
    <div class="span5">
        <!-- Inline Column 1.1 -->
        <span class="cart-fields-column">
          <div>
            <?php print $fields['field_image']->content ?>
          </div>
        </span>

        <!-- Inline Column 1.2 -->
        <span class="cart-fields-column" style="width: 150px;">
          <div>
            <?php print $fields['line_item_title']->content ?>
          </div>
          <div>
            <?php print $fields['field_colour']->label_html ?>
            <?php print $fields['field_colour']->content ?>
          </div>
          <div>
            <?php print $fields['field_style']->label_html ?>
            <?php print $fields['field_style']->content ?>
          </div>
          <div>
            <?php print $fields['edit_delete']->content ?>
          </div>
        </span>
    </div>

    <!-- Column 2 -->
    <div class="span4">
      <span class="cart-fields-column">
        <?php print $fields['commerce_price']->label_html ?>
        <span class="<?php print $line_through ?>"><?php print $fields['commerce_price']->content ?></span>
      </span>
      <span class="cart-fields-column">
        <?php print $fields['field_sale_price']->label_html ?>
        <?php print $fields['field_sale_price']->content ?>
      </span>
      <span class="cart-fields-column">
        <?php print $fields['php']->label_html ?>
        <?php print $fields['php']->content ?>
      </span>
    </div>

    <!-- Column 3 -->
    <div class="span3">
        <!-- Inline Column 3.1 -->
        <span class="cart-fields-column">
          <div>
            <?php print $fields['edit_quantity']->label_html ?>
            <?php print $fields['edit_quantity']->content ?>
          </div>
        </span>
        <!-- Inline Column 3.2 -->
        <span class="cart-fields-column">
          <div>
            <?php print $fields['commerce_total']->label_html ?>
            <?php print $fields['commerce_total']->content ?>
          </div>
        </span>
    </div>

  </div>
  
  <div class="span12">
    <?php print $fields['php_1']->content; ?>
  </div>

</div>


