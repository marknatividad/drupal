<?php
/**
 * @file
 * Default theme implementation to display a commerce product in
 * the "We also love" section.
 */

/**
 * Fields available (please update once changed)
 *
 * $content['product_display']->field_brand == Product Display: Brand
 * $content['product_display']->title == Product Display: Title
 * $content['product_display']->field_rating == Product Display: Rating
 * $content[title] == Commerce Line Item: Title
 * $content[field_image] == Commerce Line Item: Image
 * $content[sku] == Commerce Line Item: SKU
 * $content[status] == Commerce Line Item: Status
 * $content['field_sale_price'] == Commerce Line Item: Sale Price
 * $content['commerce_price'] == Commerce Line Item: RRP Price
 */
?>

<div class="commerce-product-node-teaser">
  <div class="views-field views-field-image">
    <a href="<?php echo url('node/' . $content['product_display']->nid) . '#item-' . $commerce_product->product_id; ?>">
      <?php print render($content['field_image'][0]); ?>
    </a>
  </div>
  <div class="views-field views-field-brand"><?php print $content['product_display']->field_brand[0]['#title']; ?></div>
  <div class="views-field views-field-title"><?php print $content['product_display']->title; ?></div>
  <div class="views-field views-field-commerce-price">
    <?php if (isset($content['field_sale_price'])): ?>
      <?php print render($content['field_sale_price']); ?>
    <?php else: ?>
      <?php print render($content['commerce_price']); ?>
    <?php endif; ?>
  </div>
  <div class="views-field views-field-rating"><?php print render($content['product_display']->field_rating); ?></div>
</div>
