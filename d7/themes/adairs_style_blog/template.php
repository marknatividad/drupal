<?php

function adairs_preprocess_page(&$vars, $hook) {
  if (!empty($variables['node'])) {
    $variables['theme_hook_suggestions'][] = 'page__node__' . $variables['node']->type;
  }
}


/**
 * Implements theme_preprocess_link
 */
function adairs_preprocess_link(&$vars) {
  // Taxonomy terms need their seo_id class
  // as well as path rewritten to views listing instead of taxonomy term listing
  if (@$vars['options']['entity_type'] == 'taxonomy_term') {
    $term = $vars['options']['entity'];
    $term_seo_id = @$term->field_seo_id['und'][0]['value'];

    if (!empty($term_seo_id)) {
      $vars['path'] = "blog/$term_seo_id";
      $vars['options']['attributes']['class'] = array($term_seo_id);
    }
  }
}
