<div class='page-wrapper'>
  <div class='site-switcher'>
    <div class='container'>
      <a href='' class='site-blog selected'>adairs Blog</a><!--
      --><a href='' class='site-1'>adairs</a><!--
      --><a href='' class='site-2'>adairs Kids</a>
    </div>
  </div><!-- /site-switcher -->

  <div class='page-header-wrapper'>
    <div id='page-header' class='container'>
      <div class='page-logo'>
        <a href='<?php print $base_path ?>'><img src='<?php print $logo ?>' alt='<?php if(isset($site_name_and_slogan)) { print $site_name_and_slogan; } ?>' title='<?php if(isset($site_name_and_slogan)) { print $site_name_and_slogan; } ?>' /></a>

        <?php //if ($page['header']): ?>
        <!--
          <div id='header'>
            <div class='limiter clearfix'>
              <?php //print render($page['header']); ?>
            </div>
          </div>
        -->
        <?php //endif; ?>
  <!--
        <div id='branding'>
          <div class='limiter clearfix'>
           <?php //if ($site_name): ?><h1 class='site-name'><?php //print $site_name ?></h1><?php //endif; ?>
          </div>
        </div>
  -->
      </div><!-- /page-logo -->
      <div class='page-nav-wrapper'>
        <a class='menu-icon'>Menu</a>
        <?php print render($page['navigation']); ?>
        <?php //if (isset($main_menu)) : ?>
          <?php //print theme('links', array('links' => $main_menu, 'attributes' => array('class' => 'main-menu'))) ?>
        <?php //endif; ?>
      </div><!-- /page-nav-wrapper -->
    </div><!-- /page-header -->
  </div><!-- /page-header-wrapper -->

  <div class='page-body-wrapper'>
    <div id='page-body' class='container'>
      <div class='page-body-main'>


        <?php if ($page['help'] || ($show_messages && $messages)): ?>
          <div id='console'><div class='limiter clearfix'>
            <?php print render($page['help']); ?>
            <?php if ($show_messages && $messages): print $messages; endif; ?>
          </div></div>
        <?php endif; ?>

        <?php //if ($breadcrumb) print $breadcrumb; ?>
        <?php if ($primary_local_tasks): ?><ul class='action-btn-group clearfix'><?php print render($primary_local_tasks) ?></ul><?php endif; ?>
        <?php if ($secondary_local_tasks): ?><ul class='clearfix'><?php print render($secondary_local_tasks) ?></ul><?php endif; ?>
        <?php if ($action_links): ?><ul class='clearfix'><?php print render($action_links); ?></ul><?php endif; ?>
        <div id='content' class='clearfix'><?php print render($page['content']) ?></div>

      </div><!-- /page-body-main -->

      <div class='page-body-sidebar'>

        <?php if ($page['sidebar_second']): ?>
          <div id='page-sidebar' class='clearfix'><?php print render($page['sidebar_second']) ?></div>
        <?php endif; ?>

      </div><!-- /page-body-sidebar -->
      <div class='clearfix'></div>
    </div><!-- /page-body -->
  </div><!-- /page-body-wrapper -->

  <div class='page-footer-wrapper'>
    <div id='page-footer' class='container'>
      <?php //print $feed_icons ?>
      <div class='footer-block-group'>
        <?php print render($page['footer_left']); ?>
        <?php print render($page['footer_center']); ?>
        <?php print render($page['footer_right']); ?>
<!--
      <ul class='footer-block-group'>
        <li class='block-1'>
          <h2>Store Locator</h2>
          <hr />
          <a href="#">Find your nearest Adairs store ></a>
        </li>
        <li class='block-2'>
          <h2>Shop Adairs Online</h2>
          <hr />
          <a href="#">Shop Now ></a>
        </li>
        <li class='block-3'>
          <h2>Connect With Adairs</h2>
          <hr />
          <a href="#" rel='nofollow' target="_blank"><img src="/sites/all/themes/adairs/images/icons/icon-facebook-s1.png" alt="" /></a>
          <a href="#" rel='nofollow' target="_blank"><img src="/sites/all/themes/adairs/images/icons/icon-pinster-s1.png" alt="" /></a>
          <a href="#" rel='nofollow' target="_blank"><img src="/sites/all/themes/adairs/images/icons/icon-youtube-s1.png" alt="" /></a>
        </li>
      </ul>
-->
      </div><!-- /footer-block-group -->
      <?php print render($page['footer']) ?>
    </div><!-- /page-footer -->
  </div><!-- /page-footer-wrapper -->
</div><!-- /page-wrapper -->
