
<?php if (!empty($pre_object)) print render($pre_object) ?>

<div class='<?php print $classes ?> clearfix' <?php print ($attributes) ?>>
  <?php if (!empty($title_prefix)) print render($title_prefix); ?>

  <?php if (!empty($title_suffix)) print render($title_suffix); ?>

  <?php if (!empty($submitted)): ?>
    <div class='<?php print $hook ?>-submitted clearfix'> <?php //print $submitted; ?> </div>
  <?php endif; ?>

  <?php if (!empty($content)): ?>
    <div class='<?php print $hook ?>-content clearfix <?php if (!empty($is_prose)) print 'prose' ?>'>
      <div class='article-img'><?php print render($content['field_image']) ?></div>
      <h1 class='title'><?php print $title ?></h1>
      <div class='article-info'>
        <div class='category'><?php print render($content['field_category']) ?></div>
        <div class='submitted-by'><?php print render($content['submitted_by']) ?></div>
      </div><!-- /article-info -->
      <div class='tags'><?php print render($content['field_tags']) ?></div>
      <?php print render($content) ?>
    </div>
  <?php endif; ?>
<!--
  <?php //if (!empty($links)): ?>
    <div class='<?php //print $hook ?>-links clearfix'>
      <?php //print render($links) ?>
    </div>
  <?php //endif; ?>
-->
</div>

<?php if (!empty($post_object)) print render($post_object) ?>

