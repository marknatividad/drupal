(function ($) {
  $(document).ready(function() {
    $('.menu-icon').on("click", function(){
      //alert(JQuery.jquery);
      $('.menu:first').slideToggle('fast');
    });

    $(window).resize(function() {
      if($(window).width() > 480) // or 768
        $('.menu:first').show();
    });
  });
})(jQuery);
