<?php
/**
 * @file
 * Default theme implementation to display a commerce product in
 * the "We also love" section.
 */

/**
 * Fields available (please update once changed)
 *
 * $commerce_product == Commerce Product object
 * $content['product_display']->field_brand == Product Display: Brand
 * $content['product_display']->title == Product Display: Title
 * $content['product_display']->field_rating == Product Display: Rating
 * $content[title] == Commerce Line Item: Title
 * $content[field_image] == Commerce Line Item: Image
 * $content[sku] == Commerce Line Item: SKU
 * $content[status] == Commerce Line Item: Status
 * $content['field_sale_price'] == Commerce Line Item: Sale Price
 * $content['commerce_price'] == Commerce Line Item: RRP Price
 */

$commerce_product_wrapper = entity_metadata_wrapper('commerce_product', $commerce_product);
$colour = $commerce_product_wrapper->field_colour->name->value();
$base_url = url('node/' . $content['product_display']->nid);
$product_url = format_string("$base_url?colour=@colour&style=@style", array(
  '@colour' => $commerce_product_wrapper->field_colour->name->value(),
  '@style' => $commerce_product_wrapper->field_style->tid->value()
));

?>

<div class="commerce-product-node-teaser">
  <div class="views-field views-field-image">
    <a href="<?php echo $product_url; ?>">
      <?php print render($content['field_image'][0]); ?>
    </a>
    <?php print render($content['ctools_modal_link']); ?>
  </div>
  <div class="views-field views-field-brand"><?php print $content['product_display']->field_brand[0]['#title']; ?></div>
  <div class="views-field views-field-title"><?php print $content['product_display']->title; ?></div>
  <div class="views-field views-field-style"><?php print $commerce_product_wrapper->field_style->name->value(); ?></div>
  <div class="views-field views-field-commerce-price">
    <?php if (isset($content['field_sale_price'])): ?>
      <?php print render($content['field_sale_price']); ?>
    <?php else: ?>
      <?php print render($content['commerce_price']); ?>
    <?php endif; ?>
  </div>
  <div class="views-field views-field-rating"><?php print render($content['product_display']->field_rating); ?></div>
</div>
