<div id="wishlist-item-added">
  <?php echo t('<span>Already in <a class="in-wishlist" target="_blank" href="@url">wishlist</a></span>', array('@url' => $url)); ?>
</div>