<?
// Min or Max price of target
function getTargetPrice($array, $targetFlag) {

  $target = current($array);

  // Get the lowest price in the array
  foreach ($array as $item) {
    // Strip $
    $itemPrice = str_replace('$', '', $item['rrp']);
    $targetPrice = str_replace('$', '', $target['rrp']);

    if($targetFlag == 'max') {
      // Assess Max
      if($itemPrice > $targetPrice) $target = $item;
    } elseif($targetFlag == 'min') {
      // Assess Min
      if($itemPrice < $targetPrice) $target = $item;
    }
  }
  return $target;
}

// Create description
$offerType = 'itemtype="http://schema.org/';
$offerType .= (count($styles) > 1) ? 'AggregateOffer"' : 'Offer"';
// print $offerType;

// Work out itemprop text
if(count($styles) > 1) {
  // Use 'lowPrice' and 'highPrice' schema
  $maxPriceProductId = getTargetPrice($styles, 'max');
  $minPriceProductId = getTargetPrice($styles, 'min');

} else {
  // Use 'price' schema
  $arrayItem = current($styles);
  $priceSchema = $arrayItem['rrp'];
}
?>

<? if ($page_variant == 'a'): ?>
  <div class="custom-dd" itemprop="offers" itemscope <?php print $offerType; ?>>
    <div class="custom-dd-header">
      Please select a size:
    </div>
    <div class="custom-dd-body">
      <?php foreach ($styles as $tid => $name) {
        $classes = 'custom-dd-size';
        // If this is the selected colour, add a class
        $classes .= ($style_selected == $tid) ? ' custom-dd-size-selected' : '' ;
        print "<div class='$classes' data-tid='$tid'>";
        print "<span>$name</span>";
        print '</div>';
      } ?>
    </div>
  </div>
<? endif; ?>

<? if ($page_variant == 'b'): ?>
  <div class="custom-dd" itemprop="offers" itemscope <?php print $offerType; ?>>
    <table>
      <thead class="custom-dd-header">
        <tr>
          <th class='align-left' colspan="2">SELECT STYLE</th>
          <th class="align-right">RRP</th>
          <th class="align-right">SALE</th>
          <th class="align-right">LL</th>
        </tr>
      </thead>
      <tbody class="custom-dd-body">
       <?php foreach ($styles as $tid => $value) {
        // Loop each product style
        $selected = ($style_selected == $tid) ? ' checkcircle-selected' : '' ;
        // $itemprop = 'itemprop="' . $value['itemprop'] . '"';

        print "<tr class='custom-dd-size'>";
        print "<td class='checkcircle $classes $selected' data-tid='$tid'>&nbsp;</td>";
        print "<td class='align-left'>{$value['name']}</td>";
        print "<td class='align-right $line_through'>{$value['rrp']}</td>";
        print "<td class='align-right'>{$value['sale']}</td>";
        print "<td class='align-right sale-item'>{$value['llp']}</td>";
        print "</tr>";
      } ?>
      <?php if(count($styles) > 1) { ?>
        <meta itemprop="offerCount" content="<?php print count($styles); ?>" />
        <meta itemprop="lowPrice" content="<?php print $minPriceProductId['rrp']; ?>" />
        <meta itemprop="highPrice" content="<?php print $maxPriceProductId['rrp']; ?>" />
      <?php } else { ?>
        <meta itemprop="price" content="<?php print $priceSchema; ?>" />
      <?php } ?>
      </tbody>
    </table>
  </div>
<? endif; ?>
