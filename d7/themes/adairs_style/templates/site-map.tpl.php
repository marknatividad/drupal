<?php

/**
 * @file site-map.tpl.php
 *
 * Theme implementation to display the site map.
 *
 * Available variables:
 * - $message:
 * - $rss_legend:
 * - $front_page:
 * - $blogs:
 * - $books:
 * - $menus:
 * - $faq:
 * - $taxonomys:
 * - $additional:
 *
 * @see template_preprocess()
 * @see template_preprocess_site_map()
 */
?>

<?php
  // $handle = '<h3>CLICK ME</h3>';
  // $output = 'Output here';
  // $options = array('handle' => $handle, 'content' => $output, 'collapsed' => TRUE);
  // print theme('onq_core_collapsible', $options);

  //dpm($taxonomys);
?>

<div id="site-map">
  <?php if($message): ?>
    <div class="site-map-message">
      <?php print $message; ?>
    </div>
  <?php endif; ?>

  <?php if($rss_legend): ?>
    <div class="site-map-rss-legend">
      <?php print $rss_legend; ?>
    </div>
  <?php endif; ?>

  <?php if($front_page): ?>
    <div class="site-map-front-page">
      <?php print $front_page; ?>
    </div>
  <?php endif; ?>

  <?php if($blogs): ?>
    <div class="site-map-blogs">
      <?php print $blogs; ?>
    </div>
  <?php endif; ?>

  <?php if($books): ?>
    <div class="site-map-books">
      <?php print $books; ?>
    </div>
  <?php endif; ?>

  <?php if($menus): ?>
    <div class="site-map-menus">
      <?php print $menus; ?>
    </div>
  <?php endif; ?>

  <?php if($faq): ?>
    <div class="site-map-faq">
      <?php print $faq; ?>
    </div>
  <?php endif; ?>

  <?php if($taxonomys): ?>
    <div class="site-map-taxonomys">
      <?php print $taxonomys; ?>
    </div>
  <?php endif; ?>

  <?php if($additional): ?>
    <div class="site-map-additional">
      <?php print $additional; ?>
    </div>
  <?php endif; ?>
</div>

<ul>
  <li>Other Information
  <ul class="menu">
    <li><a href="/" title="">Adairs Kids</a></li>
    <li><a href="/about-us">About Us</a></li>
    <li><a href="/" title="">Delivery &amp; Returns Policy</a></li>
    <li><a href="/" title="">Gift Cards</a></li>
    <li><a href="/store_locator">Store Locator</a></li>
    <li><a href="/" title="">Sitemap</a></li>
    <li><a class="overlay-ajax" href="/privacy-security-policy">Privacy &amp; Security Policy</a></li>
    <li><a href="/contact-us">Contact Us</a></li>
    <li><a href="/careers">Careers</a></li>
  </ul>
  </li>
</ul>

