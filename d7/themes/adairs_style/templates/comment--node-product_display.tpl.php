<div class="<?php print $classes . ' ' . $zebra; ?>"<?php print $attributes; ?>>

  <div>
    <?php print render($title_prefix); ?>
    <?php if ($title): ?>
      <h3<?php print $title_attributes; ?>>
        <?php print $title; ?>
        <?php if ($new): ?>
          <span class="new label label-important"><?php print $new; ?></span>
        <?php endif; ?>
      </h3>
    <?php elseif ($new): ?>
      <span class="new label label-important"><?php print $new; ?></span>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
  </div>

  <?php
    // We hide the comments and links now so that we can render them later.
    hide($content['links']);
  ?>

  <div>
    <span><?php print render($content['field_rating']); ?></span>
    <span class="submitted"><?php print $submitted; ?></span>
  </div>
  <div>
    <?php print render($content['comment_body']); ?>
  </div>

  <?php if ($signature): ?>
    <div class="user-signature clearfix">
      <?php print $signature; ?>
    </div>
  <?php endif; ?>

  <?php //print render($content['links']) ?>
</div> <!-- /.comment -->
