<div class='page-wrapper'>

  <div class='site-switcher'>
    <div class='container'>
      <div class="site-options">
        <a href='https://www.adairs.com.au/' class='site-1 <?php print adairs_core_kids_current_path_is_adairs_kids() ? "" :"selected";?>'>adairs</a>
        <a href='https://www.adairs.com.au/adairs-kids' class='site-2 <?php print adairs_core_kids_current_path_is_adairs_kids() ? "selected" : "";?>'>adairs kids</a>
        <a href='http://blog.adairs.com.au/' class='site-blog'>adairs blog</a>
      </div>
      <div class="phone-info">Customer Hotline: <a href="tel:1300 783 005">1300 783 005</a><span>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;Free Returns</span></div>
    </div>
  </div><!-- /site-switcher -->

  <div class='subscription-top'>
    <div class='container'>
      <?php print render($page['header']); ?>
    </div>
  </div>

  <div class='page-header-wrapper'>
    <div id='page-header' class='container'>
      <div itemscope="" itemtype="http://schema.org/Organization" class='page-logo'>
        <a href="<?php print $adairs_logo_url; ?>" itemprop="url"><img src='<?php print $logo ?>' alt='Adairs Logo' title="Adairs Logo" itemprop="logo" /></a>
      </div><!-- /page-logo -->
      <?php print render($page['navigation']); ?>
      <div class='page-nav-wrapper'>
        <!--<a class='menu-icon'>Menu</a>-->
        <?php if (!empty($primary_nav)): ?>
          <?php print render($primary_nav); ?>
        <?php endif; ?>

        <div class="search-mobile-switch"></div>

        <?php if(!empty($page['search'])): ?>
          <?php print render($page['search']); ?>
        <?php endif; ?>
        <div class="clear"></div>
      </div><!-- /page-nav-wrapper -->
      <div class="page-nav-under"></div>
    </div><!-- /page-header -->
  </div><!-- /page-header-wrapper -->

  <div class='page-body-wrapper'>
    <div id='page-body' class='container'>
      <div class='page-body-main'>

        <?php if ($page['help'] || ($show_messages && $messages)): ?>
          <div id='console'><div class='limiter clearfix'>
            <?php print render($page['help']); ?>
            <?php if ($show_messages && $messages): print $messages; endif; ?>
          </div></div>
        <?php endif; ?>

        <?php if (!empty($category_banner)): ?>
          <div class="banner clearfix"><?php print $category_banner; ?></div>
        <?php endif; ?>

        <?php if (!empty($banner_image)): ?>
          <?php print render($banner_image); ?>
        <?php endif; ?>

        <?php if ($breadcrumb): ?>
          <?php print $breadcrumb; ?>
        <?php endif; ?>
        <div class="page-title-wrapper">
        <?php print render($title_prefix); ?>
        <?php if ($title): ?><h1 class='page-title'><?php print $title ?></h1><?php endif; ?>
        <span class="page-title-suffix"><?php print render($title_suffix); ?></span>
        </div>
        <?php if($is_admin):?>
          <?php if ($primary_local_tasks): ?><ul class='action-btn-group clearfix'><?php print render($primary_local_tasks) ?></ul><?php endif; ?>
        <?php endif;?>
        <?php if ($secondary_local_tasks): ?><ul class='clearfix'><?php print render($secondary_local_tasks) ?></ul><?php endif; ?>
        <?php if ($action_links): ?><ul class='clearfix'><?php print render($action_links); ?></ul><?php endif; ?>
        <div class="page-body-left">
          <?php print render($page['sidebar_left']) ?>
        </div>
        <div class="page-body-center">
          <?php
            if (empty($body)) {
              print render($page['content']);
            } else {
              print array_key_exists('sharethis_sharethis_block', $page['content']) ? render($page['content']['sharethis_sharethis_block']) : '';
              print render($body);
            }
          ?>
        </div>
        <div class="page-body-right">
          <?php print render($page['sidebar_right']) ?>
          <?php print render($page['sidebar_right_lower']) ?>
        </div>

      </div><!-- /page-body-main -->

      <div class='clearfix'></div>
    </div><!-- /page-body -->
    <div class="container">
      <?php if ($page['content_bottom']) { print render($page['content_bottom']); } ?>

    </div><!-- /container -->
  </div><!-- /page-body-wrapper -->

  <div class='page-footer-wrapper'>
    <div id='page-footer' class='container'>
      <div class='footer-top'>

        <?php if ($page['footer_left']) { print render($page['footer_left']); } ?>
        <?php if ($page['footer_center']) { print render($page['footer_center']); } ?>
        <?php if ($page['footer_right']) { print render($page['footer_right']); } ?>

      </div><!-- /footer-top -->
      <div class='footer-bottom'>
        <?php print render($page['footer']) ?>
      </div><!-- /footer-botom -->
    </div><!-- /page-footer -->
  </div><!-- /page-footer-wrapper -->
</div><!-- /page-wrapper -->
