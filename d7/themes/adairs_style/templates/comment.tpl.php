<div class="<?php print $classes . ' ' . $zebra; ?>"<?php print $attributes; ?>>

  <div>
    <p class="submitted">
      <?php print $picture; ?>
      <?php print $submitted; ?>
      <?php print $permalink; ?>
    </p>

    <?php print render($title_prefix); ?>
    <?php if ($title): ?>
      <h3<?php print $title_attributes; ?>>
        <?php print $title; ?>
        <?php if ($new): ?>
          <span class="new label label-important"><?php print $new; ?></span>
        <?php endif; ?>
      </h3>
    <?php elseif ($new): ?>
      <span class="new label label-important"><?php print $new; ?></span>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
  </div>

  <?php
    // We hide the comments and links now so that we can render them later.
    hide($content['links']);
    print render($content);
  ?>

  <?php if ($signature): ?>
    <span class="user-signature clearfix">
      <?php print $signature; ?>
    </span>
  <?php endif; ?>

  <?php print render($content['links']) ?>
</div> <!-- /.comment -->
