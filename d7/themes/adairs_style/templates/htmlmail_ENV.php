<?php

// Testing to get around the .htpasswd issue
$env = 'https:///test.adairs.onqtesting.com.au';
global $base_root;

if($base_root == $env) {
  // Use local DNS for testing
  $path = 'http://adairs.prod' . base_path() . path_to_theme() . '/images/email/';
} else {
  $path = $base_root . base_path() . path_to_theme() . '/images/email/';
}

?>