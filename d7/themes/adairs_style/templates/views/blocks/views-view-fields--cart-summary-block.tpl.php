<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>
<div class="cart-product-img">
  <?php print $fields['field_image']->content; ?>
  <div class="del-btn">  <?php print onq_core_modal_link('', "cart/ajax/remove/" . $view->row_index, 'adairs-commerce-cart-remove delete-summary-line-item-link'); ?>
    <?php print $fields['edit_delete']->content; ?></div>
</div><!-- /cart-product-img -->
<div class="cart-product-desc">
  <div class="cart-product-desc-title">
    <?php print $fields['line_item_title']->content; ?>
  </div>
  <div class="cart-product-desc-style">
    <?php print $fields['field_style']->label_html; ?>
    <?php print $fields['field_style']->content; ?>
  </div>
  <div class="cart-product-desc-colour">
    <?php print $fields['field_colour']->label_html; ?>
    <?php print $fields['field_colour']->content; ?>
  </div>
  <div class="cart-product-desc-quantity">
    <?php print $fields['quantity']->label_html; ?>
    <?php print $fields['quantity']->content; ?>
  </div>
  <div class="cart-product-desc-total">
    <?php print $fields['commerce_total']->content; ?>
  </div>

</div><!-- /cart-product-img -->
