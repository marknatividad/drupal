<?php
/**
 * @file
 * Default simple view template to display a commerce product image field.
 */

/**
 * Fields available (please update once changed)
 *
 * $fields['field_image_1'] == Commerce Product: Image
 */
?>
<?php print render($fields['field_image_1']->content); ?>
