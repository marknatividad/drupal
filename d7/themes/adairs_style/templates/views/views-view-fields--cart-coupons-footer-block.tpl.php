<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */

/**
 * Coupon Fields Available (please update once changed)
 *
 * [type] == Commerce Coupon: Type
 * [commerce_coupon_code] == Commerce Coupon: Coupon Code
 * [commerce_unit_price] == Commerce Line Item: Unit Price
 * [remove_from_order] == Commerce Coupon: Remove from Order
 */
?>


<div class="left">
  <?php print $fields['commerce_coupon_code']->content; ?>
  <?php print $fields['commerce_unit_price']->content; ?>
</div>
<div class="right"><?php print $fields['remove_from_order']->content; ?></div>

<?php

  // print '<pre>';
  // print dpm($fields['remove_from_order']->content);
  // print '</pre>';
?>

<!-- <div class="views-field">
  <div class="float-left">
    <?php //print $fields['commerce_coupon_code']->content; ?>
    <?php //print $fields['type']->content; ?>
  </div>
  <div class="float-right">
    <span><?php //print $fields['commerce_unit_price']->content; ?></span>
  </div>
</div>
<div class="coupon-remove-button">
  <?php //print $fields['remove_from_order']->content; ?>
</div> -->
