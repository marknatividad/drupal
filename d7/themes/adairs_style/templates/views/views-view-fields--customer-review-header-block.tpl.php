<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */

/*
 * Fields Available (please update once changed)
 *  [nid] ==  Content: Nid
 *  [field_image] == (Referenced Products) Field: Image
 *  [field_brand] == Content: Brand
 *  [php] == (Referenced Products) Commerce Product: From Price
 *  [php_1] == (Referenced Products) Commerce Product: Linen Lover's Price
 *  [php_2] == Content: Path
 *  [php_3] == Customer Review Modal Form
 *  [title] == Content: Title
 *  [field_rating] == Field: Rating
 */

//dpm($variables);
?>
<div class="product-info-review-block">

  <div class="product-img">
    <?php print $fields['field_image']->content; ?>
  </div><!-- /span4 -->

  <div class="product-desc">
    <div class="field-brand"><?php print $fields['field_brand']->content; ?></div>
    <div class="field-title"><?php print $fields['title']->content; ?></div>
    <div class="field-price">
      <span>From <?php print $fields['php']->content; ?></span>
      <span>Linen Lovers <?php print $fields['php_1']->content; ?></span>
    </div>

    <?php print $fields['field_rating']->content; ?>
  </div><!-- /product-desc -->

  <div class="button-group">
    <a href="/<?php print $fields['php_2']->content; ?>" class="btn btn-greyl" id="review-product-information"><img src="/sites/all/themes/adairs_style/images/icons/icon-review-product-25-25.png" alt="Info"/> Product Information</a>
    <div class="review-btn" id="review-write">
      <?php if(!(is_null($fields['php_3']->content) || empty($fields['php_3']->content))){?>
      <a href="#" class="btn btn-greyl" id="write_review_btn"><img src="/sites/all/themes/adairs_style/images/icons/icon-review-write-25-25.png" alt="Write"/>&nbsp;Write a Review</a>
      <?php }?>
      <?php print $fields['php_3']->content; ?>
    </div>
  </div><!-- /button-group -->

</div>
