<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
  module_load_include('inc', 'adairs_commerce', 'includes/adairs_commerce.product_display');
  $lookbook_slide_pins = array();

  // Check for slide products
  if(!empty($row->_field_data['nid']['entity']->field_slide_products)) {

    // Output each product
    foreach ($row->_field_data['nid']['entity']->field_slide_products[LANGUAGE_NONE] as $slide_product) {

      $product_wrapper = entity_metadata_wrapper('field_collection_item', field_collection_item_load($slide_product['value']));
      $product_id = $product_wrapper->field_lookbook_product->product_id->value();
      $offset_x = $product_wrapper->field_offset_x->value();
      $offset_y = $product_wrapper->field_offset_y->value();
      $offset_attributes = "offset-x=$offset_x offset-y=$offset_y";
      $node = adairs_commerce_product_display_load_by_product_id($product_id);

      // Generate the modal link for Quicklook
      $href = "modal_ctools/nojs/view/product_display/display/product_display_quicklook_block/$node->nid/$product_id";
      $ctools_modal_link = onq_core_modal_link('', $href, 'quicklook');
      $lookbook_slide_pins[] = "<div style='position: absolute;' class='lookbook-slide-pin static' $offset_attributes>$ctools_modal_link</div>";
    }
  }


?>

<?php print $output; ?>
<?php foreach ($lookbook_slide_pins as $pin): ?>
  <?php print $pin; ?>
<?php endforeach ?>
