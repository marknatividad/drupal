<?php
//$path = onq_core_node_path_alias($row->nid, TRUE);
$path = 'https://www.youtube.com/watch?v=' . $row->field_field_video_id[0]['raw']['value'];
$options = sharethis_get_options_array();
?>

<div class="views-field views-field-sharethis">
  <span class="views-label views-label-sharethis">Share this video</span>
  <span class="field-content">
    <div class="sharethis-wrapper">
      <span class="st_sharethis" st_title="<?php print $row->node_title; ?>" st_url="<?php print $path; ?>"></span>
      <span class="st_fblike" st_title="<?php print $row->node_title; ?>" st_url="<?php print $path; ?>"></span>
      <script type="text/javascript">var switchTo5x = true;</script>
      <script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>
      <script type="text/javascript">stLight.options({"publisher":"<?php print $options['publisherID']; ?>"});</script>
    </div>
  </span>
</div>
