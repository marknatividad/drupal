<?php
/**
 * @file
 * Default theme implementation to display a commerce product in
 * the "We also love" section.
 */

/**
 * Fields available (please update once changed)
 *
 * $fields['field_brand'] == Product Display: Brand
 * $fields['title'] == Product Display: Title
 * $fields['php'] == Commerce Product: From price
 * $fields['field_image'] == Commerce Product: Image
 * $fields['body'] == Commerce Line Item: Title
 * $fields['field_rating'] == Product Display: Rating
 * $fields['field_product'] == Product Display: Product
 * $fields['path'] == Product Display: Path
 */
?>

<div class="product-display-quicklook">
  <div class="row">
    <div class="col-left">
      <?php print render($fields['field_image']->content); ?>
      <?php print render($fields['body']->label_html); ?>
      <?php print render($fields['body']->content); ?>
    </div><!-- /col-left -->

    <div class="col-right">
      <?php print render($fields['field_brand']->content); ?>
      <?php print render($fields['title']->content); ?>
      <?php print render($fields['php']->content); ?>
      <?php print render($fields['field_rating']->content); ?>
      <?php print render($fields['field_product']->content); ?>
      <?php print render($fields['path']->content); ?>
    </div><!-- /col-right -->
  </div>
</div>
