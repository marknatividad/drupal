<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<div class="<?php print $classes; ?>">
  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <?php print $title; ?>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($header): ?>
    <div class="view-header">
      <?php print $header; ?>
    </div>
  <?php endif; ?>

  <?php if ($exposed): ?>
    <div class="view-filters">

      <!-- Show filter -->
      <?php print $exposed; ?>

        <!-- Help icon -->
      <div class="lookbook-help">
      <img src="/sites/all/themes/adairs_style/images/icons/icon-info-19-19.png" alt="i" />&nbsp;
      <span class="default-link">Help</span>

        <!-- Custom drop down -->
        <div class="custom-dd">
          <div class="custom-dd-body">
            <div class="row">
              <img src="/sites/all/themes/adairs_style/images/icons/icon-lookbook-help-love.png" alt="social" />
              <h3>Love it? Buy it.</h3>
              <p>Click the ‘+’ to learn more or add to your cart.</p>
            </div>
            <div class="row">
              <img src="/sites/all/themes/adairs_style/images/icons/icon-lookbook-help-flip.png" alt="social" />
              <h3>Flip the page.</h3>
              <p>Tap the “>”arrow to continue, “<” to go back</p>
            </div>
            <div class="row">
              <img src="/sites/all/themes/adairs_style/images/icons/icon-lookbook-help-jump.png" alt="social" />
              <h3>Jump ahead.</h3>
              <p>Use the thumbnail page selector to skip to a page.</p>
            </div>
<!--            <div class="row">
              <img src="/sites/all/themes/adairs_style/images/icons/icon-lookbook-help-show.png" alt="social" />
              <h3>After something?</h3>
              <p>Filter pages by product categories.</p>
            </div>
             <div class="row">
              <img src="/sites/all/themes/adairs_style/images/icons/icon-lookbook-help-zoom.png" alt="social" />
              <h3>Get a closer look.</h3>
              <p>Zoom in for a detailed view. Zoom out to return.</p>
            </div>
            <div class="row">
              <img src="/sites/all/themes/adairs_style/images/icons/icon-lookbook-help-social.png" alt="social" />
              <h3>Tell your friends.</h3>
              <p>One click sharing on Facebook & Pinterest.</p>
            </div> -->
          </div>
        </div>
      </div>



    </div>
  <?php endif; ?>

  <?php if ($attachment_before): ?>
    <div class="attachment attachment-before">
      <?php print $attachment_before; ?>
    </div>
  <?php endif; ?>

  <?php if ($rows): ?>
    <div class="view-content">
      <?php print $rows; ?>
    </div>
  <?php elseif ($empty): ?>
    <div class="view-empty">
      <?php print $empty; ?>
    </div>
  <?php endif; ?>

  <?php if ($pager): ?>
    <?php print $pager; ?>
  <?php endif; ?>

  <?php if ($attachment_after): ?>
    <div class="attachment attachment-after">
      <?php print $attachment_after; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <?php print $more; ?>
  <?php endif; ?>

  <?php if ($footer): ?>
    <div class="view-footer">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

  <?php if ($feed_icon): ?>
    <div class="feed-icon">
      <?php print $feed_icon; ?>
    </div>
  <?php endif; ?>

</div><?php /* class view */ ?>