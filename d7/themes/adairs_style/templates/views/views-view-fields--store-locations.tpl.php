<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>

<?php

//Handler
$field_title = $fields['title'];
$handle = $field_title->wrapper_prefix . $field_title->label_html . $field_title->content . $field_title->wrapper_suffix;

// Content
$field_gsl_addressfield = $fields['gsl_addressfield'];
$output = '<div class="wrapper_store_location_info">';
$output .= $field_gsl_addressfield->wrapper_prefix . $field_gsl_addressfield->label_html . $field_gsl_addressfield->content . $field_gsl_addressfield->wrapper_suffix;
$field_more_information = $fields['field_more_information'];
$output .= $field_more_information->wrapper_prefix . $field_more_information->label_html . $field_more_information->content . $field_more_information->wrapper_suffix;
$output .= '</div>';

//COLLAPSIBLE
$options = array('handle' => $handle, 'content' => $output, 'collapsed' => TRUE, 'restrict_mobile' => TRUE);
print theme('onq_core_collapsible', $options);

?>