<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<div class="<?php print $classes; ?> video-gallery">
  <?php if ($header): ?>
    <div class="view-header">
      <?php print $header; ?>
    </div>
  <?php endif; ?>

  <div class="video-utility">
    <?php if (!empty($videos_nav)): ?>
      <div class="custom-dd-filter-menu-label">Sort by:</div>
      <div class="custom-el-trigger"><?php print $current_video_filter; ?>
        <div class="custom-dd-filter-menu">
          <?php print render($videos_nav); ?>
        </div>
      </div>
    <?php endif; ?>

    <?php if ($exposed): ?>
      <?php print $exposed; ?>
    <?php endif; ?>
    <?php if ($attachment_before): ?>
      <div class="attachment attachment-before">
        <?php print $attachment_before; ?>
      </div>
    <?php endif; ?>
    <div class="clear"></div>
  </div><!-- /video-utility -->

  <div class="video-list">
    <?php if ($rows): ?>
      <div class="view-content">
        <?php print $rows; ?>
      </div>
    <?php elseif ($empty): ?>
      <div class="view-empty">
        <?php print $empty; ?>
      </div>
    <?php endif; ?>
  </div><!-- /video-list -->

  <?php if ($pager): ?>
    <div class="pagination-wrapper clearfix">
      <?php print $pager; ?>
    </div>
  <?php endif; ?>
</div><?php /* class view */ ?>
