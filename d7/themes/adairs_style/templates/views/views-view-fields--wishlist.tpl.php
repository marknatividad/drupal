<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */

?>


<div class="wishlist-row clearfix">
  <div class="col-1">
    <div class="field-image">
      <?php print $fields['field_image']->content ?>
    </div>
    <div class="field-brand">
      <?php print $fields['field_brand']->content ?>
    </div>
    <div class="field-title">
      <?php print $fields['title']->content ?>
    </div>
    <div class="field-color">
      <?php print $fields['field_colour']->label_html ?>
      <?php print $fields['field_colour']->content ?>
    </div>
    <div class="field-style">
      <?php print $fields['field_style']->label_html ?>
      <?php print $fields['field_style']->content ?>
    </div>
  </div>
  <div class="col-2-wrapper">
    <div class="col-2">
      <table>
        <tr>
          <td class="field-rrp">
            <?php print $fields['commerce_price']->label_html ?>
            <?php print $fields['commerce_price']->content ?>
          </td>
          <td class="field-sale">
            <?php print $fields['field_sale_price']->label_html ?>
            <?php print $fields['field_sale_price']->content ?>
          </td>
          <td class="field-llover">
            <?php print $fields['php']->label_html ?>
            <div><?php print $fields['php']->content ?></div>
          </td>
        </tr>
      </table>
    </div>
    <div class="col-3">
      <table>
        <tr>
          <td class="field-actions">
            <?php print $fields['add_to_cart_form']->content; ?>
            <?php print $fields['delete_wishlist']->content; ?>
          </td>
        </tr>
      </table>
    </div><!-- /col-3 -->
  </div>
</div>
