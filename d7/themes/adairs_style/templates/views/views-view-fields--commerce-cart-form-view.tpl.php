<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */

/*
 * Cart Fields Available (please update once changed)
 *  [commerce_display_path] == Commerce Line item: Display path (hidden)
 *  [field_image] == Field: Image
 *  [edit_delete] == Commerce Line Item: Delete button
 *  [line_item_title] == Commerce Line Item: Title
 *  [field_colour] == Commerce Product: Colour
 *  [field_style] == Commerce Product: Colour
 *  [commerce_unit_price] == Commerce Line item: Unit price
 *  [edit_quantity] == Commerce Line Item: Quantity text field
 *  [commerce_total] == Commerce Line item: Total
 *  [field_sale_price] == Commerce Product: Sale Price
 *  [commerce_price] == Commerce Product: Price
 *  [php_1] == Product Coupon Line Item
 *  [php_2] == Product Price Discounted
 *
 *  @IMPORTANT:
 *    Bootstrap default scaffolding system is in use at the moment.
 *    Once scaffolding/grid system is confirmed, replace 'span<X>'
 *    classes.
 */

  $has_sale_price = ($row->field_field_sale_price != null && $row->field_field_sale_price[0]['raw']['amount'] > 0);
  $has_ll_account = account_management_user_has_linen_lovers_account(NULL, TRUE, TRUE);
  $has_attached_coupon = (isset($fields['php_1']) && isset($fields['php_2']));

  if ($has_sale_price || $has_ll_account) {
    $line_through = 'line-through';
  } else {
    $line_through = ($has_attached_coupon) ? 'line-through' : '';
  }

  if (!$has_sale_price) {
    $fields['field_sale_price']->content = '<div class="field-content price">n/a</div>';
  }

  if ($has_sale_price && $has_ll_account) {
    $line_through_sale = 'line-through';
  } else {
    $line_through_sale = ($has_sale_price && $has_attached_coupon) ? 'line-through' : '';
  }

  $line_through_ll_price = '';
  if ($has_ll_account && $has_attached_coupon) {
    $line_through_ll_price = 'line-through';
  }
?>

<div class="cart-row clearfix">
  <div class="col-1">
    <div class="field-image">
      <?php print $fields['field_image']->content ?>
    </div>
    <div class="field-brand">
      <?php print $fields['field_brand']->content ?>
    </div>
    <div class="field-title">
      <?php print $fields['line_item_title']->content ?>
    </div>
    <div class="field-color">
      <?php if (isset($fields['field_colour'])): ?>
        <?php print $fields['field_colour']->label_html ?>
        <?php print $fields['field_colour']->content ?>
      <?php endif; ?>
    </div>
    <div class="field-style">
      <?php if (isset($fields['field_colour'])): ?>
        <?php print $fields['field_style']->label_html ?>
        <?php print $fields['field_style']->content ?>
      <?php endif; ?>
    </div>
    <div class="field-cta">
    <?php print onq_core_modal_link('x remove', "cart/ajax/remove_cart/" . $view->row_index, 'delete-line-item-link'); ?>
      <?php print $fields['edit_delete']->content ?>
    </div>
  </div><!-- /col-1 --><!--
  --><div class="col-2">
    <table>
      <tr>
        <td class="field-rrp">
          <?php print $fields['commerce_price']->label_html ?>
          <span class="<?php print $line_through ?>"><?php print $fields['commerce_price']->content ?></span>
          <?php if (!$has_sale_price && !$has_ll_account && $has_attached_coupon): ?>
            <?php print $fields['php_2']->content; ?>
          <?php endif; ?>
        </td>
        <td class="field-sale">
          <?php print $fields['field_sale_price']->label_html ?>
          <span class="<?php print $line_through_sale; ?>"><?php print $fields['field_sale_price']->content ?></span>
          <?php if ($has_sale_price && !$has_ll_account && $has_attached_coupon): ?>
            <?php print $fields['php_2']->content; ?>
          <?php endif; ?>
        </td>
        <td class="field-llover">
          <?php print $fields['php']->label_html ?>
          <span class="<?php print $line_through_ll_price; ?>"><?php print $fields['php']->content ?></span>
          <?php if ($has_ll_account && $has_attached_coupon): ?>
            <?php print $fields['php_2']->content; ?>
          <?php endif; ?>
        </td>
      </tr>
    </table>
  </div><!-- /col-2 --><!--
  --><div class="col-3">
    <table>
      <tr>
        <td class="field-qty">
          <?php print $fields['edit_quantity']->label_html ?>
          <?php if ('membership' == $row->_field_data['commerce_product_field_data_commerce_product_product_id']['entity']->type) : ?>
            <div><?php print number_format($fields['edit_quantity']->raw); ?></div>
          <?php else: ?>
            <?php print $fields['edit_quantity']->content; ?>
          <?php endif; ?>
        </td>
        <td class="field-subtotal">
          <?php print $fields['commerce_total']->label_html ?>
          <?php if ($has_attached_coupon): ?>
            <?php print $fields['php_3']->content ?>
          <?php else: ?>
            <?php print $fields['commerce_total']->content ?>
          <?php endif; ?>
        </td>
      </tr>
    </table>
  </div><!-- /col-3 -->

  <div><?php if(isset($fields['php_1'])) { print $fields['php_1']->content; } ?></div>

</div><!-- /cart-row -->
