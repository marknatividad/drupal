<? if ($page_variant == 'a'): ?>
  <span class='attribute-selector'>select</span>
<? endif ?>
<? if ($page_variant == 'b'): ?>
<table>
  <thead>
    <tr>
      <th>&nbsp;</th>
      <th class="align-right">RRP</th>
      <th class="align-right">SALE</th>
      <th class="align-right">LL</th>
      <th>&nbsp;</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="align-left"><?=$current_style_name?></td>
      <td class="align-right <?=$price_row['line_through']?>"><?=$price_row['rrp']?></td>
      <td class="align-right"><?=$price_row['sale']?></td>
      <td class="align-right sale-item"><?=$price_row['llp']?></td>
      <td class="custom-dd-arrow-down">&nbsp;</td>
    </tr>
  </tbody>
</table>
<? endif ?>