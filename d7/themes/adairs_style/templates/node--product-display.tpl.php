<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $terms: the themed list of taxonomy term links output from theme_links().
 * - $display_submitted: whether submission information should be displayed.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */

/**
 * Product Display fields available (please update once changed)
 *
 * [product:title] == Commerce Line Item: Title
 * [product:commerce_price] == Commerce Line Item: Unit price
 * [product:from_price] == Commerce Line Item: Lowest price
 * [field_brand] == Field: Brand
 * [field_product] == == Product Display: Commerce Products
 * [field_product_features] == Product Display: Product Features
 * [field_product_information] == Product Display: Product Information
 * [field_product_care] == Product Display: Product Care
 */

  if (isset($content['field_product_features'])) {
    $product_features_object = $content['field_product_features']['#object']->field_product_features;
    $product_features = (isset($product_features_object[LANGUAGE_NONE])) ? $product_features_object[LANGUAGE_NONE] : $product_features_object;
  }
?>
<?php $variant_class = (isset($_SESSION['pd_page_variant'])) ? 'node-product-display-' . $_SESSION['pd_page_variant'] : ''; ?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> <?php print $variant_class; ?> "<?php print $attributes; ?>>

  <!-- Top container -->
  <div class="row" itemscope itemtype="http://schema.org/Product">

    <!-- Left column -->
    <div class="product-block-a">
        <?php print render($content['product:field_image']); ?>
    </div><!-- /col-left -->

    <!-- Right column -->
    <div class="product-block-b">
      <div class="mobile-product-image">
        <?php
          $uri = $content['product:field_image']['#items'][0]['uri'];
          $mobile_image = theme('image_style', array('style_name' => 'thumbnail', 'path' => $uri, 'alt' => 'image alt', 'title' => 'image title'));
          print render($mobile_image);
        ?>
        <span class="gallery-trigger"></span>
      </div>
      <div class="product-caption">
        <div itemprop="brand" itemscope itemtype="http://schema.org/Brand">
          <?php print render($content['field_brand']); ?>
        </div>
        <h1 itemprop="name"><?php print $node->title; ?></h1>

        <?php if(isset($_SESSION['pd_page_variant']) && $_SESSION['pd_page_variant'] == 'b'): ?>
        <div class="price-range field-name-field-brand">From <?php print adairs_commerce_product_price($node->nid, TRUE); ?><!--  to $CHANGE THIS --></div>
        <?php endif; ?>

        <div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
          <meta itemprop="worstRating" content="0" />
          <meta itemprop="bestRating" content="5" />
          <?php print render($content['field_rating']); ?>
          <div class="reviews-description">
          <?php print render($content['product:reviews_summary']); ?>
          <?php print render($content['product:reviews_link']); ?>
          </div>
        </div>

      </div>
    </div><!-- /col-right -->

    <div class="product-block-c">
      <?php print render($content['field_product']); ?>
      <p id="free-shipping-label">FREE Shipping for Linen Lovers &amp; Orders $<?php echo variable_get('free_shipping_limit'); ?>+</p>
      <meta itemprop="description" content="<?php print render($content['body'][0]['#markup']); ?>" />
    </div>

    <div class="clear"></div>
  </div><!-- row -->

  <!-- Bottom container -->
  <div class="row">

    <div class="product-block-e">
        <div class="_product_info_pane">
          <h3 class="more-info"><em>More Information</em></h3>
          <?php if (isset($product_features)): ?>
          <div class="text-center">
            <?php foreach ($product_features as $key => $value): ?>
              <span class="more-info-icon">
                <img src="<?php echo file_create_url($value['taxonomy_term']->field_icon[LANGUAGE_NONE][0]['uri']); ?>" />
              </span>
            <?php endforeach; ?>
          </div>
        <?php endif; ?>
        </div>

      <div class="_product_info_pane">
        <?php
          $handle = '<h3><a href="#"><span class="icon-s32 icon-s32-1">Customer Reviews</span><span class="right icon-s32 icon-s32-5"></span></a></h3>';
          $output = '<div class="average-rating"><span>Average rating&nbsp;&nbsp;</span>' . render($content['field_rating']);
          $output .= '<span>' . render($content['product:comments_reviews_link']) . "&nbsp;&nbsp;›</span></div>";
          if (!empty($content['comments']['comments'])) {
            $output .= render($content['comments']['comments']);
          } else {
            $output .= '<div>There are currently no reviews for this product. Be the first to write a review.</div>';
          }
          $output .= '<div class="reviews-buttons-area">';
          if ($comment == COMMENT_NODE_OPEN) {
            $output .= '<span><a href="#" class="btn btn-1" id="write_review_btn"><img src="/sites/all/themes/adairs_style/images/icons/icon-write-review-32-32.png" alt="Write a review" />Write A Review ›</a></span>';
          }
          $output .= '<span><a href="/customer-reviews' . $node_url . '" class="btn btn-2">View All Reviews ›</a></span>';
          $output .= '</div>';
          if ($comment == COMMENT_NODE_OPEN) {
            $output .= render($content['comments']['comment_form']);
          }
          $options = array('handle' => $handle, 'content' => $output, 'collapsed' => TRUE);
          print theme('onq_core_collapsible', $options);
        ?>
      </div><!-- /_product_info_pane -->

      <div class="_product_info_pane">
        <?php
          $handle = '<h3><a href="#"><span class="icon-s32 icon-s32-2">Why We Love This</span><span class="right icon-s32 icon-s32-5"></span></a></h3>';
          $output = render($content['body']);
          $options = array('handle' => $handle, 'content' => $output, 'collapsed' => TRUE);
          print theme('onq_core_collapsible', $options);
        ?>
      </div><!-- /_product_info_pane -->

      <div class="_product_info_pane">
        <?php
          $handle = '<h3><a href="#"><span class="icon-s32 icon-s32-3">Product Information</span><span class="right icon-s32 icon-s32-5"></span></a></h3>';
          $output = render($content['field_product_information']);
          $options = array('handle' => $handle, 'content' => $output, 'collapsed' => TRUE);
          print theme('onq_core_collapsible', $options);
        ?>
      </div><!-- /_product_info_pane -->

      <div class="_product_info_pane">
        <?php
          $handle = '<h3><a href="#"><span class="icon-s32 icon-s32-4">Product Care</span><span class="right icon-s32 icon-s32-5"></span></a></h3>';
          $output = render($content['field_product_care']);
          $options = array('handle' => $handle, 'content' => $output, 'collapsed' => TRUE);
          print theme('onq_core_collapsible', $options);
        ?>
      </div><!-- /_product_info_pane -->
    </div>

    <div class="product-block-d">
      <?php if(isset($content['product:field_we_also_love']['#object'])): ?>
      <h4><em>You Might Like...</em></h4>
      <?php endif; ?>
      <div class="we-also-love">
        <?php print render($content['product:field_we_also_love']); ?>
      </div>
    </div>

    <div class="clear"></div>
  </div><!-- /row -->
  <div class="clear"></div>
</div>
