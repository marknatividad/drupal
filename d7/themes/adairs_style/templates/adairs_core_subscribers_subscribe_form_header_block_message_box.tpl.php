<?php if ($message): ?>
  <?php print $message; ?>
<?php else: ?>
<div class="subscribe-header">
  <div class="subscribe-header-default">
    <h3>THANK YOU FOR SUBSCRIBING!</h3>
    <span>We will be in touch shortly with the latest special offers, deals and news.</span>
  </div>
</div>
<?php endif; ?>