<div class='page-wrapper'>

  <?php if ($page['help'] || ($show_messages && $messages)): ?>
    <br><br><br>
    <div id='console'><div class='limiter clearfix'>
      <?php print render($page['help']); ?>
      <?php if ($show_messages && $messages): print $messages; endif; ?>
    </div></div>
  <?php endif; ?>

  <?php print render($title_prefix); ?>
  <?php if ($title): ?><h1 class='page-title'><?php print $title ?></h1><?php endif; ?>
  <span class="page-title-suffix"><?php print render($title_suffix); ?></span>

  <div class="page-body-center"><?php print render($page['content']) ?></div>

</div>