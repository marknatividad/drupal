<div class="custom-dd">
  <div class="custom-dd-header">
    Please select a size:
  </div>
  <div class="custom-dd-body">
    <?php foreach ($styles as $tid => $name) {
      $classes = 'custom-dd-size';
      // If this is the selected colour, add a class
      $classes .= ($style_selected == $tid) ? ' custom-dd-size-selected' : '' ;
      print "<div class='$classes' data-tid='$tid'>";
      print "<span>$name</span>";
      print '</div>';
    } ?>
  </div>
</div>
