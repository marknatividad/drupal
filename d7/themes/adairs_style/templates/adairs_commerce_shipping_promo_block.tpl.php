<!-- Shipping Promo -->
<div class="shipping-promo-block">
  <p>Your order is only <?php print $difference ?> away from Free Shipping!</p><br/>
  <p>If you wish to add to your order, simply <a href="/" class="">keep shopping</a>, otherwise continue to checkout.</p>
</div>