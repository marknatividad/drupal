<?php

/**
 * @file
 * Sample template for HTML Mail test messages.
 */
include('htmlmail_ENV.php');
// global $base_root;
// $path = $base_root. base_path() . path_to_theme(). '/images/email/';
// $path = 'http://adairs.prod/sites/all/themes/adairs_style/images/email/';

?>
<body background="#ffffff" bgcolor="#ffffff" style="margin:0; padding:0; background-color: #ffffff;">
<div class="htmlmail-body">
<table align="center" id="backgroundTable" border="0" cellpadding="0" cellspacing="0" width="100%" background="#ffffff">
  <tr>
    <td width="50" bgcolor="#ffffff" background="#ffffff">&nbsp;</td>
    <td align="center" valign="top" bgcolor="#ffffff" background="#ffffff" ><!-- EDM CENTERED -->
      <table border="0" cellpadding="0" cellspacing="0" width="599">
        <!-- EDM header -->
        <tr>
          <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="599">
              <tr>
                <td colspan="4">&nbsp;</td>
              </tr>
              <tr>
                <td width="202" align="left"><a href="<?php echo $base_root ?>" target="_blank"><img src="<?php echo $path ?>logo.jpg" alt="Adairs" width="184" height="73" style="display:block; border:none" /></a></td>
                <td width="151" valign="middle"><img src="<?php echo $path ?>header-info-01.jpg" alt="Free shipping Orders $100+ & Linen Lovers" width="151" height="28" style="display:block; border:none" /></td>
                <td width="111" valign="middle"><img src="<?php echo $path ?>header-info-02.jpg" alt="1300 783 005 Customer Hotline" width="111" height="28" style="display:block; border:none" /></td>
                <td width="111" valign="middle"><img src="<?php echo $path ?>header-info-03.jpg" alt="30 Day Returns" width="111" height="28" style="display:block; border:none" /></td>
              </tr>
              <tr>
                <td colspan="4">&nbsp;</td>
              </tr>
            </table></td>
        </tr>
        <!-- /EDM header -->
        <!-- EDM body -->
        <tr>
          <td bgcolor="#FFFFFF" bordercolor="#d7d6d6" style="border:1px solid #d7d6d6;" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="599" bgcolor="#FFFFFF">
<!--               <tr>
                <td colspan="3"><img src="<?php echo $path ?>hero-contact.jpg" alt="Thank you. We'll be in touch soon" width="599" height="255" style="display:block; border:none"/></td>
              </tr> -->
              <tr>
                <td colspan="1" height="40">htmlmail--htmlmail.tpl.php</td>
              </tr>
              <tr>
                <td colspan="3" height="40">&nbsp;</td>
              </tr>
              <tr>
                <td width="40" valign="top"></td>
                <td width="519" valign="top" align="left"><span style="font-family: 'Lucida Sans Unicode', 'Lucida Grande', 'Arial', 'Helvetica','sans-serif'; font-size:12px; color:#676767; line-height:18px">

                <?php echo $body; ?>
                </span>
                </td>
                <td width="40" valign="top"></td>
              </tr>
              <tr>
                <td colspan="3" height="40">&nbsp;</td>
              </tr>
              <tr>
                <td colspan="3"><a href="<?php echo $base_root ?>" target="_blank"><img src="<?php echo $path ?>footer.jpg" alt="Vist our online store" width="599" height="158" style="display:block; border:none"/></a></td>
              </tr>
            </table></td>
        </tr>



        <!-- /EDM body -->
        <!-- EDM footer -->
        <tr>
          <td><table border="0" cellpadding="0" cellspacing="0" width="599">
              <tr>
                <td colspan="2"><a href="<?php echo $base_root ?>" target="_blank"><img src="<?php echo $path ?>footer-shadow.jpg" alt="" width="599" height="14" style="display:block; border:none"/></a></td>
              </tr>
              <tr>
                <td valign="middle" width="574"><span style="font-family:'Lucida Grande', 'Arial', 'Helvetica','sans-serif'; font-size:12px; color:#676767; letter-spacing:normal"> &nbsp;&nbsp;&nbsp;&nbsp;&copy; <?php echo date('Y')?> Adairs </span></td>
                <td valign="middle" width="25"><a href="https://www.facebook.com/AdairsRetailGroup" target="_blank"><img src="<?php echo $path ?>icon-fb.png" alt="Adairs Facebook" width="19" height="19" style="display:inline-block; border:none;" /></a></td>
              </tr>
              <tr>
                <td colspan="2">&nbsp;</td>
              </tr>
            </table></td>
        </tr>
        <!-- /EDM footer -->
      </table>
      <!-- EDM CENTERED --></td>
      <td width="50" bgcolor="#ffffff" background="#ffffff">&nbsp;</td>
  </tr>
</table>
</div>

<?php if ($debug): ?>
<hr />
<div class="htmlmail-debug">
  <dl><dt><p>
    To customize this test message:
  </p></dt><dd><ol><li><p><?php if (empty($theme)): ?>
    Visit <u>admin/config/system/htmlmail</u>
    and select a theme to hold your custom email template files.
  </p></dt><dd><ol><li><p><?php elseif (empty($theme_path)): ?>
    Visit <u>admin/appearance</u>
    to enable your selected <u><?php echo ucfirst($theme); ?></u> theme.
  </p></dt><dd><ol><li><p><?php endif; ?>
    Copy the
    <a href="http://drupalcode.org/project/htmlmail.git/blob_plain/refs/heads/7.x-2.x:/htmlmail--htmlmail.tpl.php"><code>htmlmail--htmlmail.tpl.php</code></a>
    file to your <u><?php echo ucfirst($theme); ?></u> theme directory
    <u><code><?php echo $theme_path; ?></code></u>.
  </p></li><li><p>
    Edit the copied file.
  </p></li></ol></dd></dl>
</div>
<?php endif;?>
</body>
</html>
