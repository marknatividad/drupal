<?php
// $Id$
/**
 * @file
 *    default theme implementation for displaying Google Search Appliance results
 *
 * This template collects each invocation of theme_google_appliance_result(). This and
 * the child template are dependent on one another sharing the markup for the
 * results listing
 *
 * @see template_preprocess_google_appliance_results()
 * @see template_preprocess_google_appliance_result()
 * @see google-appliance-result.tpl.php
 */
//dsm($variables);
?>
<div class="search-results-wrapper">
<!--
 <?php if (isset($show_synonyms)) : ?>
  <div class="synonyms google-appliance-synonyms">
    <span class="p"><?php print $synonyms_label ?></span> <ul><?php print $synonyms; ?></ul>
  </div>
 <?php endif; ?>
-->

<?php if (!isset($response_data['error'])) : ?>

  <div class="search-results-control-bar clearfix">
    <?php print $search_stats; ?>
    <?php print $pager; ?>
  </div>
<!--
  <?php if (!empty($keymatch_results)) : ?>
    <ol class="keymatch-results google-appliance-keymatch-results">
      <?php print $keymatch_results; ?>
    </ol>
  <?php endif; ?>
-->

<?php
    $path = drupal_get_path('module', 'fivestar');
    drupal_add_js($path . '/js/fivestar.js');
    drupal_add_css($path . '/css/fivestar.css');
  ?>
  <ol class="search-results google-appliance-results">
    <?php print $search_results; ?>
  </ol>

  <div class="search-results-control-bar clearfix">
    <?php print $search_stats; ?>
    <?php print $pager; ?>
  </div>

<?php else : ?>
  <p><?php print $error_reason ?></p>
<?php endif; ?>

</div><!-- /serach-result-wrapper -->
