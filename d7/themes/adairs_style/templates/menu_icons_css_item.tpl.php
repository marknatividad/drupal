<?php

/**
 * @file
 *
 * Template file for generating the CSS file used for the menu-items
 */

/**
 * Available variables:
 * - $path: background image path
 * - $mlid: the unique identifier for the menu icon
 * - $path: the path to the menu icon image
 * - $pos: the background position value
 * - $size: the amount of padding in pixels
 * - $height: the menu image height
 */
?>
<?php global $base_url; ?>
ul.menu-<?php print $mlid; ?> {
  background-image: url(<?php echo $base_url . $path ?>);
  padding-<?php print "$pos:$size"; ?>px;
  background-repeat: no-repeat;
  background-position: <?php print $pos; ?>;
  height: 100%;
}
