<div>
  <a href="javascript:void(0);" class="slider-close"></a>

  <?php if ($heading): ?>
    <?php print $heading; ?>
  <?php else: ?>
    <div class="subscribe-header">
      <div class="subscribe-header-default">
        <h3>SUBSCRIBE</h3>
        <span>to Adairs news, deals &amp; offers.</span>
      </div>
    </div>
  <?php endif; ?>

  <?php if ($image): ?>
    <div><?php print $image; ?></div>
  <?php endif; ?>

  <?php print $form; ?>

  <?php if ($terms): ?>
    <div id="subscribe-terms"><?php print $terms; ?></div>
  <?php endif; ?>
</div>