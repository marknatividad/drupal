<div class="custom-dd">
  <div class="custom-dd-header">
    Please select a colour:
  </div>
  <div class="custom-dd-body">
    <?php foreach ($colour_map as $tid => $uri) {
      $classes = 'custom-dd-colour';
      // If this is the selected colour, add a class
      $classes .= ($colour_selected == $tid) ? ' custom-dd-colour-checked' : '' ;
      print "<div class='$classes' data-tid='$tid'>";
      print theme('image_style', array('style_name' => '65_65', 'path' => $uri));
      print '</div>';
    } ?>
  </div>
</div>
