<!-- Pricing -->
<div class="span12 form-item">
  <div>
    <?php if ($rr_price != $current_price): ?>
      <span class="price rrp-price">
        RRP <span class="line-through">
        <?php print $rr_price ?>
      </span>
    <?php endif ?>
  </div>
  <div>
    <span class="price current-price">
      Now <?php print $current_price; ?>
    </span>
    <span class="price linen-lovers-price">&nbsp;&nbsp;&nbsp;
      L/Lovers <?php print $linen_lovers_price; ?>
    </span>
  </div>
</div>