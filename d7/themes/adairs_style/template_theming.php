<?php
/**
 * Template file for overriding theme hooks etc.
 *
 */

// ---------------------------------------------------------

/**
 * Implements theme_menu_link()
 *
 */
function adairs_style_menu_link__menu_main_menu_adairs_kids(array $variables) {
  $element = $variables['element'];
  $class_depth = (isset($element['#original_link'])) ? 'level-' . $element['#original_link']['depth'] : '';
  $sub_menu = '';

  if ($element['#below']) {
    // Prevent dropdown functions from being added to management menu as to not affect navbar module.
    if (($element['#original_link']['menu_name'] == 'management') && (module_exists('navbar'))) {
      $sub_menu = drupal_render($element['#below']);
    } else {
      // Add our own wrapper
      unset($element['#below']['#theme_wrappers']);
      $sub_menu = '<ul class="' . $class_depth . '">' . drupal_render($element['#below']) . '</ul>';
    }
  }
  // Issue #1896674 - On primary navigation menu, class 'active' is not set on active menu item.
  // @see http://drupal.org/node/1896674
  if (($element['#href'] == $_GET['q'] || ($element['#href'] == '<front>' && drupal_is_front_page())) &&
      (empty($element['#localized_options']['language']) || $element['#localized_options']['language']->language == $language_url->language)) {
    $element['#attributes']['class'][] = 'active';
  }

  // Strip the classes applied by the menu_icon module to the actual link
  if (isset($element['#localized_options']['menu_icon'])) {
    unset($element['#localized_options']['attributes']['class']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);

  // Add the Brands submenu to child nodes
  if (isset($element['#original_link']) && (($element['#original_link']['module'] == 'menu') && ($element['#original_link']['depth'] == 3))) {
    // Remove leaf class
    $element['#attributes']['class'] = array_diff($element['#attributes']['class'], array('leaf'));

    $link_path = explode('/', $element['#original_link']['link_path']);
    $tid = array_pop($link_path);

    // Retrieve all brands linked to this product category term ID
    $brands_sub_menu = adairs_style_menu_item_brands($tid, $element);
    if (!empty($brands_sub_menu)) {
      $element['#localized_options']['attributes']['class'][] = 'dropdown-toggle';
      $element['#localized_options']['attributes']['data-toggle'] = 'dropdown';

      // Apply the auto generated class from the menu_icon module
      $class_icon = '';
      if (isset($element['#original_link']['options']['menu_icon'])) {
        if ($element['#original_link']['options']['menu_icon']['enable'] == 1) {
          $class_icon = ' ' . implode(' ', $element['#original_link']['options']['attributes']['class']);
        }
      }
      $sub_menu = '<ul class="' . $class_depth . $class_icon . '">' . drupal_render($brands_sub_menu) . '</ul>';
    }
  } else if (isset($element['#original_link']) && ($element['#original_link']['depth'] == 2)) {
    if (isset($element['#original_link']['options']['menu_icon'])) {
      if ($element['#original_link']['options']['menu_icon']['enable'] == 1) {
        $class_ul = $class_depth . ' menu-icon-leaf ' . implode(' ', $element['#original_link']['options']['attributes']['class']);
        $sub_menu = '<ul class="' . $class_ul  . '" style="width: 779px;"></ul>';
      }
    }
  }

  //krumo($element);

  // Add class 'expanded' to non leaf menu items
  if (isset($element['#original_link']) && $element['#original_link']['depth'] > 1) {
    if (!in_array('leaf', $element['#attributes']['class']) && !in_array('expanded', $element['#attributes']['class'])) {
      $element['#attributes']['class'][] = 'expanded';
    }
  }

  // Add Back menu item to First elements of each level 3
  if(in_array('first', $element['#attributes']['class']) && array_key_exists('#original_link', $element) && $element['#original_link']['depth'] == 3){
    return adairs_style_get_back_button_menu($element).'<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
  }

  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

// ---------------------------------------------------------

/**
 * Implements theme_menu_link()
 *
 */
function adairs_style_menu_link__main_menu(array $variables) {
  //return onq_core_apc_cache('adairs_style_menu_generate_link_parent', array($variables), md5($variables['element']['#href'] . $variables['element']['#title']));
  return adairs_style_menu_generate_link_parent($variables);
}

function adairs_style_menu_generate_link_parent(array $variables) {
  $element = $variables['element'];
  $class_depth = (isset($element['#original_link'])) ? 'level-' . $element['#original_link']['depth'] : '';
  $sub_menu = '';

  if ($element['#below']) {
    // Prevent dropdown functions from being added to management menu as to not affect navbar module.
    if (($element['#original_link']['menu_name'] == 'management') && (module_exists('navbar'))) {
      $sub_menu = drupal_render($element['#below']);
    } else {
      // Add our own wrapper
      unset($element['#below']['#theme_wrappers']);
      $sub_menu = '<ul class="' . $class_depth . '">' . drupal_render($element['#below']) . '</ul>';
    }
  }
  // Issue #1896674 - On primary navigation menu, class 'active' is not set on active menu item.
  // @see http://drupal.org/node/1896674
  if (($element['#href'] == $_GET['q'] || ($element['#href'] == '<front>' && drupal_is_front_page())) &&
      (empty($element['#localized_options']['language']) || $element['#localized_options']['language']->language == $language_url->language)) {
    $element['#attributes']['class'][] = 'active';
  }

  // Strip the classes applied by the menu_icon module to the actual link
  if (isset($element['#localized_options']['menu_icon']) && $element['#localized_options']['menu_icon']['enable']) {
    unset($element['#localized_options']['attributes']['class']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);

  // Add the Brands submenu to child nodes
  if (isset($element['#original_link']) && (($element['#original_link']['module'] == 'taxonomy_menu') && ($element['#original_link']['depth'] == 3))) {
    // Remove leaf class
    $element['#attributes']['class'] = array_diff($element['#attributes']['class'], array('leaf'));

    $link_path = explode('/', $element['#original_link']['link_path']);
    $tid = array_pop($link_path);

    // Retrieve all brands linked to this product category term ID
    // $brands_sub_menu = adairs_style_menu_item_brands($tid, $element);

    $brands_sub_menu = onq_core_apc_cache('adairs_style_menu_item_brands', array($tid, $element), 'brands_sub_menu_' . $tid);
    if (!empty($brands_sub_menu)) {
      $element['#localized_options']['attributes']['class'][] = 'dropdown-toggle';
      $element['#localized_options']['attributes']['data-toggle'] = 'dropdown';

      // Apply the auto generated class from the menu_icon module
      $class_icon = '';
      if (isset($element['#original_link']['options']['menu_icon'])) {
        if ($element['#original_link']['options']['menu_icon']['enable'] == 1) {
          $class_icon = ' ' . implode(' ', $element['#original_link']['options']['attributes']['class']);
        }
      }
      $sub_menu = '<ul class="' . $class_depth . $class_icon . '">' . drupal_render($brands_sub_menu) . '</ul>';
    }
  } else if (isset($element['#original_link']) && ($element['#original_link']['depth'] == 2)) {
    if (isset($element['#original_link']['options']['menu_icon'])) {
      if ($element['#original_link']['options']['menu_icon']['enable'] == 1) {
        $class_ul = $class_depth . ' menu-icon-leaf ' . implode(' ', $element['#original_link']['options']['attributes']['class']);
        $sub_menu = '<ul class="' . $class_ul  . '" style="width: 779px;"></ul>';
      }
    }
  }

  //krumo($element);

  // Add class 'expanded' to non leaf menu items
  if (isset($element['#original_link']) && $element['#original_link']['depth'] > 1) {
    if (!in_array('leaf', $element['#attributes']['class']) && !in_array('expanded', $element['#attributes']['class'])) {
      $element['#attributes']['class'][] = 'expanded';
    }
  }

  // Add Back menu item to First elements of each level 3
  if(in_array('first', $element['#attributes']['class']) && array_key_exists('#original_link', $element) && $element['#original_link']['depth'] == 3){
    return adairs_style_get_back_button_menu($element).'<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
  }

  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}


function adairs_style_get_back_button_menu($element){
  $path_alias = drupal_get_path_alias($element['#href']);
  $back_item_menu = array(
    '#theme' => 'menu_link__main_menu',
    '#attributes' => array(
      'class' => array('first', 'expanded', 'back')
      ),
    '#title' => '‹ Back',
    '#href' => $path_alias,
    '#below' => array(),
    '#localized_options' => array('attributes' => array('class' => 'menu-item-back'))
    );
  $output_back_item = l($back_item_menu['#title'], $back_item_menu['#href'], $back_item_menu['#localized_options']);
  $back_menu = '<li '. drupal_attributes($back_item_menu['#attributes']). '>' .$output_back_item . "</li>\n";
  return $back_menu;
}
// ---------------------------------------------------------

/**
 * Theme wrapper function for the primary menu links
 *
 */
function adairs_style_menu_tree__primary(&$vars) {
  return '<ul id="page-nav" class="page-nav">' . $vars['tree'] . '</ul>';
}

function adairs_style_menu_tree__footer(&$vars) {
  $handle = '<h2>More Information</h2>';
  $output = '<ul class="menu">'.$vars['tree'].'</ul>';
  $options = array('handle' => $handle, 'content' => $output, 'collapsed' => TRUE, 'restrict_mobile' => TRUE);
  return theme('onq_core_collapsible', $options);
}

function adairs_style_menu_tree__taxonomy(&$vars) {
  $handle = '<h2>Shop Adairs Online</h2>';
  $output = $vars['tree'];
  $options = array('handle' => $handle, 'content' => $output, 'collapsed' => TRUE, 'restrict_mobile' => TRUE);
  return theme('onq_core_collapsible', $options);
}

// ---------------------------------------------------------

/**
 * Returns a brands sub menu structure based on the product category term ID
 *
 */
function adairs_style_menu_item_brands($tid, $element) {
  $term = taxonomy_term_load($tid);
  $result = adairs_core_brands_by_term($tid);
  $path_alias = drupal_get_path_alias($element['#href']);
  $sub_menu = array(
    array(
      array(
        '#theme' => 'menu_link__main_menu',
        '#attributes' => array(
          'class' => array('first', 'leaf', 'back')
          ),
        '#title' => '‹ Back',
        '#href' => $path_alias,
        '#below' => array(),
        '#localized_options' => array('attributes' => array('class' => 'menu-item-back'))
        ),
      array(
        '#theme' => 'menu_link__main_menu',
        '#attributes' => array(
          'class' => array()
          ),
        '#title' => 'Shop All ' . $term->name . ' ›',
        '#href' => $path_alias,
        '#below' => array(),
        '#localized_options' => array()
        ),
      array(
        '#theme' => 'menu_link__main_menu',
        '#attributes' => array(
          'class' => array('shop-brand')
          ),
        '#title' => 'OR SHOP BY BRAND:',
        '#href' => '<nolink>',
        '#below' => array(),
        '#localized_options' => array()
        )
      )
    );

  if (count($result)) {
    foreach ($result as $row) {
      $term = taxonomy_term_load($row->tid);
      $term_uri = taxonomy_term_uri($term);
      $term_alias = strstr(drupal_lookup_path('alias', $term_uri['path']), '/');

      $sub_menu[] = array(
        array(
          '#theme' => 'menu_link__main_menu',
          '#attributes' => array(
            'class' => array('leaf')
          ),
          '#title' => '•' . html_entity_decode('&nbsp;&nbsp;') . $row->name . ' ›',
          '#href' => $path_alias . $term_alias,
          '#below' => array(),
          '#localized_options' => array()
        )
      );
    }
  }

  return $sub_menu;
}


// ---------------------------------------------------------

/**
 * Taken from
 * http://drupalcontrib.org/api/drupal/contributions!fivestar!includes!fivestar.theme.inc/function/theme_fivestar_summary/7
 */
function adairs_style_fivestar_summary($variables) {
  extract($variables, EXTR_SKIP);
  $output = '';
  $div_class = '';
  if (isset($user_rating)) {
    $div_class = isset($votes) ? 'user-count' : 'user';
    $user_stars = round(($user_rating * $stars) / 100, 1);
    $output .= '<span class="user-rating">' . t('Your rating: <span>!stars</span>', array('!stars' => $user_rating ? $user_stars : t('None'))) . '</span>';
  }
  if (isset($user_rating) && isset($average_rating)) {
    $output .= ' ';
  }
  if (isset($average_rating)) {
    $div_class = isset($votes) ? 'average-count' : 'average';
    $average_stars = round(($average_rating * $stars) / 100, 1);

    $microdata = 'itemprop="ratingValue"';
    $output .= '<span class="average-rating">' . t('Average: <span !microdata>!stars</span>', array('!stars' => $average_stars, '!microdata' => $microdata)) . '</span>';
  }
  if (isset($user_rating) && isset($average_rating)) {
    $div_class = 'combo';
  }

  $microdata = 'itemprop="ratingcount"';

  if (isset($votes) && !(isset($user_rating) || isset($average_rating))) {

    $output .= ' <span class="total-votes">' . format_plural($votes, '<span '.$microdata.'>@count</span> vote', '<span>@count</span> votes') . '</span>';
    $div_class = 'count';
  }
  elseif (isset($votes)) {
    $output .= ' <span class="total-votes">(' . format_plural($votes, '<span '.$microdata.'>@count</span> vote', '<span>@count</span> votes') . ')</span>';
  }

  if ($votes === 0) {
    $output = '<span class="empty">' . t('No votes yet') . '</span>';
  }

  $output = '<div class="fivestar-summary fivestar-summary-' . $div_class . '">' . $output . '</div>';
  return $output;
}


// ---------------------------------------------------------

/**
 * Overrides theme_menu_link()
 *
 */
function adairs_style_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';
  $element['#localized_options']['html'] = TRUE;

  /**
   * Add menu item's description below the menu title
   * Source: https://drupal.org/node/1600970
   */
  if (in_array($element['#original_link']['menu_name'], array('menu-my-account-menu', 'menu-join-login-menu')) && isset($element['#localized_options']['attributes']['title'])){
    $element['#title'] .= '<span>' . $element['#localized_options']['attributes']['title'] . '</span>';
  }

  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li>' . $output ."</li>\n";
}



// ---------------------------------------------------------

/**
 * Implements theme_breadcrumb()
 *
 */
function adairs_style_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];

  if (!empty($breadcrumb)) {
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

    $output .= '<div class="breadcrumb">' . implode('&nbsp; › &nbsp;', $breadcrumb) . '</div>';
    return $output;
  }
}

// ---------------------------------------------------------

/**
 * Implements theme_form_element_label
 */
function adairs_style_form_element_label($variables) {
  $element = $variables['element'];
  // This is also used in the installer, pre-database setup.
  $t = get_t();

  // If title and required marker are both empty, output no label.
  if ((!isset($element['#title']) || $element['#title'] === '') && empty($element['#required'])) {
    return '';
  }

  // If the element is required, a required marker is appended to the label.
  $required = !empty($element['#required']) ? theme('form_required_marker', array('element' => $element)) : '';

  $title = filter_xss_admin($element['#title']);

  $attributes = array();
  // Style the label as class option to display inline with the element.
  if ($element['#title_display'] == 'after') {
    $attributes['class'] = 'option';
  }
  // Show label only to screen readers to avoid disruption in visual flows.
  elseif ($element['#title_display'] == 'invisible') {
    $attributes['class'] = 'element-invisible';
  }

  if (!empty($element['#id'])) {
    $attributes['for'] = $element['#id'];
  }

  // Add checked class for label of radio selects when value matches
  if (isset($element['#return_value']) && $element['#value'] !== FALSE && $element['#value'] == $element['#return_value']) {
    $attributes['class'] .= ' checked';
  }

  // The leading whitespace helps visually separate fields from inline labels.
  return ' <label' . drupal_attributes($attributes) . '>' . $t('!title !required', array('!title' => $title, '!required' => $required)) . "</label>\n";
}

/*
 * Rewrite Image field for maximize trigger
 */
function adairs_style_colorbox_imagefield($variables) {
  $class = array('colorbox');

  if ($variables['image']['style_name'] == 'hide') {
    $image = '';
    $class[] = 'js-hide';
  }
  elseif (!empty($variables['image']['style_name'])) {
    $image = theme('image_style', $variables['image']);
  }
  else {
    $image = theme('image', $variables['image']);
  }

  $options = array(
    'html' => TRUE,
    'attributes' => array(
      'title' => $variables['title'],
      'class' => $class,
      'rel' => $variables['gid'],
      )
    );

  if(current_path() == 'cart'){
    return l($image .'<span class="maximize-trigger"></span>', $variables['path'], $options);
  }

  return l($image, $variables['path'], $options);
}


function adairs_style_preprocess_adairs_commerce_admin_order_view(&$variables) {
  // This is the generic entity preprocessor
  template_preprocess_entity($variables);
}
