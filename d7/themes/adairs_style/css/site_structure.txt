// ------------------

CSS Structure

This file helps describe the structure of the styles.css.less file.

// ------------------


.page-wrapper

    .site-switcher

    .subscription-top

    .page-header-wrapper

        #page-header

    .page-body-wrapper

        #page-body

            .page-body-main

    .page-footer-wrapper

        #page-footer

            .footer-top

            .footer-bottom

#fb-root


// ------------------

CSS Structure

Categories inside styles.css.less

// ------------------

Global Styling
Page Body Section
Subscription Box
Product Listing Section
Product Display Section
Product Display Section
Search Result Section
Shopping Cart Section
Checkout Section
User Register Page
Wishlist Page
My Account Page
Store Locator Page
Video Page
Review Page
About Us / Careers Page
Contact Us / Your Feedback Page
Other pages
Page Footer Section





