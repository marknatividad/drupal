(function ($) {

  Drupal.behaviors.adairsCore = {

    // Markup for unsupported ie7 page
    markup:
      '<div class="page-body-wrapper" id="page-body-wrapper-unsupported">' +
        '<div class="page-wrapper">' +
          '<div class="page-body-main">' +
            '<div class="container">' +
              '<div class="page-header-wrapper clearfix">' +
                '<div id="page-header" class="container">' +
                  '<div itemscope="" itemtype="http://schema.org/Organization" class="page-logo">' +
                    '<a href="/" itemprop="url"><img src="/sites/all/themes/adairs_style/logo.png" alt="Adairs Logo" title="Adairs Logo" itemprop="logo" /></a>' +
                  '</div>' +
                '</div>' +
              '</div>' +
              '<div class="clear"></div>' +
              '<div class="page-title-wrapper clear">' +
                '<h1 class="page-title">Sorry, we do not currently support the web browser you’re using.</h1>' +
                '<h2>Please update your web browser to one of our supported browsers.</h2>' +
                '<div class="col">' +
                  '<h3>Chrome</h3>' +
                  '<a href="https://www.google.com/intl/en_uk/chrome/browser/" class="btn btn-green">Download latest version</a>' +
                '</div>' +
                '<div class="col">' +
                  '<h3>Firefox</h3>' +
                  '<a href="http://www.mozilla.org/en-US/firefox/new/" class="btn btn-green">Download latest version</a>' +
                '</div>' +
                '<div class="col">' +
                  '<h3>Safari</h3>' +
                  '<a href="https://www.apple.com/au/safari/" class="btn btn-green">Download latest version</a>' +
                '</div>' +
                '<div class="col">' +
                  '<h3>Internet Explorer</h3>' +
                  '<a href="http://windows.microsoft.com/en-au/internet-explorer/download-ie" class="btn btn-green">Download latest version</a>' +
                '</div>' +
              '</div>' +
            '</div>' +
          '</div>' +
        '</div>' +
      '</div>',

    // Replace body with markup
    attach: function (context, settings) {
      $('body').remove();
      $('<body></body>').appendTo('html')
      $(Drupal.behaviors.adairsCore.markup).appendTo('body');
    }
  }

})(jQuery);