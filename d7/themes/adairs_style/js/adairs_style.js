(function ($) {

  Drupal.behaviors.adairs_style = {
    attach: function(context, settings) {
      if ($('.ctools-modal-content').length > 0) {
        context = $('.ctools-modal-content')[0];
      }

      // Gallery
      Drupal.behaviors.adairs_style.createProductDisplayGallery(context, settings);
      Drupal.behaviors.adairs_style.buildPseudoElements(context, settings);
      if (Drupal.settings.adairs_commerce_page_variant == 'b')
        Drupal.behaviors.adairs_style.createColourThumbs(context, settings);

      Drupal.behaviors.adairs_style.clickToCall(context, settings);
    },  //attach

    createProductDisplayGallery: function(context, settings) {
      var gallery_set = '.gallery.gallery-product-display, .gallery-thumbs, .pager-left, .pager-right';

      // Product gallery
      $('.node-product-display .commerce-product-field-field-image').once('gallery', function(){

        var field_images = $('.commerce-product-field-field-image');
        field_images.find('img').wrapAll('<div class="gallery gallery-product-display"/>');
        var gallery = field_images.find('.gallery');
        var thumbs = $('<div/>').addClass('gallery-thumbs').append(gallery.find('img').clone());
        var pagers = $("<span class='pager-left'/><span class='pager-right'/>");
        $(gallery).append('<span class="gallery-trigger"/>');
        $(gallery).append('<span class="gallery-zoom-trigger"/>');

        // Update mobile image (Hidden in desktop but shown in mobile)
        var mobileImage = field_images.find('img').first();
        $('.mobile-product-image').find('img').attr({
          src: mobileImage.attr('src'),
          width: 100,
          height: 100
        });

        // Add product colour label to the gallery container
        Drupal.behaviors.adairs_style.updateGalleryColour(context, settings);

        $('.gallery-trigger', context).click(function(event) {
          $(gallery_set).fadeToggle('slow/400/fast');
          // @ISSUE: quick fix to remove left margin on @640 bracket
          $(gallery_set).find('img').css('margin-left', '0px');
        });

        field_images.append(thumbs).append(pagers);

        gallery.cycle({
          timeout: 0,
          fluid: true,
          centerHorz: true,
          swipe: true,
          log: false
        });

        thumbs.cycle({
          fx: 'carousel',
          allowWrap: false,
          timeout: 0,
          fluid: true,
          next: '.pager-right',
          prev: '.pager-left',
          log: false
        });

        // Behaviours
        $('.pager-left', context).click(function(){
          gallery.cycle('prev');
          // thumbs.cycle('prev');
        });
        $('.pager-right', context).click(function(){
          gallery.cycle('next');
          // thumbs.cycle('next');
        });
        thumbs.find('.cycle-slide').click(function(){
          var slideCount = thumbs.data('cycle.API').opts().slideCount;
          var index = thumbs.data('cycle.API').getSlideIndex(this) % slideCount;
          gallery.cycle('goto', index);
          // thumbs.cycle('goto', index);
        });

      });

      Drupal.behaviors.adairs_style.initGalleryState(gallery_set);

      $(window).resize(function(event){
        Drupal.behaviors.adairs_style.initGalleryState(gallery_set);
      });

      $(gallery_set).click(function(event) {
        var current_width = $(window).width();
        if ($(window).width() >= 640)
          return event;
        return false;
      });
    },  // createProductDisplayGallery

    // Thumbnails for product colours only
    createColourThumbs: function(context, settings) {
      $('.form-item-attributes-field-colour', context).once(function(){
        var thumbs  = $('div.custom-dd');
        thumbs.cycle({
          fx: 'carousel',
          allowWrap: false,
          timeout: 0,
          speed: 250,
          swipe: true,
          fluid: true,
          slides: '.custom-dd-colour',
          prev: '.slider-prev',
          next: '.slider-next',
          log: false
        });
      });
    },

    initGalleryState: function(gallery) {
      var current_width = $(window).width();
      if ($(window).width() >= 640) {
        $(gallery).show();
      } else {
        $(gallery).hide();
      }
    },  // initGalleryState

    buildPseudoElements: function(context, settings) {
      // Product display pseudo-form elements (colour/style)

      // Colour pseudo-element
      var colour = $('.form-item-attributes-field-colour:not(.processed)', context).addClass('processed');

      if (Drupal.settings.adairs_commerce_page_variant == 'a') {
        // Select span toggles visibility
        colour.find('.field-prefix', context).click(function(event){
          $(event.target).closest('.form-item').siblings('.form-item').find('.custom-dd').hide();
          colour.find('.custom-dd', context).toggle();
        });
      }

      // Clicking a colour
      colour.find('.custom-dd-colour', context).click(function(){
        // console.log('here');
        if (Drupal.settings.adairs_commerce_page_variant == 'a') {
          // Hide the dropdown now that we've clicked
          colour.find('.custom-dd', context).toggle();
        }

        // Get tid of clicked element and trigger click for corresponding input
        var tid = $(this).data('tid');
        colour.find('.form-type-radio input[value="'+tid+'"]', context).trigger('click');
        // console.log(colour.find('.form-type-radio input[value="'+tid+'"]', context));
      });

      if (colour.length > 0) {
        // We can set label colour text from the existing product-colour label
        var text = $('.product-colour', context).text().match(/:\s*(.*)/)[1];
        pseudoElemUpdateLabel('.form-type-radios.form-item-attributes-field-colour > label', context, 'Select colour: ', text);
      }

      // If we only have one radio button, hide the selector
      if (Drupal.settings.adairs_commerce_page_variant == 'a') {
        if (colour.find('.form-type-radio').length <= 1) {
          colour.find('.field-prefix').hide();
        }
      }

      // Style pseudo-element
      var style = $('.form-item-attributes-field-style:not(.processed)', context).addClass('processed');
      style.find('.field-prefix').click(function(event){
        if (Drupal.settings.adairs_commerce_page_variant == 'a')
          $(event.target).closest('.form-item').siblings('.form-item').find('.custom-dd').hide();
        style.find('.custom-dd').toggle();
      });

      // Clicking a style
      style.find('.custom-dd-size').click(function(){
        // Hide the dropdown now that we've clicked
        style.find('.custom-dd').toggle();
        // Get index of clicked element and trigger click for corresponding input
        var i = style.find('.custom-dd-size').index($(this));
        var select = style.find('.form-select');
        // Set the dropdown value to the value matching clicked index and trigger a change
        select.val(select.find('option').eq(i).val()).change();
      });

      if (style.length > 0) {
        if (Drupal.settings.adairs_commerce_page_variant == 'a') {
          // We can set label style text from the existing style options list
          var text = style.find('option:selected').text().match(/-\s*(.*)/)[1];
          pseudoElemUpdateLabel('.form-type-select.form-item-attributes-field-style > label', context, 'Style: ', text);
        } else if (Drupal.settings.adairs_commerce_page_variant == 'b') {
          pseudoElemUpdateLabel('.form-type-select.form-item-attributes-field-style > label', context, 'Select style', '');
        }
      }

      // If we only have one option, hide the selector
      if (Drupal.settings.adairs_commerce_page_variant == 'a') {
        if (style.find('.form-select option').length <= 1) {
          style.find('.field-prefix').hide();
        }
      }

      function pseudoElemUpdateLabel(selector, context, label, value) {
        var text = label + "<span>"+value+"</span>";
        $(selector, context).html(text);
      }
    }, // buildPseudoElements

    updateGalleryColour: function(context, settings) {
      $('.product-colour').appendTo($('.gallery', context));
    }, // updateGalleryColour

    clickToCall: function(context, settings) {
      // Wrap phone numbers in anchor with tel: URL in store listing and store locator areas.
      $('.view-store-locations .views-field-field-more-information .field-content > p, .google-store-locator-map-container .address > p').each(function(){
        var storeWrapNumber = function(text) {
          // Regex that matches a plaintext 'P <number>' pattern
          return text.replace(/P[\s|&nbsp;]+(.*?)</,'P <a href="tel:$1">$1</a><');
        }
        var p = $(this);
        var replace = storeWrapNumber(p.html());
        // Replace the HTML with our processed HTML
        p.html(replace);
      });
    }

  };


})(jQuery);
