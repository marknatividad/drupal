// Miscellaneous
(function ($) {

  Drupal.behaviors.adairsCore = {

    attach: function (context, settings) {


      // Toggle collapsibles
      // Configure custom drop downs
      // All other custom-dd events are in pseudo-elements.js
      Drupal.behaviors.adairsCore.setupCollapsibles(context, settings);
      Drupal.behaviors.adairsCore.setupDropdowns(context, settings);
      Drupal.behaviors.adairsCore.tapMenu(context, settings);
      Drupal.behaviors.adairsCore.mobileSearchBox(context, settings);
      Drupal.behaviors.adairsCore.throbberFixes(context, settings);
      Drupal.behaviors.adairsCore.lookBookRemover(context, settings);
      Drupal.behaviors.adairsCore.placeHolders(context, settings);
      Drupal.behaviors.adairsCore.addToCartProcessing(context, settings);
      // Drupal.behaviors.adairsCore.moveDuplicateCheckoutCouponBlocks(context, settings);


      // All other miscelleneous items
      Drupal.behaviors.adairsCore.misc(context, settings);

    },

    // @REFACTOR: Abstract click and hover events to assert device width
    tapMenu: function (context, settings) {

      /* Utility Blocks Menu */
      var utility_blocks = [
        {'element' : '.utility-block-01',  'content' : '.u-block-content'},
        {'element' : '.utility-block-03',  'content' : 'ul.menu'},
        {'element' : '.utility-block-03a', 'content' : 'ul.menu'},
        {'element' : '.utility-block-04',  'content' : '.u-block-cart'},
      ]
      $(utility_blocks).each(function(index, el) {
        // Enable click for touch devices
        $(this.element).once().click(function(event) {
          if (Drupal.behaviors.adairsCore.isTouchDevice()) {
            $(this).find(el.content).toggle();
            $(this).siblings('div').children('div, ul').hide();
          }
        });
        // Enable hover for desktop
        $(this.element).hover(function() {
          if(!Drupal.behaviors.adairsCore.isTouchDevice())
            $(this).find(el.content).show();
        }, function() {
          if(!Drupal.behaviors.adairsCore.isTouchDevice())
           $(this).find(el.content).hide();
        });
      });

      /* Main Menu */
      // Click
      $('#page-nav').find('li').once().click(function(event) {
        if (Drupal.behaviors.adairsCore.isTouchDevice()) {
          Drupal.behaviors.adairsCore.adairsMenuToggle(event, this);
          // Remove clickability of 'Shop' and 'Style & Advice' (keep Sale clickable)
          if ($(this).hasClass('expanded') || $(this).hasClass('back'))
            event.preventDefault();
        }
      });
      // Hover
      $('#page-nav', context).find('li:not(.processed)').hover(function() {
        /* Stuff to do when the mouse enters the element */
        $(this).addClass('processed');
        if(!Drupal.behaviors.adairsCore.isTouchDevice()) {
          $(this).addClass('level-0-selected');
          $(this).children('ul').show();
        }
      }, function() {
        /* Stuff to do when the mouse leaves the element */
        $(this).addClass('processed');
        if(!Drupal.behaviors.adairsCore.isTouchDevice()) {
          $(this).removeClass('level-0-selected');
          $(this).children('ul').hide();
        }
      });

      // Similate click location event for cart (but only on desktop)
      $('.utility-block-04').click(function(event) {
        if (!Drupal.behaviors.adairsCore.isTouchDevice()) {
          // Check to see if shopping cart icon, $amount or title clicked, but not the modal
          if ($(event.target).hasClass("cart-trigger")) {
            window.location = '/cart';
          }
        }
      });

      if (Drupal.behaviors.adairsCore.isTouchDevice()) {

        var menu = $('.my-account-menu').find('ul.menu');
        var currentLabel = $(menu).find("a[href='"+location.pathname+"']").html();
        currentLabel = (currentLabel == null) ? 'Menu' : currentLabel;
        $(menu).removeClass('menu').addClass('custom-dd-filter-menu').before("<label class='custom-el-trigger'>"+currentLabel+"<span></span></label>");
        $('ul.custom-dd-filter-menu').find("a[href='/user/logout']").addClass('logoff').insertAfter('ul.custom-dd-filter-menu');
      }
    },

    setupCollapsibles: function (context, settings) {

      // Various pages to enable and disable 'collapsed' class
      var collapsibleLocations = [
        {'collapsibleLocation' : '.page-footer-wrapper .onq_core-collapsible-handle'},
        {'collapsibleLocation' : '.page-stores .onq_core-collapsible-handle'},
        {'collapsibleLocation' : '.page-sitemap .onq_core-collapsible-handle'},
        {'collapsibleLocation' : '.footer-subscription .onq_core-collapsible-handle .info'},
        {'collapsibleLocation' : '.adairs-style-your-feedback .onq_core-collapsible-handle'},
        {'collapsibleLocation' : '.adairs-style-content-contact-us .onq_core-collapsible-handle'},
      ];

      $(collapsibleLocations).each(function(index, el) {
        // Toggle class to show '+' or '-'
        $(this.collapsibleLocation).once().click(function(event) {
          $(this).toggleClass('collapsed');
        });
      });

      // Product Detail Page
      $('.onq_core-collapsible-handle a').once().click(function(){
          // Toggle class to show plus or minus
          $(this).find('span.right').toggleClass('icon-s32-5 icon-s32-6');
        })

      // My Cart - coupon block
      $('#edit-coupon-code-fieldset h5, .onq_core-collapsible-handle h3').once().on('click', function(){
          // Toggle class to show plus or minus
          $(this).toggleClass('trigger-collapsed trigger-expanded');
        })
    },

    setupDropdowns: function (context, settings) {

      // Drop down for the videos / look book pages
      $('body').on('click', '.custom-el-trigger', function(event) {
        $(this).siblings('.custom-dd-filter-menu').toggle();
        $(this).children('.custom-dd-filter-menu').toggle();
      });

      // Lookbook help drop down
      $('.lookbook-help').once().click(function(event){
        $('.custom-dd').toggle();
      });

      // On this page we need to close the other boxes that are open
      $('body').on('click', function(event) {
        if ($(event.target).hasClass('custom-el-trigger') == false)
          $('.custom-dd-filter-menu').hide();
      });

      // Drop down for customer reviews page 'Write a Review'
      $('#write_review_btn').once().click(function(event){
        event.preventDefault();
        if(!$(this).hasClass('disabled'))
          $('#block-adairs-core-customer-review-modal').toggle();
      })
    },

    adairsMenuToggle: function(event, element) {
      event.stopPropagation();

      // Don't show the image for level-1
      if($(element).children('ul').html() == '') return false;

      // Enable 'Go back' option
      if($(element).children('a').hasClass('menu-item-back')) {
        $(element).parent('ul').hide();
      }
      // Hide any other menu lists that are open
      $(element).siblings('li.expanded').each(function(index, el) {
        $(this).removeClass('level-0-selected');
        $(this).find('ul:visible').each(function(index, el) {
            $(el).hide();
        });
      });

      // Then show this one
      $(element).children('ul').toggle();
      $(element).toggleClass('level-0-selected');
    },

    isTouchDevice: function() {
      return ($(window).width() <= 768);
    },

    mobileSearchBox: function (context, settings) {

      // Display search box
      $('.search-mobile-switch').once().click(function(event) {
        if(Drupal.behaviors.adairsCore.isTouchDevice()) {

          event.stopPropagation();

          // Show / Hide the search box
          $('.region-search').toggle();
          // Change background of search switch
          $(this).toggleClass('active');
        }
      });
    },

    // Remove lookbook modal when switched to landscape if open
    lookBookRemover: function (context, settings) {
      $( window ).resize(function() {
        if ($(window).width() < 480) {
          $('.page-lookbook #modalBackdrop').remove();
          $('.page-lookbook #modalContent').remove();
        }
      });
    },


    throbberFixes: function (context, settings) {

    },

    // Miscellaneous fixes
    misc: function (context, settings) {
      // Move around subscribe checkbox and captcha img on Feedback and Contact us pages.
      $('#webform-component-subscribe').appendTo('fieldset.captcha');
      $('.fieldset.captcha').find('img').appendTo('.form-item-captcha-response');

      $('.adairs-style-user-register').find('.field-name-field-sign-up').appendTo('.captcha');
      $('.adairs-style-user-register').find('.form-item-field-terms-cond').appendTo('.field-name-field-sign-up');

      // Move page title on selected pages
      var headingPages = [
        {'headingPage' : '.adairs-style-careers'},
        {'headingPage' : '.adairs-style-about-us'},
      ];

      $(headingPages).each(function(index, el) {
        $(this.headingPage).find('.page-title-wrapper').prependTo('.page-body-center');
      });

      // Move sales-qty
      $('.qty-badge').prependTo('.utility-block-04');

      // Adjust value of select drop downs on contact and feedback pages
      $('.webform-client-form .form-select').each(function(index, el) {
        $(el).find('option').eq(0).html('-- please select --');
      });

      // Move Add to Cart on Checkout page
      //$('.page-checkout .extra_pane__node__349', context).once().prependTo('.page-title-wrapper');

    },

    placeHolders: function(context, settings) {
      // Add placeholders to webforms different pages
      $('.webform-client-form #edit-submitted-name').attr('placeholder', 'eg. Jane Smith');
      $('.webform-client-form #edit-submitted-email').attr('placeholder', 'eg. janesmith@email.com');
      $('.webform-client-form #edit-submitted-contact-number').attr('placeholder', 'eg. 0400123456');
      $('.webform-client-form #edit-captcha-response').attr('placeholder', 'eg. DHF234D');


      // Checkout page placeholders
      var placeholders = [
        { 'element'     : '.form-item-account-login-mail',
          'placeholder' : 'eg. janesmith@email.com'},
        { 'element'    : '.form-item-customer-profile-billing-field-phone-number-und-0-value',
          'placeholder' : 'eg. 0400123456'},
        { 'element'    : '.form-item-customer-profile-billing-commerce-customer-address-und-0-first-name',
          'placeholder' : 'eg. Jane'},
        { 'element'    : '.form-item-customer-profile-billing-commerce-customer-address-und-0-last-name',
          'placeholder' : 'eg. Smith'},
        { 'element'    : '.form-item-customer-profile-billing-commerce-customer-address-und-0-thoroughfare',
          'placeholder' : 'eg. 123 Smith Street'},
        { 'element'    : '.form-item-customer-profile-billing-commerce-customer-address-und-0-locality',
          'placeholder' : 'eg. Melbourne'},
        { 'element'    : '.form-item-customer-profile-billing-commerce-customer-address-und-0-postal-code',
          'placeholder' : 'eg. 4321'},
        { 'element'    : '.form-item-customer-profile-shipping-commerce-customer-address-und-0-first-name',
          'placeholder' : 'eg. Jane'},
        { 'element'    : '.form-item-customer-profile-shipping-commerce-customer-address-und-0-last-name',
          'placeholder' : 'eg. Smith'},
        { 'element'    : '.form-item-customer-profile-shipping-commerce-customer-address-und-0-organisation-name',
          'placeholder' : 'eg. JS Florists'},
        { 'element'    : '.form-item-customer-profile-shipping-commerce-customer-address-und-0-thoroughfare',
          'placeholder' : 'eg. 123 Smith Street'},
        { 'element'    : '.form-item-customer-profile-shipping-commerce-customer-address-und-0-locality',
          'placeholder' : 'eg. Melbourne'},
        { 'element'    : '.form-item-customer-profile-shipping-commerce-customer-address-und-0-postal-code',
          'placeholder' : 'eg. 4321'},
        { 'element'    : '.form-item-commerce-payment-payment-details-credit-card-name',
          'placeholder' : 'eg. Ms Jane Smith'},
        { 'element'    : '.form-item-commerce-payment-payment-details-credit-card-number',
          'placeholder' : 'eg. XXXXXXXXXXXXXXXX'},
        { 'element'    : '.form-item-commerce-payment-payment-details-credit-card-code',
          'placeholder' : 'eg. 123'},
      ];

      // $(placeholders).each(function(index, el) {
      //   $('.page-checkout ' + this.element, context).find('input[type="text"').attr('placeholder', this.placeholder);
      // });

      // Checkout page select options
      var selectOptions = [
        { 'element'    : '.form-item-customer-profile-billing-commerce-customer-address-und-0-administrative-area'},
        { 'element'    : '.form-item-customer-profile-shipping-commerce-customer-address-und-0-administrative-area'},
      ];

      // $(selectOptions).each(function(index, el) {
      //   $('.page-checkout ' + this.element, context).find('.form-select option').eq(0).html('-- please select --');
      // });

    },

    // Add throbber to button and change text when form submitted
    addToCartProcessing: function(context, settings) {

      // Move page title on selected pages
      var buttons = [
        {'button' : '.commerce-add-to-cart .btn-submit'},
        //{'button' : ''},
      ];

      $("form input[type=submit]").click(function() {
          $("input[type=submit]", $(this).parents("form")).removeAttr("clicked");
          $(this).attr("clicked", "true");
      });

      $("#edit-add-to-wishlist").click(function() {
        $(this).addClass('submit-processing');
        // Show drupal throbber to give user feedback
        $('#throbber-default').css('visibility', 'visible');
      });

      $(".commerce-add-to-cart #edit-submit").click(function() {
        $(this).addClass('submit-processing');
        $(this).val('Adding to cart...');
        // Show drupal throbber to give user feedback
        $('#throbber-default').css('visibility', 'visible');
      });

      // $(buttons).each(function(index, el) {
      //   $(this.button).on('click', function(event) {
      //     /* Act on the event */
      //     // event.preventDefault();
      //     // console.log('prevented');
      //     $(this).addClass('submit-processing');
      //     // console.log('prevented');
      //     $(this).val('Adding to Cart...');

      //     // Drupal.behaviors.adairsCore.afterAddToCartProcessing($(this));
      //   });
      // });

    },

    // Move instance of checkout coupon blocks
    moveDuplicateCheckoutCouponBlocks: function(context, settings) {
      // Move and display
      var destination = $('.cart_contents > .fieldset-content');
      var couponBlock = $('.join-us-block-review-confirm');
      var newBlock = $(couponBlock).once().prependTo(destination);
      // Display at break point
      if($(window).width() <= 980) newBlock.show();
    }
  }

})(jQuery);

// Extra functionality for visibility hidden and visible
(function($) {
    $.fn.invisible = function() {
        return this.each(function() {
            $(this).css("visibility", "hidden");
        });
    };
    $.fn.visible = function() {
        return this.each(function() {
            $(this).css("visibility", "visible");
        });
    };
}(jQuery));
