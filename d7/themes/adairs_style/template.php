<?php
/**
 * Template file for preprocess functions
 *
 * @see template_theming.php for theme_HOOKS()
 *
 */

// Include other template files...
require_once('template_theming.php');

$theme_path = drupal_get_path('theme', 'adairs_style');
include_once($theme_path . '/includes/pager.inc');

/**
 * @ISSUE move me to a more generic place
 * Renders a view field once (unless $once passed in as false)
 * Specifically for improving the ease of override for views row template when
 * we want to display some fields in specialised markup, and then continue
 * to output the rest
 */
function onq_vf($fields, $field, $once = true) {
  $field = &$fields[$field];

  if ($once) {
    if (isset($field->shown)) {
      return;
    } else {
      $field->shown = true;
    }
  }

  if (!empty($field->separator)) {
    print $field->separator;
  }
  print $field->wrapper_prefix;
  print $field->label_html;
  print $field->content;
  if (!empty($field->wrapper_suffix)) {
    print $field->wrapper_suffix;
  }
}


// ---------------------------------------------------------

/**
 * Implements template_preprocess_html()
 *
 */
function adairs_style_preprocess_html(&$variables) {
  global $theme;
  // Add JS sale cycle integration
  drupal_add_js(drupal_get_path('theme', $theme) . '/js/sale-cycle.js', array('type' => 'file', 'scope' => 'footer'));
  #drupal_add_js(drupal_get_path('theme', $theme) . '/js/ga-social-tracking.js', array('type' => 'file', 'scope' => 'footer'));

  // Add more classes to body based on current node path
  $node_url = drupal_get_path_alias();
  $variables['classes_array'][] = drupal_clean_css_identifier($theme . '-' . $node_url);

  // Add conditional CSS for IE8.
  drupal_add_css(path_to_theme() . '/css/ie8.css.less', array('group' => CSS_THEME, 'browsers' => array('IE' => 'IE 8', '!IE' => FALSE), 'preprocess' => FALSE));

  // Add conditional CSS for IE7.
  drupal_add_css(path_to_theme() . '/css/ie7.css.less', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lt IE 8', '!IE' => FALSE), 'preprocess' => FALSE));

}

// ---------------------------------------------------------

/**
 * Implements template_preprocess_page()
 *
 */
function adairs_style_preprocess_page(&$vars, $hook) {
  // Set adairs logo URL
  $vars['adairs_logo_url'] = '/';

  // Insert the Adairs Kids logo if taxonomy exists in logo
  if(adairs_core_kids_current_path_is_adairs_kids()) {
    global $base_root;
    // Adjust logo
    $vars['logo'] = $base_root . base_path() . path_to_theme(). '/adairs-kids-logo.png';
    $vars['adairs_logo_url'] = $base_root . '/adairs-kids/';
  }


  // Main menu
  $vars['primary_nav'] = FALSE;
  if ($vars['main_menu']) {
    // Build links
    $menu_name = variable_get('menu_main_links_source', 'main-menu');

    if (module_exists('adairs_core_kids') && adairs_core_kids_current_path_is_adairs_kids()) {
      /*
      $cache_key = 'adairs_style_preprocess_page_menu_tree_kids';
      if (!($vars['primary_nav'] = apc_fetch($cache_key))) {
        $menu_name = adairs_core_kids_menu_source();
        $tree = menu_tree_all_data($menu_name);
        $vars['primary_nav'] = menu_tree_output($tree);
        apc_add($cache_key, $vars['primary_nav']);
      }
      */
      $menu_name = adairs_core_kids_menu_source();
      $tree = menu_tree_all_data($menu_name);
      $vars['primary_nav'] = menu_tree_output($tree);
    } else {
      //$cache_key = 'adairs_style_preprocess_page_menu_tree_parent';
      //$vars['primary_nav'] = onq_core_apc_cache('menu_tree', array($menu_name), $cache_key);
      $vars['primary_nav'] = menu_tree($menu_name);
    }
  }

  // Provide default theme wrapper function
  $vars['primary_nav']['#theme_wrappers'] = array('menu_tree__primary');

  if(current_path() == 'cart'){
    $vars['title_suffix'][] = array('button' => array('#markup' => '<a href="checkout" class="btn btn-green" >Checkout › </a>'));
  }

  // Collapsible theme for Footer Menu - Parent
  if (array_key_exists('menu_menu-footer-menu', $vars['page']['footer_center'])){
    $vars['page']['footer_center']['menu_menu-footer-menu']['#theme_wrappers'] = array('menu_tree__footer', 'block');
  }

  // Collapsible theme for Shop the Adairs Range footer menu - Parent
  if (array_key_exists('taxonomy_menu_block_1', $vars['page']['footer_center'])){
    $vars['page']['footer_center']['taxonomy_menu_block_1']['#theme_wrappers'] = array('menu_tree__taxonomy', 'block');
  }

  // Collapsible theme for Footer Menu - Kids
  if (array_key_exists('menu_menu-footer-menu-adairs-kids', $vars['page']['footer_center'])){
    $vars['page']['footer_center']['menu_menu-footer-menu-adairs-kids']['#theme_wrappers'] = array('menu_tree__footer', 'block');
  }

  // Collapsible theme for Shop the Adairs Range footer menu - Kids
  if (array_key_exists('taxonomy_menu_block_2', $vars['page']['footer_center'])){
    $vars['page']['footer_center']['taxonomy_menu_block_2']['#theme_wrappers'] = array('menu_tree__taxonomy', 'block');
  }

  // Product category banner
  $vars['category_banner'] = FALSE;
  if (adairs_core_is_taxonomy_page() && drupal_get_path_alias() != 'adairs-kids') {
    $vars['category_banner'] = adairs_core_category_banner_view(arg(2));
  }

  if (isset($vars['node']) && in_array($vars['node']->type, array('product_display', 'gift_card_display', 'membership_display'))) {
    // Hide the title on product display pages, as H1 comes from right column product title
    drupal_set_title('');
  }

 //Display banner before title for all default pages
  if (!empty($vars['node'])) {
    // $vars['theme_hook_suggestions'][] = 'page__node__' . $vars['node']->type;
  }
  if (isset($vars['node']) && $vars['node']->type == 'page') {
    $node = $vars['node'];
    if (isset($node) && $node) {
      $vars['banner_image'] = field_view_field('node', $node, 'field_banner_image', array('label'=>'hidden'));
      $vars['body'] = field_view_field('node', $node, 'body', array('label'=>'hidden'));
    }
  }
  // Preprocess page based on 'node' type
  if (isset($vars['node'])) {
    $function = __FUNCTION__ . '_' . $vars['node']->type;
    if (function_exists($function))
      $function($vars);
  }

}

// ---------------------------------------------------------

/**
 * Implements template_preprocess_page_lookbook_slide()
 *
 */
function adairs_style_preprocess_page_lookbook_slide(&$vars) {
  // dpm($vars['node']);
  $node_wrapper = entity_metadata_wrapper('node', $vars['node']);
  // dpm($node_wrapper->field_slide_products->value());
}


// ---------------------------------------------------------

/**
 * Implements template_preprocess_node()
 *
 */
function adairs_style_preprocess_node(&$variables) {
  if($variables['type'] == 'membership_display'){
    $linen_lovers = $variables['elements']['product:commerce_price']['#object'];
    $variables['content']['product:commerce_price'][0]['#markup'] = commerce_currency_format($linen_lovers->commerce_price['und'][0]['original']['amount'], commerce_default_currency());
  }
}

function adairs_style_page_alter(&$page) {
  if (drupal_is_front_page()) {
    $metatags = array();
    $options = array();
    $data = metatag_metatags_view("global:frontpage", $metatags, $options);
    $page['content']['metatags'] = array();
    $page['content']['metatags']["global:frontpage"] = $data;
  }
}

// ---------------------------------------------------------

/**
 * Implements template_preprocess_block()
 *
 */
function adairs_style_preprocess_block(&$variables) {
  if($variables['block_html_id'] == 'block-menu-menu-footer-menu'){
    $variables['title_attributes_array']['class'][] = 'onq_core-collapsible-handle';
  }
  if(in_array('footer-block-connect-right', $variables['classes_array'])){
    $handle = '<h2 class="block-title clearfix">CONNECT WITH ADAIRS</h2>';
    $output = $variables['content'];
    $options = array('handle' => $handle, 'content' => $output, 'collapsed' => TRUE, 'restrict_mobile' => TRUE);
    $variables['content'] = theme('onq_core_collapsible', $options);
  }
}

// ---------------------------------------------------------

/**
 * Implements theme form alter()
 *
 */
function adairs_style_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'google_appliance_block_form') {
    // Hide the value of the GSA submit button
    $form['actions']['submit']['#value'] = '';
  }

  if($form_id == 'user_login') {
    $form['actions']['submit']['#value'] = 'Log in';
  }

  // Make Product Details button + Add to Cart
  if(strpos($form_id, 'commerce_cart_add_to_cart_form') !== FALSE) {
    if($form['submit']['#disabled'] !== 1) {
      $form['submit']['#value'] = '+ ' . $form['submit']['#value'];
    }
  }
  // Delete button should not present on the form
  if ($form_id == 'views_form_cart_summary_block_default') {
    if (isset($form['edit_delete'])) {
      foreach ($form['edit_delete'] as $row_id => $row) {
        if(isset($form['edit_delete'][$row_id]['#value'])){
          $form['edit_delete'][$row_id]['#value'] = ' ';
        }
      }
    }
  }

  if($form_id == 'onq_commerce_coupons_coupon_entry_form') {
    //dpm($form['coupon_code_fieldset']['#title']);
  }
}


// // ---------------------------------------------------------

// /**
//  * Implements theme form alter()
//  *
//  */
// function adairs_style_form_onq_commerce_coupons_coupon_entry_form(&$form, $form_state, $form_id) {

//   dpm($form);
//   // if ($form_id == 'google_appliance_block_form') {
//   //   // Hide the value of the GSA submit button
//   //   $form['actions']['submit']['#value'] = '';
//   // }

//   // if($form_id == 'user_login') {
//   //   $form['actions']['submit']['#value'] = 'Log in';
//   // }

//   // // Delete button should not present on the form
//   // if ($form_id == 'views_form_cart_summary_block_default') {
//   //   if (isset($form['edit_delete'])) {
//   //     foreach ($form['edit_delete'] as $row_id => $row) {
//   //       if(isset($form['edit_delete'][$row_id]['#value'])){
//   //         $form['edit_delete'][$row_id]['#value'] = ' ';
//   //       }
//   //     }
//   //   }
//   // }
// }



// ---------------------------------------------------------

/**
 * Implements template_preprocess_field()
 *
 */
function adairs_style_preprocess_field(&$vars) {
  // dpm($vars);
}
