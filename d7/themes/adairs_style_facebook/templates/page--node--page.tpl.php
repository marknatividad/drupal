<div id="page-wrapper">
  <?php if ($primary_local_tasks): ?><ul class='action-btn-group clearfix'><?php print render($primary_local_tasks) ?></ul><?php endif; ?>
  <?php if ($secondary_local_tasks): ?><ul class='clearfix'><?php print render($secondary_local_tasks) ?></ul><?php endif; ?>
  <?php if ($action_links): ?><ul class='clearfix'><?php print render($action_links); ?></ul><?php endif; ?>

  <div id="page-header-wrapper">
    <div class="page-header">
      <div id="logo">
        <a href="" target="_blank"><img src='<?php print $logo ?>' alt='Adairs Logo' title="Adairs Logo" itemprop="logo" /></a>
      </div><!-- /logo -->
      <div class="header-nav">
        <?php print render($page['right_header']) ?>
      </div><!-- /header-nav -->
    </div><!-- page-header- -->
    <div class="page-banner home-banner">
      <?php print render($banner_image);?>
      <div id="home_like">
        <span class="hero-style1">welcome to</span> <br />
        <span class="hero-style2">ADAIRS</span><br />
        <div class="line"></div><br />
        <span class="hero-style3">
          click &nbsp;&nbsp;
          <div class="home-like-wrapper">
            <div class="fb-like" data-href="" data-send="false" data-width="45" data-show-faces="false"></div>
          </div>&nbsp;&nbsp; to stay in touch
        </span>
      </div>
    </div><!-- /page-banner -->
    <div class="info-bar baseColorPink">
      <?php print render($page['information_bar']) ?>
    </div><!-- /info-bar -->
    <div id="page-body-wrapper">
      <div class="page-body">
          <div class="page-body-left">
            <div class="home-intro">
              <div class="baseColorPurple"><?php if ($title): ?><h1 class='page-title'><?php print $title ?></h1><?php endif; ?></div>
              <div class="content ">
                <?php print render($body) ?>
              </div><!-- /content -->
            </div><!-- /home-intro -->
          </div><!-- /page-body-left -->
          <div class="page-body-right">
            <div class="home-offer">
              <div class="section-header baseColorCyan">Explore <span>the best selling range from Adairs</span></div>
              <?php print render($page['sidebar_right']) ?>
            </div>
          </div>
          <div class="clear"></div>
          <?php print render($page['footer']) ?>

      </div>
    </div>
  </div>
</div>

<div id="page-footer-wrapper">
  <div class="page-footer">
    <?php print render($page['footer_menu']) ?>
  </div><!-- /page-footer -->
</div><!-- /page-footer-wrapper -->
<script type="text/javascript" src="https://assets.pinterest.com/js/pinit.js"></script>
