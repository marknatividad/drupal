<?php

/**
 * @file
 * Supporting functions related to admin settings for the Product Selection Tool (PST).
 */

/**
 * Builds the PST settings form.
 */
function mju_product_selector_admin_settings_form($form, &$form_state) {
  $weight = 0;

  /**
   * General settings
   */
  $form['mju_product_selector_page_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Page title'),
    '#default_value' => mju_product_selector_page_title(),
    '#required' => TRUE,
    '#weight' => $weight++
  );

  /**
   * STEP 1 settings
   */
  $form['mju_product_selector_step1'] = array(
    '#type' => 'fieldset',
    '#title' => t('Step 1'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => $weight++
  );
  
  $form['mju_product_selector_step1']['mju_product_selector_step1_page_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('The title displayed for this step.'),
    '#default_value' => mju_product_selector_step_page_title(1),
    '#required' => TRUE,
    '#weight' => $weight++
  );
  
  $question_vids_options = mju_product_selector_admin_question_options();
  $categories = mju_product_selector_taxonomy_tree_options(NULL, FALSE);
  
  $form['mju_product_selector_step1']['mju_product_selector_product_categories_vocabulary'] = array(
    '#type' => 'select',
    '#title' => t('Select a Vocabulary'),
    '#description' => t('The taxonomy vocabulary to provide the terms for the product categories.'),
    '#empty_option' => t('- Select -'),
    '#default_value' => variable_get('mju_product_selector_product_categories_vocabulary', ''),
    '#options' => $question_vids_options,
    '#required' => TRUE,
    '#weight' => $weight++
  );

  /**
   * STEP 2 settings
   */
  $form['mju_product_selector_step2'] = array(
    '#type' => 'fieldset',
    '#title' => t('Step 2'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => (empty($categories)) ? t('Please select a vocabulary from Step 1.') : '',
    '#weight' => $weight++
  );

  $form['mju_product_selector_step2']['mju_product_selector_step2_page_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('The title displayed for this step.'),
    '#default_value' => mju_product_selector_step_page_title(2),
    '#required' => TRUE,
    '#weight' => $weight++
  );

  // Exclude the selected taxonomy from step 1
  $v = mju_product_selector_product_categories_vocabulary();
  if ($v && isset($question_vids_options[$v->vid->value()])) {
    unset($question_vids_options[$v->vid->value()]);
  }

  foreach ($categories as $tid => $name) {
    $form['mju_product_selector_step2']['mju_product_selector_category_' . $tid] = array(
      '#type' => 'fieldset',
      '#title' => t($name),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => $weight++
    );

    $form['mju_product_selector_step2']['mju_product_selector_category_' . $tid]['pst_question_vids_' . $tid] = array(
      '#type' => 'checkboxes',
      '#title' => t('Select taxonomies to link'),
      '#options' => $question_vids_options,
      '#default_value' => variable_get('pst_question_vids_' . $tid, array())
    );
  }
  
  /**
   * STEP 3 settings
   */
  $form['mju_product_selector_step3'] = array(
    '#type' => 'fieldset',
    '#title' => t('Step 3'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => $weight++
  );
  
  $form['mju_product_selector_step3']['mju_product_selector_step3_page_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('The title displayed for this step.'),
    '#default_value' => mju_product_selector_step_page_title(3),
    '#required' => TRUE,
    '#weight' => $weight++
  );

  // Add validation handler
  $form['#validate'][] = 'mju_product_selector_admin_settings_form_validate';

  return system_settings_form($form);
}

/**
 * Form validate handler for the PST admin settings form.
 */
function mju_product_selector_admin_settings_form_validate($form, &$form_state) {
}

/**
 * Returns taxonomy options to link to the terms selected in Step 1. 
 */
function mju_product_selector_admin_question_options() {
  $options = array();
  $vocabs = taxonomy_vocabulary_load_multiple(FALSE);
  foreach ($vocabs as $v) {
    $v_wrapper = entity_metadata_wrapper('taxonomy_vocabulary', $v);
    
    // Only include those taxonomies which are enabled
    if ($v_wrapper->field_pst_vocabulary_status->value())
      $options[$v->vid] = t($v->name);
  }
  return $options;
}
