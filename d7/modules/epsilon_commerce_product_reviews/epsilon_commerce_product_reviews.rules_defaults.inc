<?php

/*
 * @file
 * Default Epsilon commerce product reviews rules.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function epsilon_commerce_product_reviews_default_rules_configuration() {
  $items = array();
  
  $items['rules_send_product_review_confirmation_email'] = entity_import('rules_config', '{ "rules_send_product_review_confirmation_email" : {
      "LABEL" : "Send Product Review Confirmation Email",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Epsilon Commerce Product Reviews" ],
      "REQUIRES" : [ "rules", "comment" ],
      "ON" : { "comment_insert--comment_node_product_display" : { "bundle" : "comment_node_product_display" } },
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "comment:node" ],
            "type" : { "value" : { "product_display" : "product_display" } }
          }
        }
      ],
      "DO" : [
        { "mail" : {
            "to" : "[comment:mail]",
            "subject" : "[site:name] Product Review",
            "message" : "Dear [comment:name],\r\n\r\nReview title: [comment:subject]\r\nReview comments: [comment:body]\r\nDate Submitted: [comment:created]\r\n\r\nThank you for submitting a product review. We will review your submission and post it shortly.\r\n\r\nRegards,\r\n[site:name]",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  
  return $items;
}
