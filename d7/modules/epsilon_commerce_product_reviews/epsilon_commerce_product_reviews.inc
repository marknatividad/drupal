<?php

/**
 * Returns the comment author display name.
 *
 * @param $uid
 */
function epsilon_commerce_product_reviews_comment_author($uid) {
  if ($user = user_load($uid)) {
    return $user->name;
  }
  return NULL;
}

/**
 * Utility function to retrieve VotingAPI votes summary.
 *
 * @param $nid
 * @return array
 */
function epsilon_commerce_product_reviews_product_display_reviews_summary($nid) {
  // @TODO: Cache ratings
  
  // The number of stars displayed in the fivestar widget
  $average = epsilon_commerce_product_reviews_product_display_average_comments($nid);
  $number_reviews = epsilon_commerce_product_reviews_product_display_total_comments($nid);

  $average = round(($average / 20) * 2) / 2;
  $summary['average'] = $average;
  $summary['count'] = $number_reviews;

  return $summary;
}

/**
 * Returns the average rating of reviews for a product display.
 *
 * @param $nid
 */
function epsilon_commerce_product_reviews_product_display_average_comments($nid) {
  if (!$nid) {
    return 0;
  }
  
  return db_query('SELECT AVG(r.field_rating_rating)
    FROM comment c JOIN field_data_field_rating r ON (c.cid = r.entity_id)
    WHERE c.nid = :nid AND c.status = :status', array(
    ':nid' => $nid,
    ':status' => COMMENT_PUBLISHED
  ))->fetchField();
}

/**
 * Returns the number of reviews approved by the admin for a node.
 *
 * @param $nid
 */
function epsilon_commerce_product_reviews_product_display_total_comments($nid) {
  if (!$nid) {
    return 0;
  }

  return db_query('SELECT COUNT(cid)
    FROM {comment}
    WHERE nid = :nid AND status = :status', array(
    ':nid' => $nid,
    ':status' => COMMENT_PUBLISHED
  ))->fetchField();
}

/**
 * Returns sort options used in the reviews page.
 */
function epsilon_commerce_product_reviews_sort_options() {
  return array(
    'comment_created,DESC' => 'Newest',
    'comment_created,ASC' => 'Oldest',
    'field_rating_rating,DESC' => 'Highest Rating',
    'field_rating_rating,ASC' => 'Lowest Rating'
  );
}
