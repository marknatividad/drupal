<?php
/**
 * @file
 * Default theme implementation for reviews ratings.
 *
 * Available variables:
 * - $value: The average review rating.
 */
?>
<span>Rating: <?php print $value; ?></span>
<meta content="5" property="schema:bestRating">
<meta content="<?php print $value; ?>" property="schema:ratingValue">
<meta content="0" property="schema:worstRating">
