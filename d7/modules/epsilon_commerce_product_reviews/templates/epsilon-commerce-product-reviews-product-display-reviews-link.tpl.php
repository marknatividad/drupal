<?php
/**
 * @file
 * Default theme implementation for reviews link.
 *
 * Available variables:
 * - $url: The node URL alias.
 * - $text: The text for the anchor tag.
 * - $count: The number of product reviews (comments) the current node has.
 */
?>
<span>
  <a href="/product-reviews<?php print $url; ?>"><?php print t('(@num reviews)', array('@num' => $count)); ?></a>
</span>
