(function ($) {

  Drupal.behaviors.epsilon_commerce_product_reviews = {
    attach: function (context, settings) {
      // Hide the product reviews form block
      var formModal = $('#block-epsilon-commerce-product-reviews-product-reviews-form-modal');
      formModal.once('product-reviews-form-modal', function() {
        if (formModal.is(':visible')) {
          $(formModal).hide();
        }
      });
      
      // Show/hide the reviews form block
      $('#product-reviews-write').once('product-reviews-write').click(function(e) {
        e.preventDefault();
        $('#block-epsilon-commerce-product-reviews-product-reviews-form-modal').toggle();
      });
    }
  };
  
})(jQuery);
