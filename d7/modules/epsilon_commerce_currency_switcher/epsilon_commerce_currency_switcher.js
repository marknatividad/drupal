(function ($) {
  /**
    * Hide the submit button and submit the form as soon as the currency is
    * changed.
    */
  Drupal.behaviors.epsilon_commerce_currency_switcher = {
    attach: function (context, settings) {
      $('#edit-save-selected-currency.form-submit').hide();
    }
  };
})(jQuery);
