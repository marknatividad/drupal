<?php

/**
 * @file
 * Provides metadata for the Minijumbuk warranty entity.
 */

/**
 * Implements hook_entity_property_info().
 */
function mju_warranty_entity_property_info() {
  $info = array();
  
  // Add meta-data about the basic mju_warranty properties.
  $properties = &$info['mju_warranty']['properties'];

  $properties['warranty_id'] = array(
    'type' => 'integer',
    'label' => t('Warranty ID', array(), array('context' => 'a warranty registration')),
    'description' => t('The internal numeric ID of the registration.'),
    'schema field' => 'warranty_id'
  );
  
  $properties['type'] = array(
    'type' => 'text',
    'label' => t('Type'),
    'description' => t('The type of this registration.'),
    'setter callback' => 'entity_property_verbatim_set',
    'options list' => 'mju_warranty_type_options_list',
    'required' => TRUE,
    'schema field' => 'type'
  );
  
  // Customer detail fields
  $properties['mail'] = array(
    'label' => t('E-mail'),
    'description' => t('The e-mail address associated with this registration.'),
    'setter callback' => 'entity_property_verbatim_set',
    'validation callback' => 'valid_email_address',
    'schema field' => 'mail'
  );
  
  $properties['uid'] = array(
    'type' => 'integer',
    'label' => t('User ID'),
    'description' => t('The user that this warranty entity belongs to.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer mju_warranty entities',
    'schema field' => 'uid'
  );
  
  $properties['title'] = array(
    'type' => 'text',
    'label' => t('Title'),
    'description' => t('The title of the registrant.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer mju_warranty entities',
    'schema field' => 'title'
  );
  
  $properties['first_name'] = array(
    'type' => 'text',
    'label' => t('First name'),
    'description' => t('The first name of the registrant.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer mju_warranty entities',
    'schema field' => 'first_name'
  );
  
  $properties['last_name'] = array(
    'type' => 'text',
    'label' => t('First name'),
    'description' => t('The last name of the registrant.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer mju_warranty entities',
    'schema field' => 'last_name'
  );
  
  $properties['state'] = array(
    'type' => 'text',
    'label' => t('State'),
    'description' => t('The location (state) of the registrant.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer mju_warranty entities',
    'schema field' => 'state'
  );
  
  $properties['postcode'] = array(
    'type' => 'text',
    'label' => t('Post code'),
    'description' => t('The location (post code) of the registrant.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer mju_warranty entities',
    'schema field' => 'postcode'
  );
  
  $properties['contact_number'] = array(
    'type' => 'text',
    'label' => t('Contact number'),
    'description' => t('The contact number of the registrant.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer mju_warranty entities',
    'schema field' => 'contact_number'
  );
  
  $properties['birth_date'] = array(
    'type' => 'date',
    'label' => t('Birth date'),
    'description' => t('The date of birth of the registrant.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer mju_warranty entities',
    'schema field' => 'birth_date'
  );
  
  // Purchase details fields
  $properties['purchase_date'] = array(
    'type' => 'date',
    'label' => t('Purchase date'),
    'description' => t('The date of purchase.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer mju_warranty entities',
    'schema field' => 'purchase_date'
  );
  
  $properties['store'] = array(
    'type' => 'text',
    'label' => t('Store'),
    'description' => t('The store where the purchase was made.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer mju_warranty entities',
    'schema field' => 'store'
  );
  
  $properties['store_other'] = array(
    'type' => 'text',
    'label' => t('Store (other)'),
    'description' => t('The store (other) where the purchase was made.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer mju_warranty entities',
    'schema field' => 'store_other'
  );
  
  // Tell us what you think fields
  $properties['purchase_reason'] = array(
    'type' => 'text',
    'label' => t('Reason'),
    'description' => t('The reason for purchase.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer mju_warranty entities',
    'schema field' => 'purchase_reason'
  );
  
  $properties['health_related'] = array(
    'label' => t('Health related'),
    'description' => t('Boolean indicating whether or not the purchase is health related.'),
    'type' => 'boolean',
    'options list' => 'mju_warranty_health_related_options_list',
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer mju_warranty entities',
    'schema field' => 'health_related'
  );
  
  $properties['comments'] = array(
    'type' => 'text',
    'label' => t('Comments'),
    'description' => t('Optional comments.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer mju_warranty entities',
    'schema field' => 'comments'
  );
  
  $properties['lead_source'] = array(
    'type' => 'text',
    'label' => t('Lead source'),
    'description' => t('The method used to attract the user to purchase.'),
    'options list' => 'mju_warranty_lead_source_options_list',
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer mju_warranty entities',
    'schema field' => 'lead_source'
  );
  
  /*
  $properties['selection_reasons'] = array(
    'type' => 'text',
    'label' => t('Reasons'),
    'description' => t('Reasons for selecting Minijumbuk.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer mju_warranty entities',
    'schema field' => 'selection_reasons'
  );
  */
  
  $properties['feedback'] = array(
    'type' => 'text',
    'label' => t('Feedback'),
    'description' => t('Optional feedback.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer mju_warranty entities',
    'schema field' => 'feedback'
  );
  
  $properties['created'] = array(
    'type' => 'date',
    'label' => t('Date created'),
    'description' => t('The date the registration was created.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer mju_warranty entities',
    'schema field' => 'created'
  );
  
  $properties['changed'] = array(
    'type' => 'date',
    'label' => t('Date changed'),
    'description' => t('The date the registration was most recently updated.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer mju_warranty entities',
    'schema field' => 'changed'
  );
  
  return $info;
}
