<?php

/**
 * Implements hook_schema().
 */
function mju_warranty_schema() {
  $schema = array();
  
  $schema['mju_warranty'] = array(
    'description' => 'The base table for warranty registrations.',
    'fields' => array(
      'warranty_id' => array(
        'description' => 'The primary identifier for a registration.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE
      ),
      'type' => array(
        'description' => 'The type of this registration.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE
      ),
      
      // Customer detail fields
      'mail' => array(
        'description' => 'The e-mail address associated with the registration.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE
      ),
      'uid' => array(
        'description' => 'The user that this registration belongs to.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0
      ),
      'title' => array(
        'description' => 'The title of the registrant.',
        'type' => 'varchar',
        'length' => 20,
        'not null' => TRUE
      ),
      'first_name' => array(
        'description' => 'The first name of the registrant.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE
      ),
      'last_name' => array(
        'description' => 'The last name of the registrant.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE
      ),
      'state' => array(
        'description' => 'The location (state) of the registrant.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE
      ),
      'postcode' => array(
        'description' => 'The location (post code) of the registrant.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE
      ),
      'contact_number' => array(
        'description' => 'The contact number of the registrant.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => NULL
      ),
      'birth_date' => array(
        'description' => 'The date of birth of the registrant.',
        'type' => 'int',
        'not null' => FALSE,
        'default' => NULL
      ),
      
      // Purchase details fields
      'purchase_date' => array(
        'description' => 'The date of purchase.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0
      ),
      'store' => array(
        'description' => 'The store where the purchase was made.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE
      ),
      'store_other' => array(
        'description' => 'The store (other) where the purchase was made.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => NULL
      ),
      
      // Tell us what you think fields
      'purchase_reason' => array(
        'description' => 'The reason for purchase.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => NULL
      ),
      'health_related' => array(
        'description' => 'Boolean indicating whether or not the purchase is health related.',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0
      ),
      'comments' => array(
        'description' => 'Optional comments.',
        'type' => 'text',
        'size' => 'small',
        'not null' => FALSE,
        'default' => NULL
      ),
      'lead_source' => array(
        'description' => 'The method used to attract the user to purchase.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => NULL
      ),
      'selection_reasons' => array(
        'description' => 'Reasons for selecting Minijumbuk.',
        'type' => 'blob',
        'size' => 'normal',
        'not null' => FALSE,
        'default' => NULL
      ),
      'feedback' => array(
        'description' => 'Optional feedback.',
        'type' => 'text',
        'size' => 'small',
        'not null' => FALSE,
        'default' => NULL
      ),
      
      'created' => array(
        'description' => 'The Unix timestamp when the registration was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the registration was most recently saved.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0
      )
    ),
    'primary key' => array('warranty_id'),
    'indexes' => array(
      'uid' => array('uid')
    ),
    'foreign keys' => array(
      'owner' => array(
        'table' => 'users',
        'columns' => array('uid' => 'uid')
      )
    )
  );
  
  return $schema;
}
