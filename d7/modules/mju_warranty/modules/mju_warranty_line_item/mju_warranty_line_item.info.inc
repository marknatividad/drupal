<?php

/**
 * @file
 * Provides metadata for the Minijumbuk warranty line item entity.
 */

/**
 * Implements hook_entity_property_info().
 */
function mju_warranty_line_item_entity_property_info() {
  $info = array();
  
  // Add meta-data about the basic mju_warranty_line_item properties.
  $properties = &$info['mju_warranty_line_item']['properties'];
  
  $properties['warranty_line_item_id'] = array(
    'label' => t('Warranty Line item ID'),
    'description' => t('The internal numeric ID of the warranty line item.'),
    'type' => 'integer',
    'schema field' => 'warranty_line_item_id'
  );
  
  $properties['warranty_id'] = array(
    'label' => t('Warranty ID', array(), array('context' => 'a warranty registration')),
    'type' => 'integer',
    'description' => t('The internal numeric ID of the registration.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer mju_warranty entities',
    'schema field' => 'warranty_id'
  );
  
  
  
  $properties['range'] = array(
    'type' => 'text',
    'label' => t('Range'),
    'description' => t('The name of the product range.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer mju_warranty entities',
    'schema field' => 'range'
  );
  
  $properties['range_other'] = array(
    'type' => 'text',
    'label' => t('Range'),
    'description' => t('The alternate name of the product range.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer mju_warranty entities',
    'schema field' => 'range_other'
  );
  
  $properties['quantity'] = array(
    'label' => t('Quantity'),
    'description' => t('Quantity associated with this line item.'),
    'type' => 'decimal',
    'getter callback' => 'entity_property_verbatim_get',
    'setter callback' => 'entity_property_verbatim_set',
    'required' => TRUE,
    'schema field' => 'quantity'
  );
  
  $properties['created'] = array(
    'label' => t('Date created'),
    'description' => t('The Unix timestamp when the warranty line item was created.'),
    'type' => 'date',
    'setter callback' => 'entity_metadata_verbatim_set',
    'setter permission' => 'administer mju_warranty entities',
    'schema field' => 'created'
  );
  
  $properties['changed'] = array(
    'label' => t('Date changed'),
    'description' => t('The Unix timestamp when the warranty line item was most recently saved.'),
    'type' => 'date',
    'schema field' => 'changed'
  );
  
  return $info;
}
