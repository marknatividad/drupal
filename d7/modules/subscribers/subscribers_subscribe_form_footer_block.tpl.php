<div class="footer-subscription">
  <?php
  $handle = '<div class="info"><h3>Subscribe to news, deals &amp; offers</h3></div>';
  $options = array('handle' => $handle, 'content' => $form, 'collapsed' => TRUE, 'restrict_mobile' => TRUE);
  print theme('core_collapsible', $options);
  ?>
</div>
