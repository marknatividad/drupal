<?php

class SubscribersPimp extends Pimp {

  public function __construct() {
    // Always call the parent constructor first for basic setup
    parent::__construct();

    $this->name = t('Subscribers Import');
    $this->description = t('Upload subscribers from a CSV file. One value per file only! Email is unique, so duplicated ones are ignored. Only new ones can be created.');

    // @DEV logging not needed in prod
    $this->logging_enabled = TRUE;

    // Map from source csv to destination entity field
    $this->map = array(
      'first_name' => 'field_first_name',
      'last_name' => 'field_last_name',
      'email' => 'field_email_address'
    );

    // set the constaints from source csv, the value will be passed from source to destination to run constraints
    $this->constraint = array(
      'source' => 'email',
      'destination' => 'field_email_address'
    );

    // how many records to try process in a single batch task
    $this->limit = 10;
  }

  // the task config, get's run before the task is executed
  public function init() {
    // where our data is coming from
    $this->source = new PimpCSV();
    // where our data is going to
    $this->destination = new SubscriberPimpDest('subscriber', 'subscriber');
  }

  public function cleanup() {
    // Run the cleanup for our task (saves all parsed locations to node)
    $this->destination->cleanup();
  }

  public function execute(&$s) {
    // move to the next row
    $this->source->setRow($s->index);

    // if we have a valid row, continue
    if ($this->source->validRow()) {
      // give opportunity for row to be preprocessed before anything is done
      $this->preprocessRow();

      // run the provided constraint to set up our destination
      $this->runConstraint();

      // check if a destination record already exists according to the provided constraint
      if ($this->destination->hasTarget()) {
        $this->log("Skipping existing Subscriber: ".$this->current_state_string());
        $s->unchanged++;
      } else {
        // no record exists, migrate values from source into new record
        $this->migrateFieldValues();
        $s->new++;
        $this->destination->saveRow();
        $this->log("Created Subscriber: ".$this->current_state_string());
      }
    } else {
      $this->log("Skipping invalid row: ".$this->current_state_string());
      // @AMBIGUOUS
      $s->error = "Row is invalid!";
    }
    $this->destination->clearRow();
  }
}


class SubscriberPimpDest extends PimpEntity {

  public function preprocess() {
    $this->entity->field_subscriber_source->set('STORE');
    $this->entity->field_coupon_used->set(0);
    $this->entity->field_confirmed->set(0);
  }

  public function runConstraint($value) {
    $this->entity = subscribers_subscriber_existing_load($value);
  }

}
