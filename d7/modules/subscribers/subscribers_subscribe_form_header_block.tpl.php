<div>
  <a href="javascript:void(0);" class="slider-close">close</a>

  <?php if ($heading): ?>
    <?php print $heading; ?>
  <?php else: ?>
    <h2 class="subscribe-header"><span>Subscribe</span> to news, deals &amp; offers.</h2>
  <?php endif; ?>

  <?php if ($image): ?>
    <div class="subscribe-header"><?php print $image; ?></div>
  <?php endif; ?>

  <?php print $form; ?>

  <?php if ($terms): ?>
    <div class="subscribe-terms"><?php print $terms; ?></div>
  <?php endif; ?>
</div>
