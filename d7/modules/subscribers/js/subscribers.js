(function ($) {

  Drupal.behaviors.subscribers = {
    attach: function (context, settings) {
      if (document.cookie.match('subscribers_show_subscription_block') == null) {
        var subscribe_block = $('.subscription-top');
        subscribe_block.slideDown();
      }

      // Subscribe form slider
      if ($('a.user-subscribe', context).length) {
        $('a.user-subscribe', context).click(function(e) {
          e.preventDefault();
          var subscribe_block = $('.subscription-top');
          subscribe_block.slideDown();
        });
      }

      // Close slider button
      if ($('.slider-close', context).length) {
        $('.slider-close', context).click(function(e) {
          e.preventDefault();
          var subscribe_block = $('.subscription-top');
          subscribe_block.slideUp();
          var expirationDate = new Date;
          expirationDate.setMonth(expirationDate.getMonth()+6);
          document.cookie = "subscribers_show_subscription_block=1; expires=" + expirationDate;
        });
      }
    }
  };

})(jQuery);
