<?php

/**
 * Implements hook_rules_action_info().
 */
function subscribers_rules_action_info() {
  $actions = array(
    'subscribers_action_send_pet' => array(
      'label' => t('Subscription send PET mail'),
      'group' => t('Subscribers'),
      'parameter' => array(
        'pet_name' => array(
          'type' => 'text',
          'label' => t('The previewable email template to use'),
          'options list' => 'subscribers_pet_pet_list',
          'description' => t('The template that will be sent for this action. You can see the full list or add a new one from <a href="@url">this page</a>.', array('@url' => url('admin/structure/pets')))
        ),
        'to_text' => array(
          'type' => 'text',
          'label' => t('Recipient(s) (for sending to a fixed set of email address(es))'),
          'description' => t('The e-mail address or addresses where the message will be sent to. The formatting of this string must comply with RFC 2822. Either this or the variable recipient below must be provided.'),
          'optional' => TRUE,
          'default value' => NULL
        ),
        'to_account' => array(
          'type' => 'subscriber',
          'label' => t('Recipient (for sending to a user provided by a Rules event)'),
          'description' => t('Send mail to address on this account. Either this or the fixed recipient(s) above must be provided. If both are provided, this overrides the fixed list.'),
          'optional' => TRUE,
          'default value' => NULL
        ),
        'account_subs' => array(
          'type' => 'subscriber',
          'label' => t('User for token substitutions (if any)'),
          'description' => t('If your template includes user tokens, this user will be used for them.'),
          'optional' => TRUE,
          'default value' => NULL
        ),
        'node_subs' => array(
          'type' => 'node',
          'label' => t('Node for token substitutions (if any)'),
          'description' => t('If your template includes node tokens, this node will be used for them.'),
          'optional' => TRUE,
          'default value' => NULL
        )
      )
    ),
    'subscribers_action_subscriber_notifications_enable' => array(
      'label' => t('Enable news/updates notifications for a user'),
      'group' => t('Subscribers'),
      'parameter' => array(
        'subscriber' => array(
          'type' => 'subscriber',
          'label' => t('Subscriber'),
          'description' => t('The subscriber object'),
          'optional' => FALSE
        )
      )
    ),
    'subscribers_action_subscriber_coupon_redeem' => array(
      'label' => t('Mark the coupon as used for a subscriber'),
      'group' => t('Subscribers'),
      'parameter' => array(
        'email' => array(
          'type' => 'text',
          'label' => t('Email'),
          'description' => t('The subscriber email'),
          'optional' => FALSE
        )
      )
    )
  );

  return $actions;
}

/**
 * Implements hook_rules_event_info()
 */
function subscribers_rules_event_info() {
  $events = array(
    'subscribers_rules_event_subscribe_mailing_list' => array(
      'label' => t('After a user subscribes to the mailing list'),
      'group' => t('Subscribers'),
      'variables' => array(
        'email' => array(
          'type' => 'text',
          'label' => t('Email')
        )
      )
    ),
    'subscribers_rules_event_subscription_confirmation' => array(
      'label' => t('After a user confirms their subscription to the mailing list'),
      'group' => t('Subscribers'),
      'variables' => array(
        'email' => array(
          'type' => 'text',
          'label' => t('Email')
        )
      )
    )
  );

  return $events;
}

/**
 * Callback for enabling user notifications.
 *
 * @param $subscriber
 *   The subscriber object
 */
function subscribers_action_subscriber_notifications_enable($subscriber) {
  $subscriber = entity_metadata_wrapper('subscriber', $subscriber);

  // Confirm the subscriber account
  $subscriber->field_confirmed->set(1);
  subscribers_subscriber_save($subscriber->value());

  // Fetch a user object by email address
  $user = user_load_by_mail($subscriber->field_email_address->value());
  if ($user) {
    // Enable notifications for this user
    module_load_include('inc', 'core', 'includes/core.user');
    user_notifications_update($user);
  }
}

/**
 * Callback for redeeming a coupon for a subscriber.
 *
 * @param $email
 *   The subscriber email
 */
function subscribers_action_subscriber_coupon_redeem($email) {
  subscribers_subscriber_coupon_redeem($email);
}

/**
 * Callback for eponymous rules action.
 *
 * @param $pet_name
 *   The previewable email template to use.
 * @param $to_text
 *   Recipient(s) (for sending to a fixed set of email address(es)).
 * @param $to_account
 *   Recipient (for sending to a user provided by a Rules event).
 * @param $account_subs
 *   User for token substitutions (if any).
 * @param $node_subs
 *   Node for token substitutions (if any).
 * @param $settings
 */
function subscribers_action_send_pet($pet_name, $to_text, $to_account, $account_subs, $node_subs, $settings) {
  $pet = pet_load($pet_name);

  $to_account = entity_metadata_wrapper('subscriber', $to_account);

  // Resolve the recipient
  if (isset($to_account)) {
    $pet_to = $to_account->field_email_address->value();
  } else if (isset($to_text)) {
    $pet_to = $to_text;
  } else {
    watchdog('subscribers', 'Mail send using %name PET failed. No recipient provided.', array('%name' => $pet_name), WATCHDOG_ERROR);
    return;
  }

  $params = array(
    'pet_from' => variable_get('site_mail', ini_get('sendmail_from')),
    'pet_to' => $pet_to,
    'pet_uid' => isset($account_subs) ? $account_subs->uid : NULL,
    'pet_nid' => isset($node_subs) ? $node_subs->nid : NULL,
    'subscriber' => $to_account->value()
  );

  pet_send_one_mail($pet, $params);
}

/**
 * Return list of all PETs for rules configuration.
 */
function subscribers_pet_pet_list() {
  $list = array();

  foreach (pet_load_multiple(FALSE) as $pet) {
    $list[$pet->name] = $pet->title;
  }

  return $list;
}
