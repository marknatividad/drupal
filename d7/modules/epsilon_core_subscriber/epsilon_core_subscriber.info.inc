<?php

/**
 * @file
 * Provides metadata for the Epsilon subscriber entity.
 */

/**
 * Implements hook_entity_property_info().
 */
function epsilon_core_subscriber_entity_property_info() {
  $info = array();

  // Add meta-data about the basic epsilon_subscriber properties.
  $properties = &$info['epsilon_subscriber']['properties'];

  $properties['subscriber_id'] = array(
    'type' => 'integer',
    'label' => t('Subscriber ID', array(), array('context' => 'a subscriber')),
    'description' => t('The internal numeric ID of the subscriber.'),
    'schema field' => 'subscriber_id'
  );

  $properties['type'] = array(
    'type' => 'text',
    'label' => t('Type'),
    'description' => t('The human readable name of the subscriber type.'),
    'setter callback' => 'entity_property_verbatim_set',
    'options list' => 'epsilon_core_subscriber_type_options_list',
    'required' => TRUE,
    'schema field' => 'type'
  );

  $properties['first_name'] = array(
    'type' => 'text',
    'label' => t('First name'),
    'description' => t('The first name of the subscriber.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer epsilon_subscriber entities',
    'schema field' => 'first_name'
  );

  $properties['last_name'] = array(
    'type' => 'text',
    'label' => t('First name'),
    'description' => t('The last name of the subscriber.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer epsilon_subscriber entities',
    'schema field' => 'first_name'
  );

  $properties['mail'] = array(
    'label' => t('E-mail'),
    'description' => t('The e-mail address associated with this subscriber.'),
    'setter callback' => 'entity_property_verbatim_set',
    'validation callback' => 'valid_email_address',
    'schema field' => 'mail'
  );

  $properties['uid'] = array(
    'type' => 'integer',
    'label' => t('User ID'),
    'description' => t('The user that this subscriber entity belongs to.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer epsilon_subscriber entities',
    'schema field' => 'uid'
  );

  $properties['confirmed'] = array(
    'label' => t('Confirmed'),
    'description' => t('Boolean indicating whether or not the subscriber has confirmed their subscription.'),
    'type' => 'boolean',
    'options list' => 'epsilon_core_subscriber_confirmed_options_list',
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer epsilon_subscriber entities',
    'schema field' => 'confirmed'
  );

  $properties['synchronised'] = array(
    'label' => t('Synchronised'),
    'description' => t('Boolean indicating whether or not the subscriber has been synchronised to a 3rd party integration.'),
    'type' => 'boolean',
    'options list' => 'epsilon_core_subscriber_synchronised_options_list',
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer epsilon_subscriber entities',
    'schema field' => 'synchronised'
  );

  $properties['status'] = array(
    'label' => t('Status'),
    'description' => t('Boolean indicating whether or not the subscriber is enabled.'),
    'type' => 'boolean',
    'options list' => 'epsilon_core_subscriber_status_options_list',
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer epsilon_subscriber entities',
    'schema field' => 'status'
  );

  $properties['confirmed_date'] = array(
    'type' => 'date',
    'label' => t('Date confirmed'),
    'description' => t('The date the subscription was confirmed.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer epsilon_subscriber entities',
    'schema field' => 'confirmed_date'
  );

  $properties['created'] = array(
    'type' => 'date',
    'label' => t('Date created'),
    'description' => t('The date the subscriber was created.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer epsilon_subscriber entities',
    'schema field' => 'created'
  );

  $properties['changed'] = array(
    'type' => 'date',
    'label' => t('Date changed'),
    'description' => t('The date the subscriber was most recently updated.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer epsilon_subscriber entities',
    'schema field' => 'changed'
  );

  return $info;
}
