<?php

// Include the forms file from the subscriber module.
module_load_include('inc', 'epsilon_core_subscriber', 'includes/epsilon_core_subscriber.forms');

/**
 * Implements hook_entity_info().
 */
function epsilon_core_subscriber_entity_info() {
  $return = array(
    'epsilon_subscriber' => array(
      'label' => t('Epsilon Subscriber', array(), array('context' => 'a subscriber')),
      'controller class' => 'EpsilonCoreSubscriberEntityController',
      'locking mode' => 'pessimistic',
      'base table' => 'epsilon_subscriber',
      'load hook' => 'epsilon_core_subscriber_load',
      'uri callback' => 'epsilon_core_subscriber_uri',
      'label callback' => 'epsilon_core_subscriber_label',
      'fieldable' => TRUE,
      'entity keys' => array(
        'id' => 'subscriber_id',
        'bundle' => 'type'
      ),
      'bundle keys' => array(
        'bundle' => 'type'
      ),
      'bundles' => array(
        'epsilon_subscriber' => array(
          'label' => t('Epsilon Subscriber', array(), array('context' => 'a subscriber'))
        )
      ),
      'view modes' => array(
        'administrator' => array(
          'label' => t('Administrator'),
          'custom settings' => FALSE
        )
      ),
      'token type' => 'epsilon-subscriber',
      'metadata controller class' => '',
      'access callback' => '',
      'permission labels' => array(
        'singular' => t('Epsilon subscriber'),
        'plural' => t('Epsilon subscribers')
      ),
      // Prevent Redirect alteration of the subscriber form.
      'redirect' => FALSE
    )
  );

  return $return;
}

/**
 * Entity uri callback: gives modules a chance to specify a path for a subscriber.
 */
function epsilon_core_subscriber_uri($subscriber) {
  // Allow modules to specify a path, returning the first one found.
  foreach (module_implements('epsilon_core_subscriber_uri') as $module) {
    $uri = module_invoke($module, 'epsilon_core_subscriber_uri', $subscriber);

    // If the implementation returned data, use that now.
    if (!empty($uri)) {
      return $uri;
    }
  }

  return NULL;
}

/**
 * Entity label callback: returns the label for an individual subscriber.
 */
function epsilon_core_subscriber_label($entity, $entity_type) {
  return t('Subscriber @mail', array('@mail' => $entity->mail));
}

/**
 * Returns the options list for the subscriber confirmed property.
 */
function epsilon_core_subscriber_confirmed_options_list() {
  return epsilon_core_subscriber_boolean_options();
}

/**
 * Returns the options list for the subscriber synchronised property.
 */
function epsilon_core_subscriber_synchronised_options_list() {
  return epsilon_core_subscriber_boolean_options();
}

/**
 * Returns the options list for the subscriber status property.
 */
function epsilon_core_subscriber_status_options_list() {
  return array(
    0 => t('Disabled'),
    1 => t('Active')
  );
}

/**
 * Returns boolean options.
 */
function epsilon_core_subscriber_boolean_options() {
  return array(
    0 => t('No'),
    1 => t('Yes')
  );
}

/**
 * Wraps epsilon_core_subscriber_type_get_name() for the Entity module.
 */
function epsilon_core_subscriber_type_options_list() {
  return epsilon_core_subscriber_type_get_name();
}

/**
 * Returns the name of the specified subscriber type or all names keyed by type if no
 *   type is specified.
 *
 * This function merely traverses the bundles array looking for data instead of
 * relying on a special hook.
 *
 * @param $type
 *   The subscriber type whose name should be returned; corresponds to the bundle key
 *     in the subscriber entity definition.
 *
 * @return
 *   Either the specified name, defaulting to the type itself if the name is not
 *   found, or an array of all names keyed by type if no type is passed in.
 */
function epsilon_core_subscriber_type_get_name($type = NULL) {
  $names = array();

  $entity = entity_get_info('epsilon_subscriber');

  foreach ($entity['bundles'] as $key => $value) {
    $names[$key] = $value['label'];
  }

  if (empty($type)) {
    return $names;
  }

  if (empty($names[$type])) {
    return check_plain($type);
  } else {
    return $names[$type];
  }
}

/**
 * Implements hook_permission().
 */
function epsilon_core_subscriber_permission() {
  $entity_type = 'epsilon_subscriber';
  $entity_info = entity_get_info($entity_type);
  $labels = $entity_info['permission labels'];

  $permissions = array();

  // General 'administer' permission.
  $permissions['administer ' . $entity_type . ' entities'] = array(
    'title' => t('Administer @entity_type', array('@entity_type' => $labels['plural'])),
    'description' => t('Allows users to perform any action on @entity_type.', array('@entity_type' => $labels['plural'])),
    'restrict access' => TRUE
  );

  return $permissions;
}

/**
 * Callback for getting subscriber properties.
 * @see epsilon_core_subscriber_entity_property_info()
 */
function epsilon_core_subscriber_get_properties($subscriber, array $options, $name) {
  switch ($name) {
    case 'edit_url':
      return url('admin/people/subscribers/' . $subscriber->subscriber_id . '/edit', $options);
  }
}

/**
 * Implements hook_views_api().
 */
function epsilon_core_subscriber_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'epsilon_core_subscriber') . '/includes/views'
  );
}

/**
 * Implements hook_menu().
 */
function epsilon_core_subscriber_menu() {
  // Subscription page
  $items['subscribe'] = array(
    'title' => t('Subscribe'),
    'description' => t('Subscription page.'),
    'page callback' => 'epsilon_core_subscriber_form',
    'page arguments' => array('subscribe'),
    'access arguments' => array('access content'),
    'type' => MENU_NORMAL_ITEM
  );

  // Confirmation
  $items['subscribe/confirm/%'] = array(
    'page callback' => 'epsilon_core_subscriber_subscribe_confirmation_callback',
    'page arguments' => array(2),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK
  );

  // Unsubscribe page
  $items['unsubscribe'] = array(
    'title' => t('Unsubscribe'),
    'description' => t('Unsubscription page.'),
    'page callback' => 'epsilon_core_subscriber_form',
    'page arguments' => array('unsubscribe'),
    'access arguments' => array('access content'),
    'type' => MENU_NORMAL_ITEM
  );

  return $items;
}

/**
 * Implements hook_theme().
 */
function epsilon_core_subscriber_theme($existing, $type, $theme, $path) {
  return array(
    'epsilon_core_subscriber_subscribe_form' => array(
      'render element' => 'form',
      'template' => 'templates/epsilon-core-subscriber-subscribe-form'
    ),
    'epsilon_core_subscriber_subscribe_form_confirmation' => array(
      'template' => 'templates/epsilon-core-subscriber-subscribe-form-confirmation',
      'variables' => array('message' => NULL)
    )
  );
}

/**
 * Implements hook_block_info().
 *
 * Define all blocks provided by the module.
 */
function epsilon_core_subscriber_block_info() {
  $blocks['epsilon_subscribers_form_block'] = array(
    'info' => t('Epsilon Core Subscription Form Block')
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 *
 * Return a rendered or renderable view of a block.
 */
function epsilon_core_subscriber_block_view($delta = '') {
  $block = array();

  switch ($delta) {
    // Subscription form block
    case 'epsilon_subscribers_form_block':
      $block['subject'] = t('Epsilon Core Subscription Form Block');

      // Subscription form
      $block['content'] = epsilon_core_subscriber_form();
      break;
  }

  return $block;
}

/**
 * Form page callback.
 */
function epsilon_core_subscriber_form($action = 'subscribe') {
  $form_id = ($action == 'subscribe') ? 'epsilon_core_subscriber_subscribe_form' : 'epsilon_core_subscriber_unsubscribe_form';
  return drupal_get_form($form_id);
}

/**
 * Implements epsilon_core_subscriber_theme().
 *
 * Splits subscriber form into easily themable inputs.
 *
 * @param  $vars
 * @return $vars
 *
 * hook_preprocess_form
 * hook_preprocess_FORM_ID
 */
function epsilon_core_subscriber_preprocess_epsilon_core_subscriber_subscribe_form(&$vars, $hook) {
  $form = $vars['form'];

  // Add additional classes to submit button
  $form['submit']['#attributes']['class'][] = 'btn-primary form-control';

  // Decompose form into individual elements for theming
  $vars['first_name']  = render($form['first_name']);
  $vars['last_name']   = render($form['last_name']);
  $vars['email']       = render($form['mail']);
  $vars['subscribe']   = render($form['submit']);

  // Anything remaining such as hidden inputs etc...
  $vars['children']    = drupal_render_children($form);
}

/**
 * Loads a subscriber by ID.
 *
 * @param $subscriber_id
 *   The ID of the subscriber.
 */
function epsilon_core_subscriber_load($subscriber_id) {
  $subscribers = epsilon_core_subscriber_load_multiple(array($subscriber_id), array());
  return $subscribers ? reset($subscribers) : FALSE;
}

/**
 * Loads a subscriber by e-mail.
 *
 * @param $mail
 *   The e-mail of the subscriber.
 */
function epsilon_core_subscriber_load_by_mail($mail) {
  $subscribers = epsilon_core_subscriber_load_multiple(array(), array('mail' => $mail));
  return $subscribers ? reset($subscribers) : FALSE;
}

/**
 * Loads multiple subscribers by ID or based on a set of matching conditions.
 *
 * @see entity_load()
 *
 * @param $subscriber_ids
 *   An array of subscriber IDs.
 * @param $conditions
 *   An array of conditions to filter loaded subscribers by on the {epsilon_subscriber}
 *   table in the form 'field' => $value
 * @param $reset
 *   Whether to reset the internal subscriber loading cache.
 *
 * @return
 *   An array of subscriber objects indexed by subscriber_id.
 */
function epsilon_core_subscriber_load_multiple($subscriber_ids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('epsilon_subscriber', $subscriber_ids, $conditions, $reset);
}

/**
 * Deletes a subscriber by ID.
 *
 * @param $subscriber_id
 *   The ID of the subscriber to delete.
 *
 * @return
 *   TRUE on success, FALSE otherwise.
 */
function epsilon_core_subscriber_delete($subscriber_id) {
  return epsilon_core_subscriber_delete_multiple(array($subscriber_id));
}

/**
 * Deletes multiple subscribers by ID.
 *
 * @param $subscriber_ids
 *   An array of subscriber IDs to delete.
 *
 * @return
 *   TRUE on success, FALSE otherwise.
 */
function epsilon_core_subscriber_delete_multiple($subscriber_ids) {
  return entity_get_controller('epsilon_subscriber')->delete($subscriber_ids);
}

/**
 * Returns an initialized subscriber object.
 *
 * @param $type
 *   The type of the subscriber; defaults to the standard 'epsilon_subscriber' type.
 *
 * @return
 *   A subscriber object with all default fields initialized.
 */
function epsilon_core_subscriber_new($type = 'epsilon_subscriber') {
  return entity_get_controller('epsilon_subscriber')->create(array('type' => $type));
}

/**
 * Saves a subscriber.
 *
 * @param $subscriber
 *   The full subscriber object to save. If $subscriber->subscriber_id is empty, a new subscriber
 *     will be created.
 *
 * @return
 *   SAVED_NEW or SAVED_UPDATED depending on the operation performed.
 */
function epsilon_core_subscriber_save($subscriber) {
  return entity_get_controller('epsilon_subscriber')->save($subscriber);
}

/**
 * Produces base64 encoded urls.
 *
 * @param $value
 *   the string to encode.
 * @return
 *   base64 encoded string.
 */
function epsilon_core_subscriber_confirmation_code_encode($value) {
  $base64 = base64_encode($value);
  $base64url = strtr($base64, '+/=', '-_,');
  return $base64url;
}

/**
 * Decodes a base64 encoded urls.
 *
 * @param $value
 *   a base64 encoded string.
 * @return
 *   base64 decoded string.
 */
function epsilon_core_subscriber_confirmation_code_decode($value) {
  $base64url = strtr($value, '-_,', '+/=');
  $base64 = base64_decode($base64url);
  return $base64;
}

/**
 * The callback function to confirm a user subscription.
 *
 * @param $code
 *   The user subscription validation code.
 */
function epsilon_core_subscriber_subscribe_confirmation_callback($code) {
  $mail = epsilon_core_subscriber_confirmation_code_decode($code);

  // Check if a subscriber with this email exists, and is not confirmed
  $subscriber = epsilon_core_subscriber_load_by_mail($mail);
  if ($subscriber && !$subscriber->confirmed) {
    // Mark the subscription as confirmed
    $subscriber->confirmed = 1;
    $subscriber->confirmed_date = time();
    epsilon_core_subscriber_save($subscriber);

    // Invoke subscription confirmation event
    rules_invoke_event('epsilon_core_subscriber_rules_event_subscription_confirmation', $subscriber);
  }

  drupal_not_found();
}
