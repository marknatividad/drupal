<?php

/**
 * @file
 * Builds placeholder replacement tokens for subscriber related data.
 */
 
/**
 * Implements hook_token_info().
 */
function epsilon_core_subscriber_token_info() {
  $types = array(
    'epsilon-subscriber' => array(
      'name' => t('Epsilon Core Subscriber Tokens'),
      'description' => t('Tokens for Epsilon Core Subscriber module.')
    )
  );

  // Epsilon core subscriber specific tokens
  $tokens = array(
    'epsilon-subscriber' => array(
      'confirmation-url' => array(
        'name' => t('Confirmation URL'),
        'description' => t('The link to confirm a user subscription.')
      )
    )
  );

  return array(
    'types' => $types,
    'tokens' => $tokens
  );
}

/**
 * Implements hook_tokens().
 */
function epsilon_core_subscriber_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  if (($type == 'epsilon-subscriber') && !empty($data['epsilon-subscriber'])) {
    $subscriber = $data['epsilon-subscriber'];
    
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'confirmation-url':
          global $base_url;
          $code = epsilon_core_subscriber_confirmation_code_encode($subscriber->mail);
          $replacements[$original] = $base_url . '/subscribe/confirm/' . $code;
          break;
      }
    }
  }

  return $replacements;
}
