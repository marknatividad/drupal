<?php

/*
 * @file
 * Default Epsilon subscriber rules.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function epsilon_core_subscriber_default_rules_configuration() {
  $items = array();
  
  $items['rules_send_subscription_confirmation_email'] = entity_import('rules_config', '{ "rules_send_subscription_confirmation_email" : {
      "LABEL" : "Send Subscription Confirmation Email",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Epsilon Core Subscriber" ],
      "REQUIRES" : [ "rules", "epsilon_core_subscriber" ],
      "ON" : { "epsilon_core_subscriber_rules_event_subscription_confirmation" : [] },
      "DO" : [
        { "mail" : {
            "to" : [ "epsilon-subscriber:mail" ],
            "subject" : "Subscription Confirmed",
            "message" : "Dear [epsilon-subscriber:first-name],\\r\\n\\r\\nThank you for confirming your subscription.\\r\\n\\r\\nRegards,\\r\\n[site:name]",
            "language" : [ "" ]
          }
        },
        { "drupal_message" : { "message" : "Your subscription to our list has been confirmed." } },
        { "redirect" : { "url" : "[site:url]" } }
      ]
    }
  }');
  
  $items['rules_send_subscription_verification_email'] = entity_import('rules_config', '{ "rules_send_subscription_verification_email" : {
      "LABEL" : "Send Subscription Verification Email",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Epsilon Core Subscriber" ],
      "REQUIRES" : [ "rules", "epsilon_core_subscriber" ],
      "ON" : { "epsilon_core_subscriber_rules_event_subscribe" : [] },
      "DO" : [
        { "mail" : {
            "to" : [ "epsilon-subscriber:mail" ],
            "subject" : "Mailing List Confirmation",
            "message" : "Dear [epsilon-subscriber:first-name],\\r\\n\\r\\nThank you subscribing to the mailing list.\\r\\n\\r\\nPlease click \\u003Ca href=\\u0022[epsilon-subscriber:confirmation-url]\\u0022\\u003Ehere\\u003C\\/a\\u003E to confirm your subscription.\\r\\n\\r\\nRegards,\\r\\n[site:name]",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  
  return $items;
}
