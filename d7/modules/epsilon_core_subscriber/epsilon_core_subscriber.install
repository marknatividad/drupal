<?php

/**
 * Implements hook_schema().
 */
function epsilon_core_subscriber_schema() {
  $schema = array();

  $schema['epsilon_subscriber'] = array(
    'description' => 'The base table for subscribers.',
    'fields' => array(
      'subscriber_id' => array(
        'description' => 'The primary identifier for a subscriber.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE
      ),
      'type' => array(
        'description' => 'The type of this subscriber.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE
      ),
      'first_name' => array(
        'description' => 'The first name of the subscriber.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE
      ),
      'last_name' => array(
        'description' => 'The last name of the subscriber.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE
      ),
      'mail' => array(
        'description' => 'The e-mail address associated with the subscriber.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE
      ),
      'uid' => array(
        'description' => 'The user that this subscriber entity belongs to.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0
      ),
      'confirmed' => array(
        'description' => 'Boolean indicating whether or not the subscriber has confirmed their subscription.',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0
      ),
      'synchronised' => array(
        'description' => 'Boolean indicating whether or not the subscriber has been synchronised to a 3rd party integration.',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0
      ),
      'status' => array(
        'description' => 'Boolean indicating whether or not the subscriber is enabled.',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 1
      ),
      'confirmed_date' => array(
        'description' => 'The Unix timestamp when the subscription was confirmed.',
        'type' => 'int',
        'not null' => FALSE,
        'default' => NULL
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the subscriber was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the subscriber was most recently saved.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0
      )
    ),
    'primary key' => array('subscriber_id'),
    'unique keys' => array(
      'mail' => array('mail')
    ),
    'indexes' => array(
      'uid' => array('uid')
    ),
    'foreign keys' => array(
      'owner' => array(
        'table' => 'users',
        'columns' => array('uid' => 'uid')
      )
    )
  );
  
  return $schema;
}

/**
 * Implements hook_enable().
 */
function epsilon_core_subscriber_enable() {
}

/**
 * Implements hook_disable().
 */
function epsilon_core_subscriber_disable() {
}
