<div>
  <?php if ($message): ?>
    <?php print $message ?>
  <?php else: ?>
    <p><?php print t('Thank you, your submission has been received.'); ?></p>
  <?php endif; ?>
</div>
<small><em><?php print basename(__FILE__); ?></em></small>
