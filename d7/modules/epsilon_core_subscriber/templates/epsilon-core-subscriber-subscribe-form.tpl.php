<div class="row">

  <div class="col-sm-12 col-md-3 col-md-20p">
    <a href="#collapseSubscribeForm" data-toggle="collapse" data-parent="#accordion" class="eps-collapse-trigger collapsed h3">Subscribe<span class="pull-right glyphicon"></span></a>
  </div>

  <div class="col-sm-12 col-md-9 col-md-80p eps-collapse-container collapse" id="collapseSubscribeForm">
    <div class="row">
      <div class="col-sm-3">
        <?php print $first_name; ?>
      </div>

      <div class="col-sm-3">
        <?php print $last_name; ?>
      </div>

      <div class="col-sm-3">
        <?php print $email; ?>
      </div>

      <div class="col-sm-3">
        <?php print $subscribe; ?>
        <?php print $children; ?>
      </div>
    </div>
  </div>

</div>

<small><em><?php print basename(__FILE__); ?></em></small>
