<?php

/**
 * @file
 * Forms for creating, editing, and deleting subscribers.
 */

/**
 * Form callback: create or edit a subscriber.
 *
 * @param $subscriber
 *   The subscriber object to edit or for a create form an empty subscriber object.
 */
function epsilon_core_subscriber_ui_subscriber_form($form, &$form_state, $subscriber) {
  // First name
  $form['first_name'] = array(
    '#type' => 'textfield',
    '#title' => t('First Name'),
    '#default_value' => $subscriber->first_name,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -10
  );

  // Last name
  $form['last_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Last Name'),
    '#default_value' => $subscriber->last_name,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -5
  );

  // E-mail
  $form['mail'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail'),
    '#default_value' => $subscriber->mail,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => 0
  );

  // Confirmed
  $form['confirmed'] = array(
    '#type' => 'radios',
    '#title' => t('Confirmed'),
    '#description' => t('Whether or not the subscriber has confirmed their subscription.'),
    '#default_value' => $subscriber->confirmed,
    '#options' => epsilon_core_subscriber_confirmed_options_list(),
    '#required' => TRUE,
    '#weight' => 5
  );

  // Synchronised
  $form['synchronised'] = array(
    '#type' => 'radios',
    '#title' => t('Synchronised'),
    '#description' => t('Whether or not the subscriber has been synchronised to a 3rd party integration.'),
    '#default_value' => $subscriber->synchronised,
    '#options' => epsilon_core_subscriber_synchronised_options_list(),
    '#required' => TRUE,
    '#weight' => 10
  );

  // Status
  $form['status'] = array(
    '#type' => 'radios',
    '#title' => t('Status'),
    '#description' => t('Whether or not the subscriber is enabled.'),
    '#default_value' => $subscriber->status,
    '#options' => epsilon_core_subscriber_status_options_list(),
    '#required' => TRUE,
    '#weight' => 15
  );

  // Send autoresponder
  if (isset($subscriber->is_new)) {
    $form['autoresponder'] = array(
      '#type' => 'checkbox',
      '#title' => t('Autoresponder'),
      '#description' => t('Whether or not to send the subscription verification email.'),
      '#default_value' => TRUE,
      '#weight' => 20
    );
  }

  // Add the field related form elements.
  $form_state['epsilon_subscriber'] = $subscriber;

  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 400
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save subscriber'),
    '#submit' => array_merge($submit, array('epsilon_core_subscriber_ui_subscriber_form_submit'))
  );

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'epsilon_core_subscriber_ui_subscriber_form_validate';

  return $form;
}

/**
 * Validation callback for epsilon_core_subscriber_ui_subscriber_form().
 */
function epsilon_core_subscriber_ui_subscriber_form_validate($form, &$form_state) {
  $subscriber = $form_state['epsilon_subscriber'];
  $mail = $form_state['values']['mail'];

  // Validate e-mail format
  if (!valid_email_address($mail)) {
    form_set_error('mail', t('E-mail is not valid.'));
  }

  // Check if e-mail exists
  if (empty($subscriber->subscriber_id) && epsilon_core_subscriber_load_by_mail($mail)) {
    form_set_error('mail', t('E-mail is aleady in use and must be unique. Please supply another value.'));
  }
}

/**
 * Submit callback for epsilon_core_subscriber_ui_subscriber_form().
 */
function epsilon_core_subscriber_ui_subscriber_form_submit($form, &$form_state) {
  $subscriber = &$form_state['epsilon_subscriber'];

  // Save default parameters back into the $subscriber object.
  $subscriber->first_name = $form_state['values']['first_name'];
  $subscriber->last_name = $form_state['values']['last_name'];
  $subscriber->mail = $form_state['values']['mail'];
  $subscriber->confirmed = $form_state['values']['confirmed'];
  $subscriber->synchronised = $form_state['values']['synchronised'];
  $subscriber->status = $form_state['values']['status'];

  // Save the subscriber
  epsilon_core_subscriber_save($subscriber);

  // Invoke subscribe event
  if (isset($form_state['values']['autoresponder']) && $form_state['values']['autoresponder']) {
    rules_invoke_event('epsilon_core_subscriber_rules_event_subscribe', $subscriber);
  }

  // Redirect based on the button clicked.
  drupal_set_message(t('Subscriber saved.'));
  $form_state['redirect'] = 'admin/people/subscribers';
}

/**
 * Form callback: confirmation form for deleting a subscriber.
 *
 * @param $subscriber
 *   The subscriber object to be deleted.
 *
 * @see confirm_form()
 */
function epsilon_core_subscriber_ui_subscriber_delete_form($form, &$form_state, $subscriber) {
  $form_state['subscriber'] = $subscriber;

  // Ensure this include file is loaded when the form is rebuilt from the cache.
  $form_state['build_info']['files']['form'] = drupal_get_path('module', 'epsilon_core_subscriber_ui') . '/includes/epsilon_subscriber_ui.forms.inc';

  $form['#submit'][] = 'epsilon_core_subscriber_ui_subscriber_delete_form_submit';

  $content = entity_view('epsilon_subscriber', array($subscriber->subscriber_id => $subscriber));

  $form = confirm_form($form,
    t('Are you sure you want to delete subscriber %mail?', array('%mail' => $subscriber->mail)),
    '',
    drupal_render($content) . '<p>' . t('Deleting this subscriber cannot be undone.', array('@mail' => $subscriber->mail)) . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Submit callback for epsilon_core_subscriber_ui_subscriber_delete_form().
 */
function epsilon_core_subscriber_ui_subscriber_delete_form_submit($form, &$form_state) {
  $subscriber = $form_state['subscriber'];

  if (epsilon_core_subscriber_delete($subscriber->subscriber_id)) {
    drupal_set_message(t('Subscriber %id has been deleted.', array('%id' => $subscriber->subscriber_id)));
    watchdog('epsilon_core_subscriber', 'Deleted subscriber ID: %id (mail: @mail).', array('%id' => $subscriber->subscriber_id, '@mail' => $subscriber->mail), WATCHDOG_NOTICE);
    $form_state['redirect'] = 'admin/people/subscribers';
  } else {
    drupal_set_message(t('Subscriber %id could not be deleted.', array('%id' => $subscriber->subscriber_id)), 'error');
  }
}
