<?php

/**
 * Field handler to present a subscriber edit link.
 */
class epsilon_core_subscriber_handler_field_subscriber_link_edit extends epsilon_core_subscriber_handler_field_subscriber_link {

  function construct() {
    parent::construct();
  }

  function render($values) {
    // Ensure the user has access to edit this subscriber.
    if (!epsilon_core_subscriber_ui_access()) {
      return;
    }

    $text = !empty($this->options['text']) ? $this->options['text'] : t('edit');
    $subscriber_id = $this->get_value($values, 'subscriber_id');
    return l($text, 'admin/people/subscribers/' . $subscriber_id . '/edit', array('query' => drupal_get_destination()));
  }
}
