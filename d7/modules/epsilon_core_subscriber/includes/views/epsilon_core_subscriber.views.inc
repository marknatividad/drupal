<?php

/**
 * Export Epsilon subscribers to Views.
 */

/**
 * Implements hook_views_data().
 */
function epsilon_core_subscriber_views_data() {
  $table = 'epsilon_subscriber';
  $data = array();
  
  $data[$table]['table']['group']  = t('Epsilon Subscriber');

  $data[$table]['table']['base'] = array(
    'field' => 'order_id',
    'title' => t('Epsilon Subscriber'),
    'help' => t('A Subscriber.')
  );
  $data[$table]['table']['entity type'] = 'epsilon_subscriber';
  
  // Expose the subscriber ID.
  $data[$table]['subscriber_id'] = array(
    'title' => t('Subscriber ID', array(), array('context' => 'a subscriber')),
    'help' => t('The unique internal identifier of the subscriber.'),
    'field' => array(
      'click sortable' => TRUE
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric'
    ),
    'sort' => array(
      'handler' => 'views_handler_sort'
    )
  );
  
  // Expose the subscriber type.
  $data[$table]['type'] = array(
    'title' => t('Subscriber Type', array(), array('context' => 'a subscriber')),
    'help' => t('The type of the subscriber.'),
    'field' => array(
      'click sortable' => TRUE
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string'
    ),
    'sort' => array(
      'handler' => 'views_handler_sort'
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string'
    )
  );
  
  // Expose the subscriber first name.
  $data[$table]['first_name'] = array(
    'title' => t('First Name'),
    'help' => t('The first name of the subscriber.'),
    'field' => array(
      'click sortable' => TRUE
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string'
    ),
    'sort' => array(
      'handler' => 'views_handler_sort'
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string'
    )
  );
  
  // Expose the subscriber last name.
  $data[$table]['last_name'] = array(
    'title' => t('Last Name'),
    'help' => t('The last name of the subscriber.'),
    'field' => array(
      'click sortable' => TRUE
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string'
    ),
    'sort' => array(
      'handler' => 'views_handler_sort'
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string'
    )
  );
  
  // Expose the subscriber e-mail address.
  $data[$table]['mail'] = array(
    'title' => t('E-mail'),
    'help' => t('The e-mail address associated with the subscriber.'),
    'field' => array(
      'handler' => 'epsilon_core_subscriber_handler_field_mail',
      'click sortable' => TRUE
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string'
    ),
    'sort' => array(
      'handler' => 'views_handler_sort'
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string'
    )
  );
  
  // Expose the subscriber uid.
  $data[$table]['uid'] = array(
    'title' => t('Uid'),
    'help' => t("The subscribers's user ID."),
    'field' => array(
      'handler' => 'views_handler_field_user',
      'click sortable' => TRUE
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_user_uid',
      'name field' => 'name' // display this field in the summary
    ),
    'filter' => array(
      'title' => t('Name'),
      'handler' => 'views_handler_filter_user_name'
    ),
    'sort' => array(
      'handler' => 'views_handler_sort'
    ),
    'relationship' => array(
      'title' => t('Owner'),
      'help' => t("Relate this subscriber to its owner's user account"),
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'base field' => 'uid',
      'field' => 'uid',
      'label' => t('Subscriber owner')
    )
  );
  
  // Expose the subscriber confirmation field.
  $data[$table]['confirmed'] = array(
    'title' => t('Confirmed'),
    'help' => t('Boolean indicating whether or not the subscriber has confirmed their subscription.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Confirmed'),
      'type' => 'yes-no'
    ),
    'sort' => array(
      'handler' => 'views_handler_sort'
    )
  );
  
  // Expose the subscriber synchronised field.
  $data[$table]['synchronised'] = array(
    'title' => t('Synchronised'),
    'help' => t('Boolean indicating whether or not the subscriber has been synchronised to a 3rd party integration.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Synchronised'),
      'type' => 'yes-no'
    ),
    'sort' => array(
      'handler' => 'views_handler_sort'
    )
  );
  
  // Expose the subscriber status.
  $data[$table]['status'] = array(
    'title' => t('Status'),
    'help' => t('Boolean indicating whether or not the subscriber is enabled.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
      'output formats' => array(
        'active-disabled' => array(t('Active'), t('Disabled'))
      )
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Active'),
      'type' => 'yes-no'
    ),
    'sort' => array(
      'handler' => 'views_handler_sort'
    )
  );
  
  // Expose the confirmation date timestamps.
  $data[$table]['confirmed_date'] = array(
    'title' => t('Confirmed date'),
    'help' => t('The date the subscription was confirmed.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date'
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date'
    )
  );

  $data[$table]['confirmed_date_fulldate'] = array(
    'title' => t('Confirmed date'),
    'help' => t('In the form of CCYYMMDD.'),
    'argument' => array(
      'field' => 'confirmed_date',
      'handler' => 'views_handler_argument_node_created_fulldate'
    )
  );

  $data[$table]['confirmed_date_year_month'] = array(
    'title' => t('Confirmed date year + month'),
    'help' => t('In the form of YYYYMM.'),
    'argument' => array(
      'field' => 'confirmed_date',
      'handler' => 'views_handler_argument_node_created_year_month'
    )
  );

  $data[$table]['confirmed_date_timestamp_year'] = array(
    'title' => t('Confirmed date year'),
    'help' => t('In the form of YYYY.'),
    'argument' => array(
      'field' => 'confirmed_date',
      'handler' => 'views_handler_argument_node_created_year'
    )
  );

  $data[$table]['confirmed_date_month'] = array(
    'title' => t('Confirmed date month'),
    'help' => t('In the form of MM (01 - 12).'),
    'argument' => array(
      'field' => 'confirmed_date',
      'handler' => 'views_handler_argument_node_created_month'
    )
  );

  $data[$table]['confirmed_date_day'] = array(
    'title' => t('Confirmed date day'),
    'help' => t('In the form of DD (01 - 31).'),
    'argument' => array(
      'field' => 'confirmed_date',
      'handler' => 'views_handler_argument_node_created_day'
    )
  );

  $data[$table]['confirmed_date_week'] = array(
    'title' => t('Confirmed date week'),
    'help' => t('In the form of WW (01 - 53).'),
    'argument' => array(
      'field' => 'confirmed_date',
      'handler' => 'views_handler_argument_node_created_week'
    )
  );
  
  // Expose the created timestamps.
  $data[$table]['created'] = array(
    'title' => t('Created date'),
    'help' => t('The date the subscriber was created.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date'
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date'
    )
  );

  $data[$table]['created_fulldate'] = array(
    'title' => t('Created date'),
    'help' => t('In the form of CCYYMMDD.'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_fulldate'
    )
  );

  $data[$table]['created_year_month'] = array(
    'title' => t('Created year + month'),
    'help' => t('In the form of YYYYMM.'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_year_month'
    )
  );

  $data[$table]['created_timestamp_year'] = array(
    'title' => t('Created year'),
    'help' => t('In the form of YYYY.'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_year'
    )
  );

  $data[$table]['created_month'] = array(
    'title' => t('Created month'),
    'help' => t('In the form of MM (01 - 12).'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_month'
    )
  );

  $data[$table]['created_day'] = array(
    'title' => t('Created day'),
    'help' => t('In the form of DD (01 - 31).'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_day'
    )
  );

  $data[$table]['created_week'] = array(
    'title' => t('Created week'),
    'help' => t('In the form of WW (01 - 53).'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_week'
    )
  );
  
  // Expose the changed timestamps.
  $data[$table]['changed'] = array(
    'title' => t('Updated date'),
    'help' => t('The date the product was last updated.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date'
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date'
    )
  );

  $data[$table]['changed_fulldate'] = array(
    'title' => t('Updated date'),
    'help' => t('In the form of CCYYMMDD.'),
    'argument' => array(
      'field' => 'changed',
      'handler' => 'views_handler_argument_node_created_fulldate'
    )
  );

  $data[$table]['changed_year_month'] = array(
    'title' => t('Updated year + month'),
    'help' => t('In the form of YYYYMM.'),
    'argument' => array(
      'field' => 'changed',
      'handler' => 'views_handler_argument_node_created_year_month'
    )
  );

  $data[$table]['changed_timestamp_year'] = array(
    'title' => t('Updated year'),
    'help' => t('In the form of YYYY.'),
    'argument' => array(
      'field' => 'changed',
      'handler' => 'views_handler_argument_node_created_year'
    )
  );

  $data[$table]['changed_month'] = array(
    'title' => t('Updated month'),
    'help' => t('In the form of MM (01 - 12).'),
    'argument' => array(
      'field' => 'changed',
      'handler' => 'views_handler_argument_node_created_month'
    )
  );

  $data[$table]['changed_day'] = array(
    'title' => t('Updated day'),
    'help' => t('In the form of DD (01 - 31).'),
    'argument' => array(
      'field' => 'changed',
      'handler' => 'views_handler_argument_node_created_day'
    )
  );

  $data[$table]['changed_week'] = array(
    'title' => t('Updated week'),
    'help' => t('In the form of WW (01 - 53).'),
    'argument' => array(
      'field' => 'changed',
      'handler' => 'views_handler_argument_node_created_week'
    )
  );
  
  // Expose links to operate on the subscriber.
  $data[$table]['view_subscriber'] = array(
    'field' => array(
      'title' => t('Link'),
      'help' => t('Provide a simple link to the administrator view of the subscriber.'),
      'handler' => 'epsilon_core_subscriber_handler_field_subscriber_link'
    )
  );
  
  $data[$table]['edit_subscriber'] = array(
    'field' => array(
      'title' => t('Edit link'),
      'help' => t('Provide a simple link to edit the subscriber.'),
      'handler' => 'epsilon_core_subscriber_handler_field_subscriber_link_edit'
    )
  );
  
  $data[$table]['delete_subscriber'] = array(
    'field' => array(
      'title' => t('Delete link'),
      'help' => t('Provide a simple link to delete the subscriber.'),
      'handler' => 'epsilon_core_subscriber_handler_field_subscriber_link_delete'
    )
  );

  $data[$table]['operations'] = array(
    'field' => array(
      'title' => t('Operations links'),
      'help' => t('Display all the available operations links for the subscriber.'),
      'handler' => 'epsilon_core_subscriber_handler_field_subscriber_operations'
    )
  );

  $data[$table]['empty_text'] = array(
    'title' => t('Empty text'),
    'help' => t('Displays an appropriate empty text message for product lists.'),
    'area' => array(
      'handler' => 'epsilon_core_subscriber_handler_area_empty_text'
    )
  );
  
  return $data;
}
