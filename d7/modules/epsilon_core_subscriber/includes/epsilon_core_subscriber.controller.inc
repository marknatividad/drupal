<?php

/**
 * @file
 * The controller for the subscriber entity containing the CRUD operations.
 */

/**
 * The controller class for subscribers contains methods for the order CRUD
 * operations. The load method is inherited from the default controller.
 */
class EpsilonCoreSubscriberEntityController extends DrupalCommerceEntityController {
  
  /**
   * Create a default subscriber.
   *
   * @param array $values
   *   An array of values to set, keyed by property name.
   *
   * @return
   *   An order object with all default fields initialized.
   */
  public function create(array $values = array()) {
    $values += array(
      'subscriber_id' => NULL,
      'first_name' => '',
      'last_name' => '',
      'mail' => '',
      'uid' => 0,
      'confirmed' => 0,
      'synchronised' => 0,
      'status' => 1,
      'confirmed_date' => NULL,
      'created' => '',
      'changed' => ''
    );
    
    return parent::create($values);
  }
  
  /**
   * Saves a subscriber.
   *
   * @param $subscriber
   *   The full subscriber object to save.
   * @param $transaction
   *   An optional transaction object.
   *
   * @return
   *   SAVED_NEW or SAVED_UPDATED depending on the operation performed.
   */
  public function save($subscriber, DatabaseTransaction $transaction = NULL) {
    if (!isset($transaction)) {
      $transaction = db_transaction();
      $started_transaction = TRUE;
    }
    
    try {
      // Determine if we will be inserting a new subscriber.
      $subscriber->is_new = empty($subscriber->subscriber_id);
      
      // Set the timestamp fields.
      if ($subscriber->is_new && empty($subscriber->created)) {
        $subscriber->created = REQUEST_TIME;
      } else {
        // Otherwise if the subscriber is not new but comes from an entity_create()
        // or similar function call that initializes the created timestamp,
        // and uid values to empty strings, unset them to prevent
        // destroying existing data in those properties on update.
        if ($subscriber->created === '') {
          unset($subscriber->created);
        }
        if ($subscriber->uid === '') {
          unset($subscriber->uid);
        }
      }
      
      $subscriber->changed = REQUEST_TIME;
      
      return parent::save($subscriber, $transaction);
    }
    catch (Exception $e) {
      if (!empty($started_transaction)) {
        $transaction->rollback();
        watchdog_exception($this->entityType, $e);
      }
      throw $e;
    }
  }
  
}
