<?php

/**
 * @file
 * Forms for creating and unsubscribing subscribers.
 */

/**
 * Subscription form.
 *
 * Implements hook_form().
 */
function epsilon_core_subscriber_subscribe_form($form_state) {
  $form['first_name'] = array(
    '#type' => 'textfield',
    '#title_display' => 'invisible',
    '#attributes' => array(
      'placeholder' => t('First Name')
    )
  );

  $form['last_name'] = array(
    '#type' => 'textfield',
    '#title_display' => 'invisible',
    '#attributes' => array(
      'placeholder' => t('Last Name')
    )
  );

  $form['mail'] = array(
    '#type' => 'textfield',
    '#title_display' => 'invisible',
    '#attributes' => array(
      'placeholder' => t('E-mail Address')
    )
  );

  // Submit button
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Subscribe')
  );

  return $form;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function epsilon_core_subscriber_form_epsilon_core_subscriber_subscribe_form_alter(&$form, &$form_state, $form_id) {
  // Submit form via AJAX
  $form['submit']['#ajax'] = array(
    'callback' => 'epsilon_core_subscriber_subscribe_form_submit_ajax',
    'wrapper' => $form['#id'],
    'method' => 'replaceWith',
    'effect' => 'none'
  );
}

/**
 * Form validate handler for the subscription form block.
 */
function epsilon_core_subscriber_subscribe_form_validate($form, &$form_state) {
  // Check for default placeholder values
  if ($form_state['values']['first_name'] == '') {
    form_set_error('first_name', t($form['first_name']['#attributes']['placeholder'] . ' is required.'));
  }
  if ($form_state['values']['last_name'] == '') {
    form_set_error('last_name', t($form['last_name']['#attributes']['placeholder'] . ' is required.'));
  }
  if ($form_state['values']['mail'] == '') {
    form_set_error('mail', t($form['mail']['#attributes']['placeholder'] . ' is required.'));
  }

  $mail = $form_state['values']['mail'];
  if (!valid_email_address($mail)) {
    form_set_error('mail', t($form['mail']['#attributes']['placeholder'] . ' is not valid.'));
  }

  if ($subscriber = epsilon_core_subscriber_load_by_mail($mail)) {
    form_set_error('mail', t('Your e-mail was subscribed before.'));
  }
}

/**
 * Subscription form submit callback.
 */
function epsilon_core_subscriber_subscribe_form_submit_ajax($form, &$form_state) {
  // Return the actual form if it contains errors.
  if (form_get_errors()) {
    return $form;
  }

  $subscriber = epsilon_core_subscriber_new();

  $subscriber->first_name = $form_state['values']['first_name'];
  $subscriber->last_name = $form_state['values']['last_name'];
  $subscriber->mail = $form_state['values']['mail'];

  // Save the subscriber
  epsilon_core_subscriber_save($subscriber);

  // Invoke subscribe event
  rules_invoke_event('epsilon_core_subscriber_rules_event_subscribe', $subscriber);

  //drupal_set_message(t('Subscriber saved.'));

  // Define return AJAX command
  $commands[] = array();
  $commands[] = ajax_command_replace('#' . $form['#id'], theme('epsilon_core_subscriber_subscribe_form_confirmation'));
  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Unsubscription form.
 *
 * Implements hook_form().
 */
function epsilon_core_subscriber_unsubscribe_form($form_state) {
  $params = drupal_get_query_parameters();

  $form['mail'] = array(
    '#type' => 'textfield',
    '#title_display' => 'invisible',
    '#attributes' => array(
      'placeholder' => t('E-mail Address')
    ),
    '#default_value' => (isset($params['mail'])) ? $params['mail'] : ''
  );

  // Submit button
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Unsubscribe')
  );

  return $form;
}

/**
 * Form validate handler for the unsubscription form block.
 */
function epsilon_core_subscriber_unsubscribe_form_validate($form, &$form_state) {
  // Check for default placeholder values
  if ($form_state['values']['mail'] == '') {
    form_set_error('mail', t($form['mail']['#attributes']['placeholder'] . ' is required.'));
  }

  $mail = $form_state['values']['mail'];
  if (!valid_email_address($mail)) {
    form_set_error('mail', t($form['mail']['#attributes']['placeholder'] . ' is not valid.'));
  }

  $subscriber = epsilon_core_subscriber_load_by_mail($mail);
  if (!$subscriber) {
    form_set_error('mail', t('Your e-mail could not be found.'));
  }

  $form_state['epsilon_subscriber'] = $subscriber;
}

/**
 * Unubscription form submit callback.
 */
function epsilon_core_subscriber_unsubscribe_form_submit($form, &$form_state) {
  $subscriber = $form_state['epsilon_subscriber'];

  // Delete the subscriber
  epsilon_core_subscriber_delete($subscriber->subscriber_id);

  // Invoke unsubscribe event
  rules_invoke_event('epsilon_core_subscriber_rules_event_unsubscribe', $subscriber->mail);

  drupal_set_message(t('You have been unsubscribed.'));
}
