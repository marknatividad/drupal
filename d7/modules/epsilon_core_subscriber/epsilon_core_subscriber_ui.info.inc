<?php

/**
 * Implements hook_entity_property_info_alter().
 */
function epsilon_core_subscriber_ui_entity_property_info_alter(&$info) {
  $info['epsilon_subscriber']['properties']['edit_url'] = array(
    'label' => t('Edit URL'),
    'description' => t("The URL of the subscriber's edit page."),
    'getter callback' => 'epsilon_core_subscriber_get_properties',
    'type' => 'uri'
  );
}
