<?php

/**
 * @file
 * Rules integration for Epsilon subscribers.
 *
 */

/**
 * Implements hook_rules_event_info().
 */
function epsilon_core_subscriber_rules_event_info() {
  $events = array(
    'epsilon_core_subscriber_rules_event_subscribe' => array(
      'label' => t('After a user subscribes'),
      'group' => t('Epsilon Subscriber'),
      'variables' => array(
        'epsilon_subscriber' => array(
          'type' => 'epsilon_subscriber',
          'label' => t('Subscriber')
        )
      )
    ),
    'epsilon_core_subscriber_rules_event_unsubscribe' => array(
      'label' => t('After a user unsubscribes'),
      'group' => t('Epsilon Subscriber'),
      'variables' => array(
        'email' => array(
          'type' => 'text',
          'label' => t('Email')
        )
      )
    ),
    'epsilon_core_subscriber_rules_event_subscription_confirmation' => array(
      'label' => t('After a user confirms their subscription'),
      'group' => t('Epsilon Subscriber'),
      'variables' => array(
        'epsilon_subscriber' => array(
          'type' => 'epsilon_subscriber',
          'label' => t('Subscriber')
        )
      )
    )
  );

  return $events;
}
