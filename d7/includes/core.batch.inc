<?php

/**
 * @file
 * Supporting functions related to batch processes for the Core module.
 */

/**
 * Returns a config array for path bulk updates.
 */
function core_batch_path_bulk_update_settings() {
  return array(
    array(
      'groupheader' => t('Product Displays'),
      'batch_update_callback' => 'core_batch_product_display_pathauto_bulk_update_process',
      'batch_file' => drupal_get_path('module', 'core') . '/includes/core.batch.inc'
    )
  );
}

/**
 * Builds the Core path bulk update settings form.
 * Form contructor for path alias bulk update form.
 *
 * @see pathauto_bulk_update_form_submit()
 * @ingroup forms
 */
function core_batch_path_bulk_update_settings_form($form, &$form_state) {
  $form['#update_callbacks'] = array();

  $form['update'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select the types of un-aliased paths for which to generate URL aliases'),
    '#options' => array(),
    '#default_value' => array(),
  );

  $settings = core_batch_path_bulk_update_settings();
  foreach ($settings as $setting) {
    $form['#update_callbacks'][$setting['batch_update_callback']] = $setting;
    $form['update']['#options'][$setting['batch_update_callback']] = $setting['groupheader'];
  }

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  return $form;
}

/**
 * Form submit handler for path alias bulk update form.
 *
 * @see core_batch_path_bulk_update_settings_form()
 * @see core_batch_path_bulk_update_finished()
 */
function core_batch_path_bulk_update_settings_form_submit($form, &$form_state) {
  $batch = array(
    'title' => t('Bulk updating URL aliases'),
    'operations' => array(
      array('core_batch_path_bulk_update_start', array()),
    ),
    'finished' => 'core_batch_path_bulk_update_finished',
    'file' => drupal_get_path('module', 'core') . '/includes/core.batch.inc',
  );

  foreach ($form_state['values']['update'] as $callback) {
    if (!empty($callback)) {
      $settings = $form['#update_callbacks'][$callback];
      if (!empty($settings['batch_file'])) {
        $batch['operations'][] = array('core_batch_bulk_update_batch_process', array($callback, $settings));
      } else {
        $batch['operations'][] = array($callback, array());
      }
    }
  }

  batch_set($batch);
}

/**
 * Common batch processing callback for all operations.
 *
 * Required to load our include the proper batch file.
 */
function core_batch_bulk_update_batch_process($callback, $settings, &$context) {
  if (!empty($settings['batch_file'])) {
    require_once DRUPAL_ROOT . '/' . $settings['batch_file'];
  }
  return $callback($context);
}

/**
 * Batch callback; count the current number of URL aliases for comparison later.
 */
function core_batch_path_bulk_update_start(&$context) {
  $context['results']['count_before'] = core_batch_product_display_process_pending_count();
}

/**
 * Batch finished callback.
 */
function core_batch_path_bulk_update_finished($success, $results, $operations) {
  if ($success) {
    // Count the current number of URL aliases after the batch is completed
    // and compare to the count before the batch started.
    $results['count_after'] = core_batch_product_display_process_pending_count();
    $results['count_changed'] = max($results['count_before'] - $results['count_after'], 0);

    if ($results['count_changed']) {
      drupal_set_message(format_plural($results['count_changed'], 'Generated 1 URL alias.', 'Generated @count URL aliases.'));
    } else {
      drupal_set_message(t('No new URL aliases to generate.'));
    }
  } else {
    $error_operation = reset($operations);
    drupal_set_message(t('An error occurred while processing @operation with arguments : @args', array('@operation' => $error_operation[0], '@args' => print_r($error_operation[0], TRUE))));
  }
}

/**
 * Returns the number of product display nodes without correct aliases.
 *
 * @param $nid
 */
function core_batch_product_display_process_pending_count($nid = NULL) {
  $q = db_select('node', 'n');
  $q->leftJoin('url_alias', 'ua', "ua.source = CONCAT('node/', n.nid)");
  $q->condition('n.type', 'product_display');
  $q->condition(db_or()->condition('ua.alias', 'content/%', 'LIKE')->isNull('ua.alias'));

  if ($nid != NULL) {
    $q->condition('n.nid', $nid);
  }

  return $q->countQuery()->execute()->fetchField();
}

/**
 * Batch processing callback; Generate aliases for product displays.
 */
function core_batch_product_display_pathauto_bulk_update_process(&$context) {
  if (!isset($context['sandbox']['current'])) {
    $context['sandbox']['count'] = 0;
    $context['sandbox']['current'] = 0;
  }

  $q = db_select('node', 'n');
  $q->leftJoin('url_alias', 'ua', "ua.source = CONCAT('node/', n.nid)");
  $q->condition('n.type', 'product_display');
  $q->condition(db_or()->condition('ua.alias', 'content/%', 'LIKE')->isNull('ua.alias'));
  $q->addField('n', 'nid');
  $q->condition('n.nid', $context['sandbox']['current'], '>');
  $q->orderBy('n.nid');
  $q->addTag('core_batch_product_display_pathauto_bulk_update');
  $q->addMetaData('entity', 'node');

  // Get the total amount of items to process.
  if (!isset($context['sandbox']['total'])) {
    $context['sandbox']['total'] = $q->countQuery()->execute()->fetchField();

    // If there are no nodes to update, the stop immediately.
    if (!$context['sandbox']['total']) {
      $context['finished'] = 1;
      return;
    }
  }

  $q->range(0, 25);
  $nids = $q->execute()->fetchCol();
  $nodes = node_load_multiple($nids);

  foreach ($nodes as $node) {
    $node->path = array();
    $result = commerce_product_display_pathauto_alias_save($node, TRUE);
    //drupal_set_message(t(((result) ? 'Updated alias for node @nid.' : 'Update failed for node @nid'), array('@nid' => $node->nid)));
  }

  $context['sandbox']['count'] += count($nids);
  $context['sandbox']['current'] = max($nids);

  if ($context['sandbox']['count'] != $context['sandbox']['total']) {
    $context['finished'] = $context['sandbox']['count'] / $context['sandbox']['total'];
  }
}
